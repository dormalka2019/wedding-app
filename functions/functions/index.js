const functions = require('firebase-functions');
var shorturl = require('./shorturl');
var querystring = require('querystring');
const smsapi = require('./smsapi');
const mailer = require('./mailer');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const url = require('url');

const replaceCodeWithLink = async (body,mapedSendTo) => {
  if(!body.includes('[%-')) return body;
  var temp = '';
  const shortenUri = new shorturl();
  var splitedBody = body.split('[%-');
  for(var i = 0 ; i < splitedBody.length; i++){
    if(!splitedBody[i].includes('-%]')) temp += splitedBody[i];
    else{
        var temp2 = splitedBody[i].split('-%]');
        var fullLink = `https://tinyurl.com/wlgvq3d/invitations/view/${temp2[0]}`;
        var shorturi = await shortenUri.createShortUri(fullLink + '?p=' + mapedSendTo) 
        temp += 'https://'+shorturi.data.shortUrl;
        if(temp2[1]) temp += temp2[1];
    }
  }
  return temp;

}


exports.sendVarifiedPhoneNumber = functions.https.onRequest(async (req,res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
  res.set('Access-Control-Allow-Headers', '*');
  const {varifiedCode, phone} = req.query;
  var sender = new smsapi();
  var text = `קוד אימות שנשלח הינו: ${varifiedCode}`;
  sender.sendSms(phone,text);
  res.status(200).send({data:'done'});
})


exports.sendSmsProtfolio = functions.https.onRequest(async (req,res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET');
  res.set('Access-Control-Allow-Headers', '');
  const {message} = req.query;
  var sender = new smsapi();
  sender.sendSms('0524431050',message)
  res.status(200).send({data:'Ok'});
});




function sendMessage(scheduleMsgId, userId, sendNow) {

  admin.database().ref(`users/${userId}/scheduleMsgs`)
  .once('value',async (snapshot2) => {
    var scheduleMsgs2 = snapshot2.val();
    var currList = scheduleMsgs2.list[scheduleMsgId];
    const { body,mapedSendTo,inProccess } = currList;
    var uniqSet = [];
    console.log('inProccess-->',inProccess)
    console.log('sendNow-->',sendNow)
    if(inProccess || sendNow) {
      var sender = new smsapi();
      var fails = [];
      for( var i = 0; i < mapedSendTo.length; i++ ) {
        let realBody = body;
        try{
          console.log('realBody-->',realBody)
          if(uniqSet.includes(mapedSendTo[i])) throw new Error('Duplicate Number:' + mapedSendTo[i]);
          else uniqSet.push(mapedSendTo[i])
          realBody = await replaceCodeWithLink(realBody,mapedSendTo[i]);
          sender.sendSms(mapedSendTo[i],realBody)
          console.log('SMS sent to ' + mapedSendTo[i] + ' with body '+ realBody);
          const logObj = {
            type: 'Schedule SMS Success',
            relatedToUser: userId,
            relatedToMessage: scheduleMsgId,
            message: `Schedule Message Id (${scheduleMsgId}) \n 
                      userId: ${userId}  \n 
                      phone number ${mapedSendTo[i]} sent \n 
                      body: ${realBody}`
          }
          admin.database().ref(`logs/${new Date().getTime()}`).set(logObj)


        }
        catch(error){
          
          const errorObj = {
            type: 'Schedule SMS Error',
            relatedToUser: userId,
            relatedToMessage: scheduleMsgId,
            message: `Schedule Message Id (${scheduleMsgId}) \n 
                      userId: ${userId}  \n 
                      phone number ${mapedSendTo[i]} dosn't sent \n 
                      errorMessage: ${error.message}`
          }
          console.error('Error triggered: ' + error);
          admin.database().ref(`logs/${new Date().getTime()}`).set(errorObj)
          fails.push(mapedSendTo[i]);
        }
      }
      if(fails.length > 0) {
        scheduleMsgs2.status.totalUsed -= fails.length;
        scheduleMsgs2.status.totalLeft += fails.length;
        scheduleMsgs2.list[scheduleMsgId].success = false;
        scheduleMsgs2.list[scheduleMsgId].error = true;
        scheduleMsgs2.list[scheduleMsgId].failsNumbers = fails;
        
        const errorObj = {
          type: 'Schedule SMS Error',
          relatedToUser: userId,
          relatedToMessage: scheduleMsgId,
          message: `Schedule Message Id (${scheduleMsgId}) <br/>
                    userId: ${userId}  <br/>
                    phone number ${JSON.stringify(fails)} dosn't sent <br/>`
        }


        var mailing = new mailer();
        mailing.sendMail('dormalk@gmail.com', 'Schedule SMS Error', JSON.stringify(errorObj));
        
      } else {
        scheduleMsgs2.list[scheduleMsgId].error = false;
        scheduleMsgs2.list[scheduleMsgId].success = true;
      }
      scheduleMsgs2.list[scheduleMsgId].datetime = new Date().getTime();
      scheduleMsgs2.list[scheduleMsgId].inProccess = false;
      admin.database().ref(`users/${userId}/scheduleMsgs`).update(scheduleMsgs2);

    }
  })      

}


exports.sendMail = functions.https.onRequest(async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
  res.set('Access-Control-Allow-Headers', '*');
  const {dest, subject, message} = req.query;
  var mailing = new mailer();
  mailing.sendMail(dest, subject, message);
})


exports.schedualsEmailSend = functions.pubsub.schedule('every 2 minutes').onRun((context) => {
  admin.database().ref('pendingScheduleJobs').orderByChild('datetime').endAt(new Date().getTime())
  .once('value', snapshot => {
    if(snapshot){
      const allPendingMessages = snapshot.val();
      for(const key in allPendingMessages){
        const {createdBy} = allPendingMessages[key];
        sendMessage(key,createdBy, false) 
        admin.database().ref(`pendingScheduleJobs/${key}`).set(null);
      }
    }
  })
})

exports.sendScheduleMessage = functions.https.onRequest(async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
  res.set('Access-Control-Allow-Headers', '*');
  const {scheduleMsgId, userId} = req.query;
  admin.database().ref(`users/${userId}/scheduleMsgs`)
  .once('value',(snapshot) => {
    var scheduleMsgs = snapshot.val();
    const { inProccess,sendNow } = scheduleMsgs.list[scheduleMsgId];
    if(sendNow){
      if(!inProccess){
        scheduleMsgs.list[scheduleMsgId].inProccess = true;
        scheduleMsgs.list[scheduleMsgId].datetime = new Date(scheduleMsgs.list[scheduleMsgId].datetime).getTime();
        scheduleMsgs.list[scheduleMsgId].createdBy = userId;
        admin.database().ref(`pendingScheduleJobs/${scheduleMsgId}`).set(scheduleMsgs.list[scheduleMsgId])
      }
    } else {
      scheduleMsgs.list[scheduleMsgId].inProccess = true;
      sendMessage(scheduleMsgId,userId,true);
    }

    admin.database().ref(`users/${userId}/scheduleMsgs`).update(scheduleMsgs);

  })
  res.status(200).send({data:'Cool'});

})


exports.cancelScheduleMessage = functions.https.onRequest(async (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
    res.set('Access-Control-Allow-Headers', '*');
    const {scheduleMsgId,userId} = req.query;
    admin.database().ref(`users/${userId}/scheduleMsgs/list/${scheduleMsgId}/inProccess`).set(null)
    admin.database().ref(`pendingScheduleJobs/${scheduleMsgId}`).set(null);
    res.status(200).send('OK');
});


// Here create Send now sms

  /**
 * Sample PayPal IPN Listener implemented for Google Clound Functions.
 */

const request = require("request");

/**
 * @const {boolean} sandbox Indicates if the sandbox endpoint is used.
 */
const sandbox = true;

/** Production Postback URL */
const PRODUCTION_VERIFY_URI = "https://ipnpb.paypal.com/cgi-bin/webscr";
/** Sandbox Postback URL */
const SANDBOX_VERIFY_URI = "https://ipnpb.sandbox.paypal.com/cgi-bin/webscr";

/**
 * Determine endpoint to post verification data to.
 * 
 * @return {String}
 */



exports.maxPaymentHandler = functions.https.onRequest(async (req,res) => {
  console.log('Max Payment Event Received');

  console.log('req===>',req.method)

  const queryObject = url.parse(req.url,true).query;
  console.log(queryObject);
  const orderId = queryObject.Order;
  console.log('ordersInProgress/'+orderId);
  admin.database().ref('ordersInProgress/'+orderId)
  .once('value',snapshot => {
    var orderIn = snapshot.val();
    const packName = orderIn.orderApi;
    const userId = orderIn.userId;
    admin.database().ref('users/'+userId)
    .once('value', snapshot2 => {
      var results = snapshot2.val();
      if(packName === 'p50'){
        results.scheduleMsgs.status.totalPurchase += 50;
        results.scheduleMsgs.status.totalLeft += 50;  
      } else if (packName === 'p100'){
        results.scheduleMsgs.status.totalPurchase += 100;
        results.scheduleMsgs.status.totalLeft += 100;  
      } else if (packName === 'p100'){
        results.scheduleMsgs.status.totalPurchase += 100;
        results.scheduleMsgs.status.totalLeft += 100;  
      } else if (packName === 'p200'){
        results.scheduleMsgs.status.totalPurchase += 200;
        results.scheduleMsgs.status.totalLeft += 200;  
      } else if (packName === 'p400'){
        results.scheduleMsgs.status.totalPurchase += 400;
        results.scheduleMsgs.status.totalLeft += 400;  
      } else if (packName === 'p800'){
        results.scheduleMsgs.status.totalPurchase += 800;
        results.scheduleMsgs.status.totalLeft += 800;  
      } else if (packName === 'test'){
        results.scheduleMsgs.status.totalPurchase = 9999;
        results.scheduleMsgs.status.totalLeft = 9999;  
      } else if(packName === 'wedding_sea'){
        const ownerMail = orderIn.allDataPack.ownerMail;
        if(results.templatePurchase === undefined) {
          results['templatePurchase'] = [];
        }
        results.templatePurchase = [...results.templatePurchase, packName];
        const mail = new mailer();
        mail.sendMail(ownerMail, 'בוצעה רכישה של עיצוב', `
          היי =) <br/>
          רק רצינו להודיע לך שבוצעה רכישה של הזמנה ${orderIn.allDataPack.templateLabel} <br/>
          על סך ${orderIn.allDataPack.price} שקלים<br/>
          <br/>
          תודה רבה!
          MyEvent
        `)
      }
  
  
      if(!results.purchases) results.purchases = [];
      const newPurchas = {
        item: queryObject.ACode,
        time: new Date().getTime()
      }
      results.purchases.push(newPurchas)
      admin.database().ref(`users/${userId}`).set(results)
      .then(() => {
      admin.database().ref('ordersInProgress/'+orderId).update({transactionComplete: true});
        console.log("IPN Notification Event received successfully.");
        res.write('חיוב בוצע בהצלחה');
        res.end();      
      })
      .catch(error => {
        res.write('התרחשה שגיאה בעת החיוב - אנא פנה לשירות הלקוחות לבירור פרטים נוספים ('+error+')');
        res.end();      

      })
    })
    .catch(error => {
      res.write('התרחשה שגיאה בעת החיוב - אנא פנה לשירות הלקוחות לבירור פרטים נוספים');
      res.end();      
    });
  })
  .catch(error => {
    res.write('התרחשה שגיאה בעת החיוב - אנא פנה לשירות הלקוחות לבירור פרטים נוספים');
    res.end();      
  });
});





exports.getMessageBalance = functions.https.onRequest(async (req,res) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
  res.set('Access-Control-Allow-Headers', '*');
  res.set('Content-Type','text/xml; charset=utf-8')
  var sender = new smsapi();
  const respons = await sender.getBalance();
  res.send(respons.data);
})


