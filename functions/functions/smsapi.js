/* ---------------Your Api Access Key --------------------- */
/* -------------------------------------------------------- */
var ApiKey = "bH4389fR5gZ7r544648JN7x29nM234kP";
/* -------------------------------------------------------- */

var axios = require('axios');

module.exports = function()
{

	this.sendSms = function(number, text){

		const data =`<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<sendSmsToRecipients xmlns="apiGlobalSms">
			  <ApiKey>${ApiKey}</ApiKey>
			  <txtOriginator>0524431050</txtOriginator>
			  <destinations>${number}</destinations>
			  <txtSMSmessage>${text}</txtSMSmessage>
			  <dteToDeliver></dteToDeliver>
			  <txtAddInf>jsnodetest</txtAddInf>
			</sendSmsToRecipients>
		  </soap:Body>
		</soap:Envelope>`;

		const headers = {
			'Content-Type': 'text/xml; charset=utf-8',
			'SOAPAction': 'apiGlobalSms/sendSmsToRecipients'
		} 
		return axios.post(`https://sapi.itnewsletter.co.il/webservices/WsSMS.asmx`,data,{headers})
	}


	this.getBalance = function() {
		const data =`<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
			<getBalance xmlns="apiGlobalSms">
			  <ApiKey>${ApiKey}</ApiKey>
			</getBalance>
		  </soap:Body>
		</soap:Envelope>`;
	  
		const headers = {
			'Content-Type': 'text/xml; charset=utf-8',
			'SOAPAction': 'apiGlobalSms/getBalance'
		} 
		return axios.post(`https://sapi.itnewsletter.co.il/webservices/WsSMS.asmx`,data,{headers})
	  
	}
}

