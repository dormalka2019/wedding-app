import React from 'react';
import AppRouter from './routers/AppRouter';
import Alert from './components/Alert';
import LoginAs from './components/LoginAs';
import { VarificationMail } from './components';
class App extends React.Component{
  render(){
    return (
      <div className="App">
        <VarificationMail/>
        <LoginAs/>
        <AppRouter/>
        <Alert/>
      </div>
    );
  
  }
}

export default App;
