import { createStore,combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import userdata from '../reducers/auth';
import invitedsData from '../reducers/invitedsData'
import budgetData from '../reducers/budgetData'
import sittingData from '../reducers/sittingData'
import alerts from '../reducers/alerts'
import schedualsMsgs from '../reducers/messaging'
import invitations from '../reducers/invitations'


const composeEnhancers =  window.__REDUX_DEVTOOLS_EXTENSION__COMPOSE__ || compose;


export default () => {
    const store = createStore(
        combineReducers({   userdata,
                            invitedsData,
                            budgetData,
                            sittingData,
                            alerts,
                            schedualsMsgs,
                            invitations
                        }),
        composeEnhancers(applyMiddleware(thunk))
    )
    return store;
}