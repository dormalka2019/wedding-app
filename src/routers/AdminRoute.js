import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({
  isAuthenticated,
  isAdmin,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
        isAuthenticated && isAdmin? 
        (
          <Component {...props} />
        ) : !isAuthenticated?(
            <Redirect to="/signin" />
        ): (
            <Redirect to="/budgetmanager" />
        )
    )} />
  );

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.userdata.uid,
  isAdmin: state.userdata.isAdmin
});

export default connect(mapStateToProps)(PrivateRoute);
