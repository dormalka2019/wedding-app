import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { isMobile } from '../helpers/device-detect'
import InformOnlyDesktop from '../components/InformOnlyDesktop';

export const PrivateRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
        !isMobile? (
          isAuthenticated? (
            <Component {...props} />
          ): (
            <Redirect to="/signin"/>
          )
        ) : (
          <InformOnlyDesktop/>
        )
    )} />
  );

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.userdata.uid
});

export default connect(mapStateToProps)(PrivateRoute);
