import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
export const PrivateRoute = ({
  isAuthenticated,
  isAdmin,
  emailVerified,
  component: Component,
  ...rest
}) => (
    <Route {...rest} component={(props) => (
        isAuthenticated &&
        emailVerified ?
        !isAdmin?
        (
          <Component {...props} />
        ) : (
          <Redirect to="/allusers" />
        ): (
          <Redirect to="/signin" />
        )
    )} />
  );

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.userdata.uid,
  isAdmin: state.userdata.isAdmin,
  emailVerified: state.userdata.emailVerified
});

export default connect(mapStateToProps)(PrivateRoute);
