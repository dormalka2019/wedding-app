import React from 'react';
import {  InvitationGenerator,
    Header, 
    TemplateView, 
    Main,
    Footer, 
    InvitedManager,
    BudgetMenager,
    SignIn, 
    MessageManager,
    SittingArrange,
    UsersView,
    MessagesView,
    InvitationManager,
    InvitationDesignManager,
    InvitationStore,
    LogsView,
    PackageStore,
    MainPanel} from '../components';
import { Router, Switch, Route } from 'react-router-dom';
import { matchPath } from 'react-router';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import DesktopRoute from './DesktopRoute';
import { SelfDetails } from '../components/SelfDetails/SelfDetails';
import AdminRoute from './AdminRoute';
import SingleUserManage from '../components/SingleUserManage';
import { Takanon } from '../components/Takanon';


export const history = require("history").createBrowserHistory();

export default () => (
    <Router history={history}>
        {!matchPath(window.location.pathname,"/invitations/view/:id") && <Header/>}
        <Switch>
            <Route exact path="/"><Main/></Route>
            <Route path ="/invitations/create" component={InvitationGenerator}></Route>
            <Route path ="/invitations/view/:id" component={TemplateView}></Route>
            <Route path ="/invitations/edit/:id" component={InvitationGenerator}></Route>
            <PrivateRoute path ="/invitedmanager" component={InvitedManager}></PrivateRoute>
            <PrivateRoute path="/budgetmanager" component={BudgetMenager}></PrivateRoute>
            <PrivateRoute path="/selfdetails" component={SelfDetails}></PrivateRoute>
            <PrivateRoute path="/messagemanager" component={MessageManager}></PrivateRoute>
            <DesktopRoute path="/sittingarrange" component={SittingArrange}></DesktopRoute>
            <PublicRoute path ="/signin" component={SignIn}></PublicRoute>
            <PublicRoute path ="/signup" component={SignIn}></PublicRoute>
            <AdminRoute path = "/allusers" component={UsersView}></AdminRoute>
            <AdminRoute path = "/alltickets" component={MessagesView}></AdminRoute>
            <AdminRoute path = "/logs" component={LogsView}></AdminRoute>
            <AdminRoute path = "/invitaionsdesign" component={InvitationDesignManager}></AdminRoute>
            <AdminRoute path = '/admin/user/:id' component={SingleUserManage}></AdminRoute>
            <PrivateRoute path = "/invitaionmanager" component={InvitationManager}></PrivateRoute>
            <PrivateRoute path="/invitationsstore" component={InvitationStore}></PrivateRoute>
            <PrivateRoute path="/packagestore" component={PackageStore}></PrivateRoute>
            <AdminRoute path='/mainpanel' component={MainPanel}></AdminRoute>
            <PublicRoute path ="/takanon" component={Takanon}></PublicRoute>
            {!matchPath(window.location.pathname,"/invitations/view/:id") && <Route component={NotFound}></Route>}
        </Switch>
        {!matchPath(window.location.pathname,"/invitations/view/:id") && <Footer/>}
    </Router>
);


const NotFound = () => (<div style={{height: '100vh'}}>Not found</div>)
