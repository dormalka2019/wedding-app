import { SET_ALERT,HIDE_ALERT } from "../actions/alerts";

export default (state = {showAlert: false}, action) => {
    switch(action.type){
        case SET_ALERT:
            return {
                ...state,
                showAlert: true,
                message: action.message
            }
        case HIDE_ALERT:
            return {
                ...state,
                showAlert: false
            }
        default:
            return state;
    }
}