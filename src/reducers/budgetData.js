import { SET_BUDGET_LIST } from "../actions/budgetData";

export default (state = {}, action) => {
    switch(action.type){
        case SET_BUDGET_LIST:
            return {
                ...state,
                ...action.budgetlist
            }
        default:
            return state;
    }
}