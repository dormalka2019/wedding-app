import { SET_SITTING_DATA } from "../actions/sittingData";

export default (state = {tableMap: [], tables: []}, action) => {
    switch(action.type){
        case SET_SITTING_DATA:
            return {
                ...action.sittingData
            }
        default:
            return state;
    }
}