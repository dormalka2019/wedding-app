import { CREATE_NEW_SCHEDULE_MESSAGE,
        FETCH_SCHEDULE_MESSAGES,
        DELETE_SCHEDULE_MESSAGE,
        UPDATE_SCHEDULE_MESSAGE,
        UPDATE_STATUS } from "../actions/messaging";


const renderScheduleMsgsData = (messaging) => {
    var parsedData = [];
    for(let key in messaging){
        const newElem = {
            id: key,
            ...messaging[key]
        }
        parsedData.push(newElem);
    }
    return parsedData;
}


const INITIAL_STATE = {
    list: [],
    status: {
        totalPurchase: 0, 
        totalUsed: 0, 
        totalLeft: 0    
    }
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case CREATE_NEW_SCHEDULE_MESSAGE:
            var newElem = {
                ...action.newScheduleMsg,
                id: action.id
            };
            return {
                ...state,
                list: [...state.list, newElem]
            }
        case FETCH_SCHEDULE_MESSAGES:
            return {
                ...state,
                ...action.scheduleMsgs,
                list: [...state.list,...renderScheduleMsgsData(action.scheduleMsgs.list)]
            }
        case DELETE_SCHEDULE_MESSAGE:
            return {
                ...state,
                list: [...state.list.filter(elem => elem.id !== action.id)]
            }
        case UPDATE_SCHEDULE_MESSAGE:            
            return {
                ...state,
                list: [...state.list.map(elem => elem.id === action.id? action.update : elem)]    
            }
        case UPDATE_STATUS:
            return {
                ...state,
                status: action.status
            }
        default:
            return state;
    }
}