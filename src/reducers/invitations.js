import {    CREATE_INVITATION,
            DELETE_INVITATION,
            UPDATE_INVITATION,
            FETCH_INVITATIONS } from "../actions/invitations";


const renderInvitationsData = (messaging) => {
    var parsedData = [];
    for(let key in messaging){
        const newElem = {
            id: key,
            ...messaging[key]
        }
        parsedData.push(newElem);
    }
    return parsedData;
}


export default (state = [], action) => {
switch(action.type){
    case CREATE_INVITATION:
        return [
            ...state,
            action.invitation            
        ]
    case FETCH_INVITATIONS:
        return [
            ...state,
            ...renderInvitationsData(action.invitations).map(invitation => {
                invitation.watchCount = invitation.watchCount? invitation.watchCount : 0
                return invitation;
            })
        ]
    case DELETE_INVITATION:
        return [
            ...state.filter(invitation => invitation.id !== action.invitationId)
        ]
    case UPDATE_INVITATION:
        let updatedElem = {
            ...state.filter(invitation => invitation.id === action.invitationId)[0],
            ...action.update,
            id: action.invitationId
        };
        return [
            ...state.map(invitation => invitation.id === action.invitationId? updatedElem:invitation),
        ]   
    default:
        return state;
}
}