
import { LOGIN_AS } from '../actions/admin';

export default (state = {loading:true}, action) => {
    switch(action.type){
        case 'LOGIN':
        case 'UPDATE_USER':
            return {
                ...state,
                ...action.user,
                isAdmin: action.user.isAdmin,
                loading: false
            }
        case LOGIN_AS:
            return {
                ...state,
                adminId: action.adminId
            }
        case 'LOGOUT':
            return {}
        case 'IN_LOAD':
            return{
                ...state,
                loading: true
            }
        default:
            return state;
    }
}