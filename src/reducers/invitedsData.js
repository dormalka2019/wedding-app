import { SET_INVITED_LIST } from "../actions/invitedsData";

export default (state = [], action) => {
    switch(action.type){
        case SET_INVITED_LIST:
            const invitedlist = action.invitedlist? action.invitedlist : []; 
            return [
                ...invitedlist
            ]
        default:
            return state;
    }
}