export const exportTableToExcel = (listObj,headers) => {
    return new Promise((resolve,reject) => {
        var table = [];
        var table_row = [];
        headers.forEach(header => table_row.push(header.label))
        table.push(table_row);

        listObj.forEach(row => {
            table_row = [];
            headers.forEach(header => table_row.push(row[header.api]))
            table.push(table_row);
        })

        var dataType = "data:text/csv;charset=utf-8,"
        let csvContent =  table.map(e => e.join(",")).join("\n");
        var universalBOM = "\uFEFF";
        var encodedUri = encodeURIComponent(universalBOM + csvContent);
        var link = document.createElement("a");
        link.setAttribute("href",dataType + encodedUri);
        link.setAttribute("download", "my_data.csv");
        document.body.appendChild(link); // Required for FF
        
        link.click();
        resolve(true)
    })
}


export const importFileToTableForInvitedList = (inputId,fileName,headers) => {
    return new Promise((resolve,reject) => {
        var fileUpload = document.getElementById(inputId);
        if (fileName.endsWith('.csv')) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var table = [];
                    var rows = e.target.result.split("\n");
                    for (var i = 1; i < rows.length; i++) {
                        var cells = rows[i].split(",");
                        if (cells.length > 1) {
                            var row = {}
                            row.fullName = cells[0]
                            row.phone = cells[1]
                            row.totalCount = Number.parseInt(cells[2])
                            row.children = Number.parseInt(cells[3])
                            row.meet = Number.parseInt(cells[4])
                            row.vegetarian = Number.parseInt(cells[5])
                            row.vegan = Number.parseInt(cells[6])
                            row.side = cells[7]
                            row.approved = Number.parseInt(cells[8])
                            row.present = Number.parseInt(cells[9])
                            table.push(row)
                        }
                    }
                    fileUpload.value = '';
                    resolve(table);
                }
                reader.readAsText(fileUpload.files[0]);
                
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid CSV file.");
        }
    })
  
}




export const importFileToTableForBudgetList = (inputId,fileName,headers) => {
    return new Promise((resolve,reject) => {
        var fileUpload = document.getElementById(inputId);
        if (fileName.endsWith('.csv')) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var table = [];
                    var rows = e.target.result.split("\n");
                    for (var i = 1; i < rows.length; i++) {
                        var cells = rows[i].split(",");
                        if (cells.length > 1) {
                            var row = {}
                            row.expence = cells[0]
                            row.contact = cells[1]
                            row.phone = cells[2]
                            row.price = Number.parseInt(cells[3]);
                            row.category = cells[4]
                            table.push(row)
                        }
                    }
                    fileUpload.value = '';
                    resolve(table);
                }
                reader.readAsText(fileUpload.files[0]);
                
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid CSV file.");
        }
    })
  
}