import axios from 'axios';
import firebase from 'firebase';

const MasofId = '5600884636';
const key = '71d7909c7cc5e84074de7372fe3db3fbf25aa5aa';


export const generatePaymentUrl = (pack,userdata) => {
    firebase.database().ref('ordersInProgress').push({
        userId: userdata.uid,
        orderApi: pack.api,
        allDataPack: pack,
        startAt: new Date().getTime()
    }).then(snapshot => {
        var uri = 'https://icom.yaad.net/p3/?';
        uri+='action=APISign&';
        uri+='What=SIGN&';
        uri+=`KEY=${key}&`;
        uri+='PassP=yaad&;'
        uri+=`Masof=${MasofId}&`;
        uri+=`Order=${snapshot.key}&`;
        uri+=`Info=${pack.api}&`;
        uri+=`Amount=${pack.price}&`;
        uri+='UTF8=True&';
        uri+='UTF8out=True&';
        uri+='UserId=&';
        uri+=`ClientName=${userdata.displayName.split(' ')[1]}&`;
        uri+=`ClientLName=${userdata.displayName.split(' ')[0]}&`;
        uri+='street=&';
        uri+='city=&';
        uri+='zip=&';
        uri+='cell=&';
        uri+=`phone=${userdata.phone? userdata.phone: ''}&`;
        uri+=`email=${userdata.email}&`;
        uri+='Tash=2&';
        uri+='FixTash=False&';
        uri+='ShowEngTashText=False&';
        uri+='Coin=1&';
        uri+='Postpone=False&';
        uri+='J5=False&';
        uri+='Sign=True&';
        uri+='MoreData=True&';
        uri+='sendemail=True&';
        uri+='SendHesh=True&';
        uri+=`heshDesc=%5b0~${pack.name}~1~${pack.price}%5d&`;
        uri+='Pritim=True&';
        uri+='PageLang=HEB&';
        uri+='tmp=1';
    
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        axios.get(proxyurl+uri)
        .then(response => {
            const a_el = document.createElement('a');
            a_el.href = 'https://icom.yaad.net/p3/?action=pay&'+response.data;
            a_el.target = '_blank';
            a_el.click();
        })
        .catch(error => console.error(error))
    })

}
