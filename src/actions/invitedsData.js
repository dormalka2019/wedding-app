import * as firebase from 'firebase';
import { convertToArr } from '../helpers/fucntions';

export const SET_INVITED_LIST = 'SET_INVITED_LIST';


export const startSetList = (invitedlist) => {
    return(dispatch) => {
        const user = firebase.auth().currentUser;
        return firebase.database().ref(`users/${user.uid}/invitedlist`).set(invitedlist)
        .then((res) => dispatch({type: SET_INVITED_LIST, invitedlist})) 
    }
}


export const startFetchInvetedList = (uid) => {
    return(dispatch) => {
        return firebase.database().ref(`users/${uid}/invitedlist`)
        .once('value', (snapshot) => {
            const invitedlist = convertToArr(snapshot.val());
            dispatch({type: SET_INVITED_LIST, invitedlist});
        })
    }
}