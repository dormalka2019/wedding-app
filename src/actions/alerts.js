export const SET_ALERT = 'SET_ALERT'
export const HIDE_ALERT = 'HIDE_ALERT'

export const startSetAlert = (message,timeout) => {
    return(dispatch) => {
        dispatch({type: SET_ALERT, message});
        window.setTimeout(() => {
            dispatch({type: HIDE_ALERT, message});
        },timeout)
    }
}
