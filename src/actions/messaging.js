import * as firebase from 'firebase';

import axios from 'axios';
export const CREATE_NEW_SCHEDULE_MESSAGE = 'CREATE_NEW_SCHEDULE_MESSAGE';
export const DELETE_SCHEDULE_MESSAGE = 'DELETE_SCHEDULE_MESSAGE';
export const UPDATE_SCHEDULE_MESSAGE = 'UPDATE_SCHEDULE_MESSAGE'; 
export const FETCH_SCHEDULE_MESSAGES = 'FETCH_SCHEDULE_MESSAGES';  
export const UPDATE_STATUS = 'UPDATE_STATUS';


const createSchedualMessage = (newScheduleMsg,dispatch) => {
    const currUser = firebase.auth().currentUser;
    return new Promise((resolve, reject) => {
        if(newScheduleMsg.datetime == null) {
            newScheduleMsg.datetime = new Date().getTime();
            newScheduleMsg.inProccess = false;
            newScheduleMsg.sendNow = false;
        }else {
            newScheduleMsg.sendNow = true;
        } 
        return firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/status`)
        .once('value', snapshot => {
            if(snapshot.val()){
                let {totalUsed,totalPurchase,totalLeft} = snapshot.val();
                totalUsed += Number.parseInt(newScheduleMsg.mapedSendTo.length);
                totalLeft = totalPurchase - totalUsed;
                if(totalLeft >= 0){
                    firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/status`)
                    .set(fixNegativeValues({totalUsed,totalPurchase,totalLeft}))
                    .then(() => dispatch({type: UPDATE_STATUS, status: fixNegativeValues({totalUsed,totalPurchase,totalLeft})}))
                    firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/list`)
                    .push(newScheduleMsg)
                    .then((snapshot2) => {
                        axios.get(`https://us-central1-wedding-app-a228e.cloudfunctions.net/sendScheduleMessage?scheduleMsgId=${snapshot2.key}&userId=${currUser.uid}`)
        
                        dispatch({type: CREATE_NEW_SCHEDULE_MESSAGE, newScheduleMsg, id: snapshot2.key})
                    })

                    resolve()
                }
                else reject('לא ניתן לתזמן הודעה - אין יתרה מספקה')
            } else {
                firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/status`)
                .set({totalUsed:0,totalPurchase:0,totalLeft:0})
                .then(() => dispatch({type: UPDATE_STATUS, status: fixNegativeValues({totalUsed:0,totalPurchase:0,totalLeft:0})}))
                reject('לא ניתן לתזמן הודעה - לא קיימת חבילה בתוקף')
            }
        })

    })
}


export const startCreateSchedualMessage = (newScheduleMsg) => {
    return(dispatch) => createSchedualMessage(newScheduleMsg,dispatch)
}

const deleteScheduleMessage = (msgId,dispatch) => {
    const currUser = firebase.auth().currentUser;
    return new Promise((resolve,reject) => {
        firebase.database().ref(`users/${currUser.uid}/scheduleMsgs`)
        .once('value', snapshot => {
            let {totalUsed,totalPurchase,totalLeft} = snapshot.val().status;

            if(!snapshot.val().list || !snapshot.val().list[msgId]) reject();
            else {
                const currMsg = snapshot.val().list[msgId];
                totalUsed = totalUsed - currMsg.mapedSendTo.length 
                totalLeft = totalPurchase - totalUsed;
                if(currMsg.inProccess){
                    firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/status`)
                    .set(fixNegativeValues({totalUsed,totalPurchase,totalLeft}))
                    .then(() => dispatch({type: UPDATE_STATUS, status: fixNegativeValues({totalUsed,totalPurchase,totalLeft})}))
                    axios.get(`https://us-central1-wedding-app-a228e.cloudfunctions.net/cancelScheduleMessage?scheduleMsgId=${msgId}&userId=${currUser.uid}`)
    
                    firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/list/${msgId}`)
                    .set(null)
                    .then(() => dispatch({type: DELETE_SCHEDULE_MESSAGE,id: msgId}))    
                    resolve();
                } else reject('לא ניתן למחוק הודעה');
            }
        })    
    })
}

export const startDeleteScheduleMessage = (msgId) => {
    return(dispatch) => deleteScheduleMessage(msgId,dispatch);
}


export const startUpdateScheduleMessage = (update,msgId) => {
    return(dispatch) => {
        const currUser = firebase.auth().currentUser;
        return new Promise((resolve,reject) => {
            return firebase.database().ref(`users/${currUser.uid}/scheduleMsgs`)
            .once('value', snapshot => {
                if(snapshot.val() && snapshot.val().status){
                    deleteScheduleMessage(msgId, dispatch)
                    .then(() => {
                        const newMsg = snapshot.val();
                        Object.assign(newMsg,update);
                        createSchedualMessage(newMsg,dispatch)
                        .catch((error) => reject(error))
                    })
                    .catch(error => {
                        console.error(error)
                        reject(error)
                    })
                }
                else {
                    firebase.database().ref(`users/${currUser.uid}/scheduleMsgs/status`)
                    .set({totalUsed:0,totalPurchase:0,totalLeft:0})
                    .then(() => dispatch({type: UPDATE_STATUS, status: {totalUsed:0,totalPurchase:0,totalLeft:0}}))
                    reject('לא ניתן לעדכן הודעה מתוזמנת - לא קיימת חבילה בתוקף')
                }
            })
        })

    }
}


const fixNegativeValues = ({totalUsed,totalPurchase,totalLeft}) => {
    return {
        totalUsed: totalUsed < 0 ? 0 : totalUsed,
        totalPurchase: totalPurchase < 0 ? 0 : totalPurchase,
        totalLeft: totalLeft < 0 ? 0 : totalLeft
    }
}