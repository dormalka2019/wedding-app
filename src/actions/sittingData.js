import * as firebase from 'firebase';

export const SET_SITTING_DATA = 'SET_SITTING_DATA';


export const startSetData = (sittingData) => {
    return(dispatch) => {
        const user = firebase.auth().currentUser;
        dispatch({type: SET_SITTING_DATA, sittingData});
        return firebase.database().ref(`users/${user.uid}/sittingData`).set(sittingData)
    }
}
