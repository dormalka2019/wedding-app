import * as firebase from 'firebase';
import { SET_INVITED_LIST } from './invitedsData';
import { SET_BUDGET_LIST } from './budgetData';
import { SET_SITTING_DATA } from './sittingData';
import { FETCH_SCHEDULE_MESSAGES } from './messaging';
import { FETCH_INVITATIONS } from './invitations';
import { convertToArr, hendleUndefiendJSON } from '../helpers/fucntions';
import { isMobile } from '../helpers/device-detect';



export const login = (uid) => {
    return(dispatch) => {
        dispatch({type: 'IN_LOAD'})
        firebase.database().ref(`users/${uid}`)
        .once('value',snapshot => {
            const user = snapshot.val()
            user.emailVerified = firebase.auth().currentUser.emailVerified;
            dispatch({type:'LOGIN', user })
            if(user.invitedlist) dispatch({type: SET_INVITED_LIST, invitedlist: user.invitedlist})
            if(user.budgetlist) dispatch({type: SET_BUDGET_LIST, budgetlist: user.budgetlist})
            if(user.sittingData) dispatch({type: SET_SITTING_DATA, sittingData: user.sittingData})
            if(user.scheduleMsgs) dispatch({type: FETCH_SCHEDULE_MESSAGES, scheduleMsgs: user.scheduleMsgs})

            firebase.database().ref('invitations').orderByChild('creator').equalTo(uid)
            .once('value',snapshot => {
                if(snapshot.val()) dispatch({type: FETCH_INVITATIONS, invitations: convertToArr(snapshot.val())})
            })
            generateMessagingToken(uid);
            if(!user.isAdmin){
                firebase.database().ref(`users/${uid}/lastLogin`)
                .set(new Date().getTime())    
            }
        })
    }
}

export const logout = () => {
    return(dispatch) => {
        return firebase.auth().signOut()
        .then(() =>dispatch({type: 'LOGOUT'}))
    }
}

export const startSignIn = (email,password) => {
    return() => {
        return firebase.auth().signInWithEmailAndPassword(email,password)
    }
}

export const startSignUp = (email,password) => {
    return() => {
        return firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(() => {
            firebase.auth().currentUser.sendEmailVerification();
        })

    }
}

export const startSignInWithGoogle = () => {
    return() => {
        var provider = new firebase.auth.GoogleAuthProvider();
        return firebase.auth().signInWithPopup(provider)
    }
}

export const updateUser = (user) => {
    return(dispatch) => {
        return firebase.database().ref(`users/${user.uid}`).update(hendleUndefiendJSON(user))
        .then(snapshot => {
            dispatch({type: 'UPDATE_USER', user})
        })    
    }
}


export const sendVarificationMail = () => {
    return firebase.auth().currentUser.sendEmailVerification();
}


export const generateMessagingToken = (uid) => {
    firebase.messaging().requestPermission();
    firebase.messaging().getToken().then((msgToken) => {
        if(isMobile) firebase.database().ref(`tokens/${uid}/msgTokenMobile`).set(msgToken)
        else firebase.database().ref(`tokens/${uid}/msgToken`).set(msgToken)
    }, (error) => console.error(error))

}