import * as firebase from 'firebase';
export const CREATE_INVITATION = 'CREATE_INVITATION';
export const UPDATE_INVITATION = 'UPDATE_INVITATION';
export const DELETE_INVITATION = 'DELETE_INVITATION';
export const FETCH_INVITATIONS = 'FETCH_INVITATIONS';


export const startCreateInvitation = (invitation) => {
    return(dispatch) => {
        return new Promise((resolve,reject) => {
            invitation['createdAt'] = new Date().toJSON();
            const key = new Date().getTime().toString(36);
            firebase.database().ref(`invitations/${key}`).set(invitation)
            .then((snapshot) => {
                dispatch({type: CREATE_INVITATION, invitation: {...invitation, id: key}})
                resolve(key)
            })
        })
    }
}


export const updateInvitaion = (update, invitationId) => {
    return(dispatch) => {
        return new Promise((resolve,reject) => {
            update['updatedAt'] = new Date().toJSON();
            firebase.database().ref(`invitations/${invitationId}`)
            .update(update)
            .then((snapshot) => {
                dispatch({type: UPDATE_INVITATION, update, invitationId})
                resolve(snapshot)
            })
        })
    }
}

export const deleteInvitation = (invitationId) => {
    return(dispatch) => {
        return new Promise((resolve,reject) => {
            firebase.database().ref(`invitations/${invitationId}`).set(null)
            .then(() => {
                dispatch({type: DELETE_INVITATION, invitationId})
                resolve()
            })
            .catch(() => reject())
        }) 
    }
}