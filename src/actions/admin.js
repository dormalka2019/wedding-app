import * as firebase from 'firebase';
import axios from 'axios';

import { login } from './auth';

export const LOGIN_AS = 'LOGIN_AS';
export const FETCH_ALL_USERS = 'FETCH_ALL_USERS';

export const startLoginAs = (userId) => {
    return(dispatch) => {
        dispatch(login(userId));
        dispatch({type: LOGIN_AS, adminId: firebase.auth().currentUser.uid})
    }
}

export const startEndLoginAs = () => {
    return (dispatch) => {
        const currUser = firebase.auth().currentUser;
        dispatch(login(currUser.uid));
        dispatch({type: LOGIN_AS, adminId: null})

    }
}


export const startDeleteScheduleMessageOfUser = (msgId,userId) => {
    return(dispatch) => {
        return new Promise((resolve,reject) => {
            firebase.database().ref(`users/${userId}/scheduleMsgs`)
            .once('value', snapshot => {
                let {totalUsed,totalPurchase,totalLeft} = snapshot.val().status;
                if(!snapshot.val().list || !snapshot.val().list[msgId]) reject();
                else {
                    const currMsg = snapshot.val().list[msgId];
                    totalUsed = totalUsed - currMsg.mapedSendTo.length 
                    totalLeft = totalPurchase - totalUsed;
                    firebase.database().ref(`users/${userId}/scheduleMsgs/status`)
                    .set(fixNegativeValues({totalUsed,totalPurchase,totalLeft}))
                    .then(() => dispatch({type: 'UPDATE_STATUS', status: fixNegativeValues({totalUsed,totalPurchase,totalLeft})}))
                    axios.get(`https://us-central1-wedding-app-a228e.cloudfunctions.net/cancelScheduleMessage?scheduleMsgId=${msgId}&userId=${userId}`)
    
                    firebase.database().ref(`users/${userId}/scheduleMsgs/list/${msgId}`)
                    .set(null)
                    .then(() => dispatch({type: 'DELETE_SCHEDULE_MESSAGE',id: msgId}))    
                }
            })    
        })
    }
}


export const startSetListForUser = (invitedlist,userId) => {
    return(dispatch) => {
        return firebase.database().ref(`users/${userId}/invitedlist`).set(invitedlist)
        .then((res) => dispatch({type: 'SET_INVITED_LIST', invitedlist})) 
    }
}




const fixNegativeValues = ({totalUsed,totalPurchase,totalLeft}) => {
    return {
        totalUsed: totalUsed < 0 ? 0 : totalUsed,
        totalPurchase: totalPurchase < 0 ? 0 : totalPurchase,
        totalLeft: totalLeft < 0 ? 0 : totalLeft
    }
}


export const getMessageBalance = async () => {
    let respons = await axios.get('https://us-central1-wedding-app-a228e.cloudfunctions.net/getMessageBalance');
    respons = respons.data.split('<getBalanceResult>');
    respons = respons[1].split('</getBalanceResult>');
    return respons[0];
}