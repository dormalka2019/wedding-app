import * as firebase from 'firebase';

export const SET_BUDGET_LIST = 'SET_BUDGET_LIST';


export const startSetList = (budgetlist) => {
    return(dispatch) => {
        const user = firebase.auth().currentUser;
        return firebase.database().ref(`users/${user.uid}/budgetlist`).set(budgetlist)
        .then((res) => dispatch({type: SET_BUDGET_LIST, budgetlist}))     
    }
}
