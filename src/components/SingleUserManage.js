import React,{useState,useEffect} from 'react';
import * as firebase from 'firebase'
import { formatDate, convertToArr } from '../helpers/fucntions';
import { siteConfigs } from '../shared/siteConfigs';
import { Link } from 'react-router-dom';


const exportPurchasesData = (val) =>{
    return siteConfigs.packages.filter((p) => p.api === val)[0]
}

export default (props) => {

    const [currUser, setCurrUser] = useState(null)
    useEffect(() => {
        const userId = props.match.params.id; 
        if(currUser == null){
            firebase.database().ref(`users/${userId}`)
            .on('value', snapshot => setCurrUser(snapshot.val()))
        } else {
            // console.log(currUser)
        }
        return(() => {
            firebase.database().ref(`users/${userId}`).off();
        })
    })


    const retrivePurchasesData = (data) => {
        if(!data) return [];
        return data.map((purchas) => exportPurchasesData(purchas))
    }

    const calculatTotalPriceOfPurchases = () => {
        let total = 0;
        retrivePurchasesData(currUser.purchases)
        .forEach((p) => total+=p.price)
        return total;
    }

    const calculateBadget = () => {
        let total = 0;
        const {budgetlist} = currUser;
        for(let obj in budgetlist){
            // eslint-disable-next-line no-loop-func
            budgetlist[obj].forEach((row) => {
                if(row['perPerson']){
                    total += (Number.parseInt(row['price'])* calculateNumOfInveted('totalCount'));
                } else {
                    total += (Number.parseInt(row['price']) );
                }
                
            })
        }
        return total;
    }

    const calculateNumOfInveted = (field) =>{
        let total = 0;
        const {invitedlist} = currUser;
        invitedlist.forEach(person => total += Number.parseInt(person[field]))
        return total;
    }

    const calculateNumOfApprovedInveted = (field) =>{
        let total = 0;
        const {invitedlist} = currUser;
        invitedlist.filter(person => person['approved'] !== 0)
        .forEach(person => total += Number.parseInt(person[field]))
        return total;
    }

    const calculateCountSentScheduleMsgs = () => {
        const {scheduleMsgs} = currUser;
        scheduleMsgs.list = convertToArr(scheduleMsgs.list)
        if(scheduleMsgs.list){
            return scheduleMsgs.list.filter((msg) => msg.success)
        } else {
            return [];
        }
    }
    const calculateCountFaildSchedualMsgs = () => {
        const {scheduleMsgs} = currUser;
        scheduleMsgs.list = convertToArr(scheduleMsgs.list)
        if(scheduleMsgs.list){
            return scheduleMsgs.list.filter((msg) => msg.error === true);
        } else {
            return [];
        }
    }


    const updateSchedulePurchase = (value) =>{
        const userId = props.match.params.id;
        if(value < 0) value = 0;
        var totalLeft = value - currUser.scheduleMsgs.status.totalUsed;
        if(totalLeft < 0) totalLeft = 0;
        const ScheduleStatus = {
            totalPurchase: value,
            totalLeft
        } 
        firebase.database().ref(`users/${userId}/scheduleMsgs/status`)
        .update(ScheduleStatus)
        .then(() => {
            const updatedUser = {
                ...currUser,
                scheduleMsgs: {
                    ...currUser.scheduleMsgs,
                    status: {
                        ...currUser.scheduleMsgs.status,
                        ...ScheduleStatus
                    }
                }
            }
            setCurrUser(updatedUser)
        })
    }

    return(
        <div id="single_user_manage" className="container">
            {
                currUser == null? <img className="_loader" src="\assets\images\loader.gif" alt="loader"/>:
                (
                    <div>
                        <h1 className="title">המשתמש: {currUser.displayName} </h1>
                        <h3>פרטי המשתמש</h3>
                        <section className="box row">
                            <article>
                                <div><b>אימייל: </b> {currUser.email}</div> 
                                <div><b>טלפון: </b> {currUser.phone}</div> 
                            </article>
                            <article>
                                <div><b>סוג האירוע: </b> {currUser.eventType === 'אחר'? currUser.otherEventType : currUser.eventType}</div> 
                                <div><b>שם האולם: </b> {currUser.hallName}</div> 
                                <div><b>תאריך האירוע: </b> {formatDate(currUser.datetime)}</div> 
                            </article>
                        </section>
                        <h3>ניהול האירוע</h3>
                        <section className="box row">
                            <article>
                                <div><b>תקציב האירוע: </b>{calculateBadget()}</div>
                                <div><b>מס' המוזמנים לאירוע: </b>{calculateNumOfInveted('totalCount')}</div>
                                <div>מתוכם <span>{calculateNumOfInveted('approved')}</span> <b>אישרו הגעה</b></div>
                                <div>באירוע ישתתפו <span>{calculateNumOfApprovedInveted('children')}</span> <b>ילדים</b></div>
                                <div><span>{calculateNumOfApprovedInveted('vegetarian')}</span> <b>צמחוניים</b> ו<span>{calculateNumOfApprovedInveted('vegan')}</span> <b>טבעוניים</b></div>
                            </article>
                            <article>
                                <div><b>מס' הודעות מתוזמנות: </b> {currUser.scheduleMsgs.list? convertToArr(currUser.scheduleMsgs.list).length : 0}</div>
                                <div>מתוכן <span>{calculateCountSentScheduleMsgs().length}</span> <b>נשלחו</b></div>
                                <div>ו- <span>{calculateCountFaildSchedualMsgs().length}</span> <b>נכשלו</b> {calculateCountFaildSchedualMsgs().length > 0? <Link to={`/logs?relatedTo=${currUser.uid}`}>פירוט</Link>:null}</div>
                                <div><b>מס' הודעות שנרכשו (עבור אורח): </b> {currUser.scheduleMsgs.status? currUser.scheduleMsgs.status.totalPurchase : 0}</div>
                                <div><b>מס' הודעות שנוצלו (עבור אורח):</b> {currUser.scheduleMsgs.status? currUser.scheduleMsgs.status.totalUsed : 0}</div>
                                <div><b>מס' הודעות שנותרו (עבור אורח):</b> {currUser.scheduleMsgs.status? currUser.scheduleMsgs.status.totalLeft : 0}</div>
                                <button className="btn"
                                        onClick={() => updateSchedulePurchase(currUser.scheduleMsgs.status.totalPurchase - 50)}>50 <i className="fas fa-minus"></i></button>
                                <button className="btn"
                                        onClick={() => updateSchedulePurchase(currUser.scheduleMsgs.status.totalPurchase + 50)}>50 <i className="fas fa-plus"></i></button>

                            </article>
                        </section>
                        <h3 className="center">רכישות אחרונות</h3>
                        <table className="table purchases_table">
                            <thead>
                                <tr>
                                    <th>שם החבילה</th>
                                    <th>מחיר</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    retrivePurchasesData(currUser.purchases)
                                    .map((purchas,index) => (
                                        <tr key={index}>
                                            <td>{purchas.name}</td>
                                            <td>{purchas.price}₪</td>
                                        </tr>
                                    ))
                                }
                                <tr>
                                    <td><b>סה"כ</b></td>
                                    <td><b>{calculatTotalPriceOfPurchases()}₪</b></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                )
            }
        </div>
)} 