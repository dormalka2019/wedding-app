import React from 'react';
import {formatDate} from '../../helpers/fucntions'; 
import {isMobile} from '../../helpers/device-detect';
import { Tooltip } from '../commons';

export default ({scheduals,onDeleteScheduleMessage,onEditScheduleMessage,onResendFaildScheduleMsg, isAdmin}) => {
    return(
        <table id="scheduals" className="table table-hover">
            <thead>
                <tr>
                    {!isMobile&&<th>מזהה הודעה</th>}
                    <th>תזמון תאריך ושעה</th>
                    <th>פעולות נוספות</th>
                </tr>
            </thead>
            <tbody>
                {
                    scheduals.map((row) => 
                    <tr key={row.id}>
                        {!isMobile&&<td>{row.id}</td>}
                        <td>{formatDate(row.datetime)}</td>
                        <td>
                        {
                            ((row.sendNow && (row.inProccess === undefined || row.inProccess)) || row.inProccess) &&
                            <i className="far fa-edit"
                                onClick={() => onEditScheduleMessage(Object.assign(row,{viewMode: false}))}></i>
                        }
                        {
                            ((row.sendNow && (row.inProccess === undefined || row.inProccess)) || (isAdmin || row.inProccess)) &&
                            <i className="fas fa-trash"
                                onClick={() => onDeleteScheduleMessage(row.id)}></i>
                        }
                        {
                            (row.sendNow && (row.inProccess === undefined || row.inProccess)) || (row.inProccess !== undefined && row.inProccess) ? 
                            <i className="far fa-clock"></i>:
                            row.error? 
                            <React.Fragment>
                                <Tooltip text="חלק מההודעות לא הגיעו ליעדם - צור קשר עם השירות לקוחות לקבלת פרטים נוספים">
                                    <i  className="fas fa-exclamation-triangle"
                                        onClick={() => onEditScheduleMessage(Object.assign(row,{viewMode: true}))}></i>
                                </Tooltip>{
                                    !row.rerun?
                                    <Tooltip text="שליחה חוזרת של הודעות שנכשלו">
                                    <i  className="fas fa-redo"
                                        onClick={() => onResendFaildScheduleMsg(row)}></i>
                                    </Tooltip>: true
                                }
                            </React.Fragment>:
                            <i  className="far fa-check-circle"
                                onClick={() => onEditScheduleMessage(Object.assign(row,{viewMode: true}))}></i>
                        }
                       </td>
                    </tr>)
                }
            </tbody>
        </table>
    )
}