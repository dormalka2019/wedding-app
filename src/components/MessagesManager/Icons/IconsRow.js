import React, { useState } from 'react';

import { copyToClipboard } from '../../../helpers/fucntions';
import { icons_array } from './icons';




export default (props) => {
    const [blockToShow, setBlockToShow] = useState('none')


    return(
        <div className="icons_row">
            <div className="top">
                <div onClick={() => blockToShow !=='smaileys'? setBlockToShow('smaileys'):setBlockToShow('none')}>סמיילים</div>
                <div onClick={() => blockToShow !=='peoples'? setBlockToShow('peoples'):setBlockToShow('none')}>אנשים</div>
                <div onClick={() => blockToShow !=='animals'? setBlockToShow('animals'):setBlockToShow('none')}>חיות</div>
                <div onClick={() => blockToShow !=='plants'? setBlockToShow('plants'):setBlockToShow('none')}>צמחים</div>
                <div onClick={() => blockToShow !=='nature'? setBlockToShow('nature'):setBlockToShow('none')}>טבע</div>
                <div onClick={() => blockToShow !=='food'? setBlockToShow('food'):setBlockToShow('none')}>אוכל</div>
                <div onClick={() => blockToShow !=='activity'? setBlockToShow('activity'):setBlockToShow('none')}>פעילויות</div>
                <div onClick={() => blockToShow !=='travel'? setBlockToShow('travel'):setBlockToShow('none')}>טיולים</div>
                <div onClick={() => blockToShow !=='objects'? setBlockToShow('objects'):setBlockToShow('none')}>אובייקטים</div>
                <div onClick={() => blockToShow !=='symbols'? setBlockToShow('symbols'):setBlockToShow('none')}>סמלים</div>
                <div onClick={() => blockToShow !=='links'? setBlockToShow('links'):setBlockToShow('none')} className="links">קישור להזמנה</div>
            </div>
            <div className="container">
            {
                blockToShow !== 'links'?
                icons_array[blockToShow].map((icon,index) => 
                    <input  value={icon} 
                            id={`smaili_${index}`}
                            key={index}
                            className="pickicon"
                            readOnly
                            onChange={()=>{}} 
                            onClick={() => props.onAddIcon(copyToClipboard(`smaili_${index}`))}/>)
                :
                props.invitations.map((invitaion, index) => 
                    <div    onClick={() => props.onAddIcon(`[%-${invitaion.id}-%]`)}
                            className="linkspicker"
                            key={index}>
                        {invitaion.nameid}
                    </div>
                )
            }
            </div>
        </div>
    )
}