const smaileys = ['😀','😁','😂','🤣','😃','😄','😅','😆','😉','😊','😋','😎','😍','😘','😗',
'😙','😚','🙂','🤗','🤩','🤔','🤨','😐','😑','😶','🙄','😏','😣','😥','😮','🤐','😯','😪',
'😫','😴','😌','😛','😜','😝','🤤','😒','😓','😔','😕','🙃','🤑','😲','🙁','😖','😞','😟',
'😤','😢','😭','😦','😧','😨','😩','🤯','😬','😰','😱','😳','🤪','😵','😡','😠','🤬','😷',
'🤒','🤕','🤢','🤮','🤧','😇','🤠','🤡','🤥','🤫','🤭','🧐','🤓','😈','👿','👹','👺','💀',
'👻','👽','🤖','💩']

const peoples = ['👦','👶','👧','👨','👩','👴','👵','👾','👨‍⚕️','👩‍⚕️','👨‍🎓','👩‍🎓','👨‍⚖️','👩‍⚖️','👨‍🌾','👩‍🌾'
,'👨‍🍳','👩‍🍳','👨‍🔧','👩‍🔧','👨‍🏭','👩‍🏭','👨‍💼','👩‍💼','👨‍🔬','👩‍🔬','👨‍💻','👩‍💻','👨‍🎤','👩‍🎤','👨‍🎨','👩‍🎨','👨‍✈️','👩‍✈️','👨‍🚀'
,'👩‍🚀','👨‍🚒','👩‍🚒','👮‍♂️','👮‍♀️','🕵','💂','💂‍♀️','👷','👷‍♂️','👷‍♀️','🤴','👸','👳‍♂️','👳‍♀️','👲','🧕','🧔','👱‍♂️'
,'👱‍♀️','🤵','👰','🤰','🤱','👼','🎅','🤶','🧙‍♀️','🧙‍♂️','🧚‍♀️','🧚‍♂️','🧛‍♀️','🧛‍♂️','🧜‍♀️','🧜‍♂️','🧝‍♀️','🧝‍♂️','🧞‍♀️'
,'🧞‍♂️','🧟‍♀️','🧟‍♂️','🙍','🙍‍♂️','🙎','🙎‍♀️','🙅','🙅‍♂️','🙆','🙆‍♂️','💁','💁‍♂️','🙋','🙋‍♂️','🙇','🙇‍♀️','🤦','🤦‍♂️'
,'🤷','🤷‍♂️','💆','💆‍♂️','💇‍♂️','💇‍♀️','🚶','🚶‍♀️','🏃','🏃‍♀️','💃','🕺','👯‍♂️','👯‍♀️','🧖‍♀️','🧖‍♂️','🕴','🗣','👤','👥'
,'👫','👬','👭','💏','👨‍❤️‍💋‍👨','💑','👪','👨‍👩‍👦','👨‍👩‍👧','👨‍👩‍👧‍👦','👨‍👩‍👦‍👦','👨‍👩‍👧‍👧','👨‍👨‍👦','👨‍👨‍👧','👨‍👨‍👧‍👦','👨‍👨‍👦‍👦','👨‍👨‍👧‍👧','👩‍👩‍👦'
,'👩‍👩‍👧','👩‍👩‍👧‍👦','👩‍👩‍👦‍👦','👩‍👩‍👧‍👧','👨‍👦','👨‍👦‍👦'];

const animals = [
'😺','😸','😹','😻','😼','😽','🙀','😿','😾','🙈','🙉','🙊','🐵','🐒','🦍','🐶'
,'🐕','🐩','🐺','🦊','🐱','🐈','🦁','🐯','🐅','🐆','🐴','🐎','🦄','🦓','🐮','🐂'
,'🐃','🐄','🐷','🐖','🐗','🐽','🐏','🐑','🐐','🐪','🐫','🦒','🐘','🦏','🐭','🐁'
,'🐀','🐹','🐰','🐇','🐿','🦔','🦇','🐻','🐨','🐼','🐾','🦃','🐔','🐓','🐣','🐤'
,'🐥','🐦','🐧','🕊','🦅','🦆','🦉','🐸','🐊','🐢','🦎','🐲','🐉','🦕','🦖','🐳'
,'🐋','🐬','🐟','🐠','🐡','🦈','🐙','🐚','🦀','🦐','🦑','🐌','🦋','🐛','🐜','🐝'
,'🐞','🦗','🕷','🕸','🦂']


const plants = [
'💐','🌸','💮','🏵','🌹','🥀','🌺','🌻'
,'🌼','🌷','🌱','🌲','🌳','🌴','🌵','🌾','🌿','☘','🍀','🍁','🍂','🍃','🍄','🌰']


const nature = [
'🌍','🌎','🌏','🌐','🌑','🌒','🌓','🌔','🌕','🌖','🌗','🌘','🌙','🌚','🌛','🌜'
,'☀','🌝','🌞','⭐','🌟','🌠','☁','⛅','⛈','🌤','🌥','🌦','🌧','🌨','🌩','🌪'
,'🌫','🌬','🌈','☔','⚡','❄','☃','⛄','☄','🔥','💧','🌊','🎄','✨','🎋','🎍']

const food = [
'🍇','🍈','🍉','🍊','🍋','🍌','🍍','🍎','🍏','🍐','🍑','🍒','🍓','🥝','🍅','🥥'
,'🥑','🍆','🥔','🥕','🌽','🌶','🥒','🥦','🥜','🍞','🥐','🥖','🥨','🥞','🧀','🍖'
,'🍗','🥓','🍔','🍟','🍕','🌭','🥪','🌮','🌯','🍳','🍲','🥣','🥗','🍿','🥫','🍱'
,'🍘','🍙','🍚','🍛','🍜','🍝','🍠','🍢','🍣','🍤','🍥','🍡','🥟','🥠','🥡','🍦'
,'🍧','🍨','🍩','🍪','🎂','🍰','🥧','🍫','🍬','🍭','🍮','🍯','🍼','🥛','☕','🍵'
,'🍶','🍾','🍷','🍸','🍹','🍺','🍻','🥂','🥃','🥤','🥢','🍽','🍴','🥄'
];


const activity = [
'🏇','⛷','🏂','🧗‍♀️','🧗‍♂️','🧘‍♀️','🧘‍♂️','🏌️‍♂️','🏌️‍♀️','🏄‍♂️','🏄‍♀️','🚣‍♂️','🚣‍♀️','🏊‍♂️','🏊‍♀️','⛹️‍♂️'
,'⛹️‍♀️','🏋','🏋️‍♂️','🏋️‍♀️','🚴','🚴‍♀️','🚵','🚵‍♀️','🤸','🤸‍♂️','🤼','🤼‍♂️','🤼‍♀️','🤽‍♂️','🤽‍♀️'
,'🤾‍♂️','🤾‍♀️','🤹‍♂️','🤹‍♀️','🎪','🎗','🎟','🎫','🎖','🏆','🏅','🥇','🥈','🥉','⚽'
,'⚾','🏀','🏐','🏈','🏉','🎾','🎳','🏏','🏑','🏒','🏓','🏸','🥊','🥋','⛳'
,'⛸','🎣','🎽','🎿','🛷','🥌','🎯','🎱','🎮','🎰','🎲','🎭','🎨','🎼','🎤','🎧'
,'🎷','🎸','🎹','🎺','🎻','🥁','🎬','🏹'
];


const travel = [
'🏖','🏎','🏍','🗾','🏔','⛰','🌋','🗻','🏕','🏜','🏝','🏞','🏟','🏛','🏗','🏘'
,'🏚','🏠','🏡','🏢','🏣','🏤','🏥','🏦','🏨','🏩','🏪','🏫','🏬','🏭','🏯','🏰'
,'💒','🗼','🗽','⛪','🕌','🕍','⛩','🕋','⛲','⛺','🌁','🌃','🏙','🌄','🌅','🌆'
,'🌇','🌉','🌌','🎠','🎡','🎢','🚂','🚃','🚄','🚅','🚆','🚇','🚈','🚉','🚊','🚝'
,'🚞','🚋','🚌','🚍','🚎','🚐','🚑','🚒','🚓','🚔','🚕','🚖','🚗','🚘','🚚','🚛'
,'🚜','🚲','🛴','🛵','🚏','🛤','⛽','🚨','⛵','🚤','🛳','⛴','🛥','🚢','✈','🛩'
,'🛫','🛬','💺','🚁','🚟','🚠','🚡','🛰','🚀','🛸','⛱','🎆','🎇','🎑','🗿','🛂'
,'🛃','🛄','🛅'
]


const objects = [
'💎','👓','🕶','👔','👕','👖','🧣','🧤','🧥','🧦','👗','👘','👙','👚','👛','👜'
,'👝','🎒','👞','👟','👠','👡','👢','👑','👒','🎩','🎓','🧢','⛑','💄','💍','🌂'
,'☂','💼','☠','🛀','🛌','💌','💣','🚥','🚦','🚧','⚓','🕳','🛍','📿','🔪','🏺'
,'🗺','💈','🛢','🛎','⌛','⏳','⌚','⏰','⏱','⏲','🕰','🌡','🎈','🎉','🎊','🎎','🎏','🎐'
,'🎀','🎁','🔮','🕹','🖼','🎙','🎚','🎛','📻','📱','📲','☎','📞'
,'📟','📠','🔋','🔌','💻','🖥','🖨','⌨','🖱','🖲','💽','💾','💿','📀','🎥','🎞'
,'📽','📺','📷','📸','📹','📼','🔍','🔎','🕯','💡','🔦','🏮','📔','📕','📖','📗','📘','📙','📚','📓','📃','📜','📄','📰'
,'🗞','📑','🔖','🏷','💰','💸','💳','✉','📧','📨','📩','📤','📥','📦','📫','📪'
,'📬','📭','📮','🗳','✏','✎','🖉','✒','🖋','🖊','🖌','🖍','📝','📁','📂','🗂'
,'📅','📆','🗒','🗓','📇','📈','📉','📊','📋','📌','📍','📎','🖇','📏','📐','✂'
,'🗃','🗄','🗑','🔒','🔓','🔏','🔐','🔑','🗝','🔨','⛏','⚒','🛠','🗡',
'⚔','🔫','🛡','🔧','🔩','⚙','🗜','⚖','🔗','⛓','⚗','🔬','🔭','📡','💉','💊',
'🚪','🛏','🛋','🚽','🚿','🛁','🚬','⚰','⚱','💘','❤','💓','💔','💕','💖','💗'
,'💙','💚','💛','🧡','💜','🖤','💝','💞','💟','❣','💦','💨','💫','🏁','🚩','🎌'
,'🏴','🏳','🏳️‍🌈']

const symbols = [
'👍','👎','💪','🤳','👈','👉','☝','👆','🖕','👇','✌','🤞','🖖','🤘','🖐','✋','👌'
,'✊','👊','🤛','🤜','🤚','👋','🤟','✍','👏','👐','🙌','🤲','🙏','🤝','💅'
,'👂','👃','👣','👀','👁','🧠','👅','👄','💋','💤','💢','💬','🗯','💭','♨','🛑'
,'🕛','🕧','🕐','🕜','🕑','🕝','🕒','🕞','🕓','🕟','🕔','🕠','🕕','🕡','🕖','🕢'
,'🕗','🕣','🕘','🕤','🕙','🕥','🕚','🕦','🌀','🃏','🀄','🎴','🔇','🔈','🔉','🔊'
,'📢','📣','📯','🔔','🔕','🎵','🎶','🏧','🚮','🚰','♿','🚹','🚺','🚻','🚼','🚾'
,'⚠','🚸','⛔','🚫','🚳','🚭','🚯','🚱','🚷','🔞','☢','☣','🛐','⚛','🕉','✡'
,'☸','☯','✝','☦','☪','☮','🕎','🔯','♈','📴','♻','🔱','📛','🔰','⭕','✅'
,'☑','✔','✖','❌','‼','⁉','❓','❔','❕','❗','💯','🔸','🔹','🔺','🔻','💠'
,'🔲','🔳','⚪','⚫','🔴','🔵']

export const icons_array = {
    smaileys, 
    peoples, 
    animals , 
    plants, 
    nature,
    food, 
    activity,
    travel,
    objects,
    symbols,
    'none':[]} 