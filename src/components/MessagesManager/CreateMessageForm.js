import React from 'react';
import IconsRow from './Icons/IconsRow';
import DateTimePicker from 'react-datetime-picker/dist/DateTimePicker';
import { isMobile } from '../../helpers/device-detect'
import { AlertMessage,ButtonWithConfirmMessage } from '../commons';
export default class CreateMessageForm extends React.Component{
    constructor(props){
        super(props)
        const {editMessage,invitedsData} = props;
        this.state = {
            body: editMessage? editMessage.body : '',
            datetime: editMessage? editMessage.datetime : null,
            filtering: editMessage && editMessage.filtering? editMessage.filtering : 'all' ,
            sendTo: editMessage? invitedsData.filter(person => editMessage.mapedSendTo.includes(person.phone)) : [],
            search: '',
            addLink: editMessage && editMessage.addLink? editMessage.addLink: false,
            editMode: !!editMessage && !editMessage.resend,
            viewMode: editMessage? !!editMessage.viewMode : false,
            additionOptions: false,
            resend: editMessage? !!editMessage.resend : false,
            alert: {
                type: 'alert-danger',
                message: '',
                isShown: false
            },
        }
    }
    componentDidMount(){
        this.setFiltering(this.state.filtering)

    }
    componentWillReceiveProps(nextProps){
        const {editMessage} = nextProps;
        if(editMessage) {
            const sendTo = editMessage.mapedSendTo ? this.props.invitedsData.filter(person => editMessage.mapedSendTo.includes(person.phone)) : [];
            // this.setState({...editMessage, editMode: !!editMessage && !editMessage.resend, sendTo})
            this.setState(Object.assign(this.state,editMessage,{editMode: !!editMessage && !editMessage.resend},sendTo))
            this.setFiltering(editMessage.filtering);
        } else {
            this.setFiltering('all')
        }
    }

    setFiltering(value){
        var options = document.getElementsByClassName('optradio');
        for(var i = 0 ; i < options.length ; i++){
            options[i].checked = false;
        }
        document.getElementById(value).checked = true;
        this.setState({filtering: value})
    }

    resetForm(){
        const resetState = {
            body: '',
            datetime:  null,
            filtering: 'all',
            sendTo: [],
            search: '',
            addLink: false,
            editMode: false,
            viewMode: false,
            resend: undefined,
            alert: {
                isShown: false   
            }
        }
        this.setState(Object.assign(this.state,resetState))
        this.setFiltering('all')
    }

    checkErrors(mapedSendTo){
        const {body, datetime,addLink,filtering} = this.state;
        var errors = [];
        if(body === '') errors.push('תוכן הודעה')
        if(datetime && new Date(datetime).getTime() < new Date().getTime() ) errors.push('תאריך ושעת תזמון')
        if((filtering.indexOf('approved') === -1) && mapedSendTo.length === 0) !errors.includes('רשימת תפוצה') && errors.push('רשימת תפוצה')
        if(addLink && (!this.props.invitations || this.props.invitations.length === 0)) !errors.includes('הזמנה לקישור') && errors.push('הזמנה לקישור')
        else if(addLink && this.props.invitations.length > 1 && this.props.invitations.filter(inv => inv.default === true).length !== 1){
            !errors.includes('הזמנה כברירת מחדל לקישור') && errors.push('הזמנה כברירת מחדל לקישור')
        }
        var buildErrorMessage = 'חסרים הפרטים הבאים: ';
        errors.forEach((error,index) => buildErrorMessage += `${index !== 0? ',':''}${error}`)
        this.setState({
            alert: {
                isShown: errors.length !== 0,
                type: 'alert-danger',
                message: buildErrorMessage
            }
        })
        return errors.length === 0;
    }


    addFromIconRow(val){
        var { body } = this.state;
        body += val;
        this.setState({body})
        document.getElementById('message_body').focus()
    }

    render(){
        const {onCreateNewScheduleMessage,onUpdateScheduleMessage} = this.props;
        const {body, datetime, filtering, sendTo, search, editMode, addLink,additionOptions, viewMode ,resend} = this.state;
        const invitedsData = this.props.invitedsData.sort((p1,p2) => {
            if(p1.fullName < p2.fullName) { return -1; }
            if(p1.fullName > p2.fullName) { return 1; }
            return 0;
        })
        return(
            <form id="create_message_form" className="container">
                {viewMode && <i className="fas fa-times top" onClick={() => this.resetForm()}></i>}
                <AlertMessage {...this.state.alert}/>
                <div className="form-group-lg">
                    <label htmlFor="message_body" style={{fontSize: '1.8rem'}}>תוכן ההודעה</label>
                    <textarea   className={`form-control`} 
                                value={body}
                                id="message_body"
                                onChange={(event) => { this.setState({body:event.target.value})}} 
                                rows="5"></textarea>
                    
                    {<IconsRow onAddIcon={this.addFromIconRow.bind(this)}
                                            invitations={this.props.invitations}/>}    
                </div>
                <div className="form-group-lg">
                <div className="form-check-inline">
                    <label className="form-check-label">
                        <input  type="radio" 
                                className="form-check-input" 
                                name="dateRadio" 
                                checked={datetime == null}
                                onChange={() => this.setState({datetime: null})}/> שליחת הודעה באופן מיידי
                    </label> 
                    <label className="form-check-label">
                        <input  type="radio" 
                                className="form-check-input" 
                                name="dateRadio"
                                checked={datetime != null}
                                onChange={() => this.setState({datetime: new Date().getTime() + (3600*360)})}/> שליחת הודעה בתאריך ושעה
                    </label>
                    </div>
                </div>
                {datetime &&
                <div className="row">
                    <label htmlFor="date" className="col-sm-2 col-form-label">תזמון שליחת ההודעה</label>
                    <div style={{overflow: 'hidden'}} className="col-sm-10">
                    <DateTimePicker onChange={(newdatetime) => this.setState({datetime:new Date(newdatetime).toJSON()})}
                                        value={new Date(datetime)}
                                        id="date2"/>
                    </div>
                </div>}
                <br/>
                <small>* רשימות המספרים עדכניות למצב הנוכחי - שינוי מצבת מוזמנים לא תשפיע על הודעה שכבר תוזמנה</small>
                <div className="form-group-lg">
                    <div className="form-check-inline">
                        <label className="form-check-label">
                            <input  type="radio" 
                                    id="all"
                                    className="form-check-input optradio" 
                                    name="optradio" 
                                    onChange={() => this.setFiltering('all')}/> שליחה לכל הרשימה ({invitedsData.length})
                        </label> 
                        <label className="form-check-label">
                            <input  type="radio" 
                                    id="individual"
                                    className="form-check-input optradio" 
                                    name="optradio"
                                    onChange={() => this.setFiltering('individual')}/> בחירה מתוך הרשימה ({sendTo.length})

                        </label> 
                        {
                            !isMobile && !additionOptions &&
                            <button className="btn"
                                    onClick={(event) => {
                                        event.preventDefault();
                                        this.setState({additionOptions: !additionOptions})}}>אפשרויות נוספות</button>
                        }
                        {
                            additionOptions &&
                            <label className="form-check-label">
                                <input  type="radio" 
                                        id="not-approved"
                                        className="form-check-input optradio" 
                                        name="optradio"
                                        // eslint-disable-next-line eqeqeq
                                        onChange={() => this.setFiltering('not-approved')}/> לא אישרו הגעה ({invitedsData.filter(perosn=> perosn['approved'] == 2).length})
                            </label> 
                        }
                        {
                            additionOptions &&
                            <label className="form-check-label">
                                <input  type="radio" 
                                        id="approved"
                                        className="form-check-input optradio" 
                                        name="optradio"
                                        // eslint-disable-next-line eqeqeq
                                        onChange={() => this.setFiltering('approved')}/> אישרו הגעה ({invitedsData.filter(perosn=> perosn['approved'] == 1).length})
                            </label> 
                        }
                    </div>
                </div>
                {
                    filtering === 'individual' && 
                    <div>

                    <div className="pickpersons row">
                        <label htmlFor="search" className="col-1 col-form-label"><i className="fas fa-search"></i></label>
                        <div className="col-10">
                            <input  type="test"
                                    autoComplete="false"
                                    className="form-control"
                                    placeholder="חיפוש מוזמן"
                                    id="search"
                                    value={search}
                                    onChange={(event) => this.setState({search:event.target.value})}/>
                            <ul className="search_result">
                            {
                                search !== '' && invitedsData.filter((to,index) =>  to.phone && to.phone !== "0" && 
                                                                                    to.fullName.includes(search))
                                                .map((to,index) => (
                                                        index < 4 && <li    key={index}
                                                                className={`person ${sendTo.includes(to)&& 'include'}`}
                                                                onClick={() => sendTo.includes(to)? 
                                                                    this.setState({sendTo: sendTo.filter(inTo => inTo.phone !== to.phone)}):
                                                                    this.setState({sendTo: [...sendTo,to]})}>{to.fullName}</li>
                                                    ))
                            }
                            </ul>
                        </div>
      
                    </div>
                    <ul className="picked_person col-sm-6">
                        {
                            sendTo.map((to,index) => (
                                <li key={index}>
                                    {to.fullName}
                                    <i className="fas fa-times"
                                        onClick={() => this.setState({sendTo: sendTo.filter(inTo => inTo.phone !== to.phone)})}></i>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                }
                <center>
                    {
                        viewMode? null:
                        <ButtonWithConfirmMessage className="btn btn-primary"
                                style={{margin: '10px auto'}}
                                message={(
                                    <span><b>תזכורת!!</b> רשימת התפוצה של הודעה זו הינו קבוע ובהתאם לרשימה הנוכחית <br/> 
                                        המשמעות היא שכל <b>שינוי של פרטי מוזמן</b> לרבות <b>מספר פלאפון וסטאטוס הגעה</b> שיתבצעו לאחר תזמון הודעה זו לא יכנסו לתוקף עבור <b>הודעה זו</b><br/>
                                        במידה ותרצו להחיל את השינויים על הודעה מתוזמנת יש למחוק את ההודעה הלא רלוונטית וליצור חדשה (לאחר ש<b>עדכנם את פרטי המוזמנים!</b>)
                                    </span> )}
                                onClick={(event) => {
                                    event.preventDefault();
                                    var mapedSendTo;
                                    if(filtering === 'individual'){
                                        mapedSendTo = sendTo.map(to => to.phone);
                                    } else if(filtering === 'all') {
                                        mapedSendTo = invitedsData.map(to => to.phone);
                                    } else if(filtering === 'approved'){
                                        // eslint-disable-next-line eqeqeq
                                        mapedSendTo = invitedsData.filter(to=> to.approved == 1).map(to => to.phone);
                                    } else if(filtering === 'not-approved'){
                                        // eslint-disable-next-line eqeqeq
                                        mapedSendTo = invitedsData.filter(to=> to.approved == 2).map(to => to.phone);                                    
                                    } else {
                                        mapedSendTo =[]
                                    }
                                    if (this.checkErrors(mapedSendTo)) {
                                        !editMode? 
                                        onCreateNewScheduleMessage({body,datetime,mapedSendTo,addLink,filtering,resend}):
                                        onUpdateScheduleMessage({body,datetime,mapedSendTo,addLink,filtering,resend,id:this.props.editMessage.id})
                                        this.resetForm();                                
                                    }
                                }}>
                            {`${editMode? 'עדכון הודעה מתוזמנת' : datetime?'ביצוע תזמון הודעה': 'שליחת הודעה באופן מיידי'}`}
                        </ButtonWithConfirmMessage>
                    }
                </center>
            </form>

        )
    }
}



