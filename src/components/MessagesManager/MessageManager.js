import React from 'react';
import CreateMessageForm from './CreateMessageForm';
import {    startCreateSchedualMessage,
            startDeleteScheduleMessage,
            startUpdateScheduleMessage } from '../../actions/messaging';
import { startDeleteScheduleMessageOfUser } from '../../actions/admin';
import { connect } from 'react-redux';
import { ToggleBar } from '../commons';
import ScheduleMessagesTable from './ScheduleMessagesTable'
import { AlertMessage } from '../commons';
import MessagesStatus from './MessagesStatus';
import { Link  } from 'react-router-dom';



export class MessageManagerCls extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            invitedsData: props.invitedsData || [],
            editMessage: null,
            alert: {
                type: 'alert-primary',
                message: '',
                isShown: false
            },
        }
    }

    componentWillUpdate(nextProps, nextState){
        if(nextProps.invitedsData !== this.state.invitedsData) this.setState({invitedsData:nextProps.invitedsData})
    }

    onResendFaildScheduleMsg(scheduleMsg){
        const newScheduleMsg = {
            datetime: null,
            mapedSendTo: scheduleMsg.failsNumbers,
            body: scheduleMsg.body,
            addLink: scheduleMsg.addLink,
            filtering: 'individual',
            resend: scheduleMsg.id
        }
        this.onEditScheduleMessage(newScheduleMsg);
    }

    onCreateNewScheduleMessage(scheduleMsg){
        scheduleMsg.resend = !!scheduleMsg.resend;
        this.props.startCreateSchedualMessage(scheduleMsg)
        .then((data) => {
            if(scheduleMsg.resend){
                var updateMsg = this.props.schedualsMsgList.filter(msg => msg.id === scheduleMsg.resend)[0];
                updateMsg.rerun = true;
                this.onUpdateScheduleMessage(updateMsg)
            }
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'יצירה בוצעה בהצלחה'
                }
            })
        })    
        .catch(error => {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: error
                }
            })
        })
    }

    onDeleteScheduleMessage(msgId) {
        if(this.props.isAdmin){
            this.props.startDeleteScheduleMessageOfUser(msgId,this.props.uid)
        } else {
            this.props.startDeleteScheduleMessage(msgId)
            .then(() => {
                this.setState({
                    alert: {
                        isShown: true,
                        type: 'alert-primary',
                        message: 'מחיקה בוצעה בהצלחה'
                    }
                })
            })
            .catch(err => {
                if(err){
                    this.setState({
                        alert: {
                            isShown: true,
                            type: 'alert-danger',
                            message: err
                        }
                    })
                }
            })
        }
 
    }

    onEditScheduleMessage(row) {
        const toggleBar = document.getElementById('messageform'); 
        const text = getComputedStyle(toggleBar,':before').getPropertyValue('content');
        if(text.indexOf('-') >= 0){
            toggleBar.click();
        }
        this.setState({editMessage: row});
    }

    onUpdateScheduleMessage(scheduleMessage){

        this.setState({editMessage: null})
        this.props.startUpdateScheduleMessage(scheduleMessage)
        .then(() => {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'עדכון בוצע בהצלחה'
                }
            })
        })
        .catch(error => {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: error
                }
            })
        })
    }

    render(){
        const {alert} = this.state;
        const { totalLeft } = this.props.schedualsMsgStatus;
        return (
            <div id="message_manager" className="container">
                <h1>מערכת ניהול שליחת הודעות</h1> 
                <AlertMessage isShown={totalLeft === 0} type="alert-danger">
                    נראה כי יתרת ההודעות אינה מאפשרת לך לבצע פעולות בעמוד זה <Link to="/packagestore">לרכישת חבילת הודעות</Link>
                </AlertMessage>
                <Link to="/packagestore" className="linkToStore">רכישת הודעות <i className="fas fa-store-alt"></i></Link>
                <MessagesStatus {...this.props.schedualsMsgStatus} />
                <ToggleBar  label="מתזמן הודעות" 
                            id='messageform'>
                    <CreateMessageForm {...this.state}
                                        invitations = {this.props.invitations}
                                        onCreateNewScheduleMessage={this.onCreateNewScheduleMessage.bind(this)}
                                        onUpdateScheduleMessage={this.onUpdateScheduleMessage.bind(this)}
                                        editMessage={this.state.editMessage}/>
                </ToggleBar>
                <div style={{marginTop: '50px'}}></div>
                <AlertMessage {...alert}/>
                {
                    this.props.schedualsMsgList.length > 0 &&
                    <ScheduleMessagesTable  scheduals={this.props.schedualsMsgList}
                                            isAdmin={this.props.isAdmin}
                                            onResendFaildScheduleMsg={this.onResendFaildScheduleMsg.bind(this)}
                                            onEditScheduleMessage={this.onEditScheduleMessage.bind(this)}
                                            onDeleteScheduleMessage={this.onDeleteScheduleMessage.bind(this)}/>
                }

            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startCreateSchedualMessage: (scheduleMsg) => dispatch(startCreateSchedualMessage(scheduleMsg)),
    startDeleteScheduleMessage: (msgId) => dispatch(startDeleteScheduleMessage(msgId)),
    startUpdateScheduleMessage: scheduleMsg => dispatch(startUpdateScheduleMessage(scheduleMsg,scheduleMsg.id)),
    startDeleteScheduleMessageOfUser: (msgId,userId) => dispatch(startDeleteScheduleMessageOfUser(msgId,userId))
})

const mapStateToProps = (state) => ({
    invitations: state.invitations,
    invitedsData: state.invitedsData,
    schedualsMsgList: state.schedualsMsgs.list,
    schedualsMsgStatus: state.schedualsMsgs.status,
    uid: state.userdata.uid,
    isAdmin: !!state.userdata.adminId
})


export const MessageManager = connect(mapStateToProps,mapDispatchToProps)(MessageManagerCls);