import React from 'react';

export default ({totalPurchase, totalUsed, totalLeft}) => {
    return(
            <table id="summarize_schedule_msgs" className="table">
                <thead>
                    <tr>
                        <th>סה"כ נרכשו</th>
                        <th>סה"כ נוצלו</th>
                        <th>סה"כ נותרו</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{totalPurchase}</td>
                        <td>{totalUsed}</td>
                        <td>{totalLeft}</td>
                    </tr>
                </tbody>
            </table>
    )
}