import React,{useState} from 'react';
import {sendVarificationMail} from '../actions/auth';
import {connect} from 'react-redux';

const containerStyle = {
    width: '100%',
    textAlign: 'center',
    backgroundColor: '#286fc6',
    color: 'white'
}

const linkStyle = {
    cursor: 'pointer',
    textDecoration: 'underline' 
}

const mapStateToProps = (state) => ({
    isLogin: !!state.userdata.uid,
    emailVerified: state.userdata.emailVerified
})

export const VarificationMail = connect(mapStateToProps,undefined)(({emailVerified,isLogin}) => {
    const [resend, setResend] = useState(false);
    return(
        (!emailVerified && isLogin)?
        <div style={containerStyle}>
            נראה שהאיימיל איתו נרשמתם עדין לא אומת <span>  </span>
            <span   onClick={() => {
                sendVarificationMail()
                setResend(true)
            }}
                    style={linkStyle}>
                    {
                        !resend?
                        'לשליחה של לינק לאימות':
                        'שלח שוב'
                    }
                    
            </span>
        </div>: null
    )
})