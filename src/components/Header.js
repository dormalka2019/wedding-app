import React, { useState } from 'react';
import { Link,NavLink  } from 'react-router-dom';
import { siteConfigs } from '../shared/siteConfigs';
import { connect } from 'react-redux';
import { logout } from '../actions/auth';
import { isMobile } from '../helpers/device-detect';

const HeaderFoo = (props) => {
    const [isClose, setIsClose] = useState(true);
    const {pagesToShow} = props;
    return(
        <header>
            <div className="top">
                {
                    !props.isLoggedIn?
                        <Link className="login-btn" to="/signin">התחבר/הרשם</Link>:
                        <Link className="login-btn" to="/" onClick={() => props.logout()}>ניתוק</Link>
                }
                <Link to='/' className="navbar-brand">{siteConfigs.siteName}</Link>
            </div>

            {!isMobile? (
                <nav className="none_mobile">
                    <ul>{ siteConfigs[pagesToShow].filter(item => (item.isPrivate && props.isLoggedIn) || !item.isPrivate).map((item,index) => <li key={index}><NavLink activeClassName="selected" to={item.url}>{item.label}</NavLink></li>) }</ul>
                </nav>
            ):(
                <nav className="mobile">
                    <ul>
                        <button className="menu" onClick={() => setIsClose(!isClose)}></button>
                        <div className={`background ${isClose? 'close':''}`} onClick={() => setIsClose(!isClose)}></div>
                        <div className={`links ${isClose? 'close':''}`}>
                            { siteConfigs[pagesToShow].filter(item => (item.isPrivate && props.isLoggedIn) || !item.isPrivate).map((item,index) => <li key={index}><NavLink activeClassName="selected" to={item.url} onClick={() => setIsClose(true)}>{item.label}</NavLink></li>)}
                        </div>
                    </ul>
                </nav>
            )}
        </header>
    )
}

const mapDispatchToProps = (dispatch) => ({
    logout: () => dispatch(logout())
})

const mapStateToProps = (state) => ({
    isLoggedIn: !!state.userdata.uid,
    pagesToShow: !!state.userdata.isAdmin? 'adminPages': 'pages'
})
export const Header = connect(mapStateToProps,mapDispatchToProps)(HeaderFoo)