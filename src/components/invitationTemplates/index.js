import React from 'react';
import { templatesList } from '../../shared/templatesList'
export const switchTemplate = (templateConfigs) => {
    try{
        const { templateName } = templatesList[templateConfigs.tempalteIndex];
        return  require(`./${templateName}`).template(templateConfigs);
    }catch(e){
        return (<div></div>)
    }
}