import React from 'react';

export const template = (props) => {
    const date = new Date(props.datetime);
    const bodyParsed = props.body != null ? props.body.split('\n').map((item,index) => <span key={index}>{item}<br/></span>) : <span></span>
    return(
        <div id="wedding_sea">
            <div className="innerBox" style={{backgroundImage: 'url(/assets/images/templates/wedding_sea/wedding_sea.png)'}}>
                <h1>
                    {props.prolog != null && props.prolog}
                </h1>
                <div className="body">
                    {bodyParsed}
                    {props.hall !== '' && `ב ${props.hall}`}<br/>
                    {props.datetime != null  && `בתאריך ${date.getFullYear()}\\${date.getMonth()+1}\\${date.getDate()}`}<br/>
                    {props.datetime != null  && `בשעה ${date.getHours()}:${date.getMinutes() < 10? `0`+date.getMinutes(): date.getMinutes()}`}
                </div>
                {
                    props.groomParents? 
                    (
                        <div className="groomParents">
                            <b>הורי צד א</b>
                            <span>{props.groomParents}</span>
                        </div>
                    ):
                    (
                        <div className="groomParents">
                        </div>                        
                    )
                }

                {
                    props.brideParents? 
                    (
                        <div className="brideParents">
                            <b>הורי צד ב</b>
                            <span>{props.brideParents}</span>
                        </div>
                    ):
                    (
                        <div className="brideParents">
                        </div>                        
                    )
                }
                <div className="epilog">
                    <div >
                        {props.epilog != null && props.epilog}
                    </div>
                </div>
            </div>
        </div>
    )
}


