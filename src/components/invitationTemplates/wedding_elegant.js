import React from 'react';

export const template = (props) => {
    const date = new Date(props.datetime);
    const bodyParsed = props.body != null ? props.body.split('\n').map((item,index) => <span key={index}>{item}<br/></span>) : <span></span>
    return(
        <div id="wedding_elegant">
            <div className="innerBox">
                <h1>
                    {props.prolog != null && props.prolog}
                </h1>
                <div className="body">
                    {bodyParsed}
                    {props.hall !== '' && `ב ${props.hall}`}<br/>
                    {props.datetime != null && `בתאריך ${date.getFullYear()}\\${date.getMonth()+1}\\${date.getDate()}`}<br/>
                </div>
                <div className="details">
                    {
                        props.datetime != null && (
                            <React.Fragment>
                                <div>
                                    <h3>קבלת פנים</h3>
                                    {`בשעה ${date.getHours()}:${date.getMinutes() < 10? `0`+date.getMinutes(): date.getMinutes()}`}
                                </div>
                                <div>
                                    <h3>חופה וקידושין</h3>
                                    {`בשעה ${new Date(date.getTime() +(1000 * 60 * 60)).getHours()}:${new Date(date.getTime() +(1000 * 60 * 60)).getMinutes() < 10? `0`+new Date(date.getTime() +(1000 * 60 * 60)).getMinutes(): new Date(date.getTime() +(1000 * 60 * 60)).getMinutes()}`}
                                </div>
                                <div>
                                    <h3>אוכל וריקודים</h3>
                                    {`בשעה ${new Date(date.getTime() +(1000 * 60 * 90)).getHours()}:${new Date(date.getTime() +(1000 * 60 * 90)).getMinutes() < 10? `0`+new Date(date.getTime() +(1000 * 60 * 90)).getMinutes(): new Date(date.getTime() +(1000 * 60 * 90)).getMinutes()}`}
                                </div>
                            </React.Fragment>
                        )
                    }
                </div>
                {
                    props.groomParents? 
                    (
                        <div className="groomParents">
                            <b>הורי צד א</b>
                            <span>{props.groomParents}</span>
                        </div>
                    ):
                    (
                        <div className="groomParents">
                        </div>                        
                    )
                }

                {
                    props.brideParents? 
                    (
                        <div className="brideParents">
                            <b>הורי צד ב</b>
                            <span>{props.brideParents}</span>
                        </div>
                    ):
                    (
                        <div className="brideParents">
                        </div>                        
                    )
                }
                <div className="image">
                    <img src='/assets/images/decoration1.svg' alt="wedding-decoration"/>
                </div>
                <div className="epilog">
                    {props.epilog != null && props.epilog}
                </div>
            </div>
        </div>
    )
}


