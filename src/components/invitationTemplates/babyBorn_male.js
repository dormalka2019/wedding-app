import React from 'react';

export const template = (props) => {
    const date = new Date(props.datetime);
    const bodyParsed = props.body != null ? props.body.split('\n').map((item,index) => <span key={index}>{item}<br/></span>) : <span></span>
    return(
        <div id="templateOne">
            <div className="innerBox">
                <h1>
                    {props.prolog != null && props.prolog}
                </h1>
                <div className="body">
                    {bodyParsed}
                </div>
                <div className="details">
                        {props.hall !== '' && `ב ${props.hall}`}<br/>
                        {props.datetime != null && `בתאריך ${date.getFullYear()}\\${date.getMonth()+1}\\${date.getDate()}`}<br/>
                        {props.datetime != null && `בשעה ${date.getHours()}:${date.getMinutes() < 10? `0`+date.getMinutes(): date.getMinutes()}`}
                </div>
                <div className="image">
                    <img src='/assets/images/strok_blue.png' alt="strock"/>
                </div>
                <div className="epilog">
                    {props.epilog != null && props.epilog}
                </div>
            </div>
        </div>
    )
}


