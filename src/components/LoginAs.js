import React from 'react';
import { connect } from 'react-redux';
import { startEndLoginAs } from '../actions/admin';

const mapDispatchToProps = (dispatch) => ({
    startEndLoginAs: () => dispatch(startEndLoginAs())
})

const mapStateToProp = (state) => ({
    adminId: state.userdata.adminId,
    loginAs: state.userdata.displayName
  })
  

export default connect(mapStateToProp,mapDispatchToProps)(({adminId,loginAs,startEndLoginAs}) => {
    return(
        <div id="loginAs" className={`${!!adminId? 'show':'hide'}`}>
             מחובר כ {loginAs} <span onClick={startEndLoginAs.bind(this)}>ניתוק</span>
        </div>
    )
})
