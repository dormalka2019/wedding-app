import React, {useState} from 'react'



export const ToggleBar = (props) => {
    const [open, setOpen] = useState(JSON.parse(localStorage.getItem(props.id)));


    return (
        <div className="toggle_bar">
            <div    className={`bar ${open? 'open':''}`}
                    id={props.id}
                    onClick={() => {
                        localStorage.setItem(props.id,JSON.stringify(!open))
                        setOpen(!open)
                    }}>
                {props.label}
            </div>
            <div className="body" style={open? {height: 'auto'} : {height: '0px',overflow:'hidden'}}>
                {props.children}
            </div>
        </div>
    )
}