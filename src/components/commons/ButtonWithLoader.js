import React from 'react';

const loaderGifStyle = {
    position: 'absolute',
    top:'0px', 
    left: '0px',
    width: '100%', 
    height: '100%', 
    backgroundColor: 'rgba(255,255,255,0.5)',
    backgroundImage: `url('/assets/images/loader_mini.gif')`,
    backgroundSize: '20%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
}

export const ButtonWithLoader = (props) => (
    <button
        style={{position: 'relative'}}
        className={props.className != null? props.className : 'btn btn-primary'}
        onClick={(event)=> props.onClick(event)}
    >
    {props.showLoader && <div  style={loaderGifStyle}></div>}
    {props.children}
    </button>
)