import React from 'react';
import * as firebase from 'firebase';
import { ButtonWithLoader } from './ButtonWithLoader'
import axios from 'axios';

const initialForm = {
    name: '',
    phone: '',
    message: ''
}

export class ContactForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            ...initialForm,
            phone: props.phone || '',
            name: props.name || '',
            inProccess: false,
            showSuccessMessage: false
        }
    }

    submitMessage(event){
        event.preventDefault();
        this.setState({inProccess: true})
        const {name,phone,message} = this.state;
        const email = firebase.auth().currentUser.email;
        firebase.database().ref('tickets').push({name,email,phone,message,time: new Date().toJSON()})
        .then((snapshot) => {
            const dest = 'dormalk@gmail.com';
            const subject = 'פנייה נפתחה';

            axios.get(`https://us-central1-wedding-app-a228e.cloudfunctions.net/sendMail?dest=${dest}&subject=${subject}&message=${message}`)
            this.setState(
                {...initialForm,
                    inProccess: false,
                    showSuccessMessage:true,
                    phone: this.props.phone || '',
                    name: this.props.name || '',
                })
        })
    }

    render(){
        const {name, phone, message,inProccess,showSuccessMessage} = this.state;
        return(
            <form>
                <div>
                    <input  type="text" 
                            className="form-control" 
                            placeholder="שם"
                            value={name}
                            onChange={(event) => this.setState({name:event.target.value})}/>
                    <input  type="text" 
                            className="form-control" 
                            placeholder="טלפון"
                            value={phone}
                            onChange={(event) => this.setState({phone:event.target.value})}/>
                </div>
                <textarea   rows="2" 
                            className="form-control"
                            value={message}
                            onChange={(event) => this.setState({message:event.target.value})}/>
                {showSuccessMessage && <small>!תודה רבה על פנייתך, נדאג ליצור קשר בהקדם</small>}
                <ButtonWithLoader
                    showLoader={inProccess}
                    onClick={(event) => this.submitMessage(event)}>
                    שלח
                </ButtonWithLoader>                                                        
            </form>
        )
    }
}