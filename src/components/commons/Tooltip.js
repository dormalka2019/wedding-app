import React,{useState} from 'react';


export const Tooltip = ({children,text}) => {
    const [show,setShow] = useState(false)
    return(
        <span className="tooltip_c">
            {show && <small className="label">{text}</small>}
            <span onMouseEnter={() => setShow(true)} onMouseLeave={() => setShow(false)}>
                {children}
            </span>
        </span>
    )
}