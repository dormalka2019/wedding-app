import React from 'react';



export class AlertMessage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isShown: props.isShown,
            type: props.type,
            message: props.message
        }
    }

    componentWillUpdate(nextProps, nextState) {
        var nextUpdate = this.state;
        var isUpdate = false;

        if(nextProps.isShown !== this.state.isShown) {
            nextUpdate.isShown = nextProps.isShown;
            isUpdate = true;
        };
        if(nextProps.type !== this.state.type) {
            nextUpdate.type = nextProps.type;
            isUpdate = true;
        };
        if(nextProps.message !== this.state.message) {
            nextUpdate.message = nextProps.message;
            isUpdate = true;
        };
        if(isUpdate)
            this.setState({...nextUpdate})
    }

    render(){
        const {isShown, type, message} = this.state;
        return(
            isShown && <div className={`alert ${type}`} role="alert">
            {
                message && message !== ''? message : this.props.children
            }
            <div    style={{float:'left', cursor: 'pointer'}}
                    onClick={() => this.setState({isShown:false})}>x</div>
        </div>
        )
    }

}
