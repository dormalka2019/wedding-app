import React from 'react';

export const SmileEmoji = () => (
    <img className="emoji" src="/assets/images/emojis/positive.png" alt="smile"/>
)