import React from 'react';

export const CheckBox = ({children,value,onChange,style}) => {
    return(
        <div
            className={value? 'checkbox active':'checkbox'}
            onClick={() => onChange(!value)}
            style={style}
        >
            <span></span>
            {children}
        </div>
    )
}