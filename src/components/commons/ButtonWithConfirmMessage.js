import React,{useState} from 'react';

export const ButtonWithConfirmMessage = ({children, onClick, className,message}) => {
    const [showConfirmMessage, setShowConfirmMassage] = useState(false)
    
    return(
        <React.Fragment>
        <div  className={`confirmMessage ${showConfirmMessage? 'show':''}`}>
            <div className="innerbox">
                <section>
                    {message}
                </section>
                <div className="row">
                    <button className='btn btn-primary'
                            onClick={(event)=> {
                                setShowConfirmMassage(false)
                                onClick(event)
                            }}>אישור</button>
                    <button className='btn btn-danger'
                            onClick={(event)=> {
                                event.preventDefault();
                                setShowConfirmMassage(false)
                            }}>ביטול</button>
                </div>
            </div>
        </div>
        <button
            style={{position: 'relative', margin: '10px'}}
            className={className != null? className : 'btn btn-primary'}
            onClick={(event) => {
                event.preventDefault();
                setShowConfirmMassage(true)
            }}
        >
                    {children}
        </button>

        </React.Fragment>

    )
}