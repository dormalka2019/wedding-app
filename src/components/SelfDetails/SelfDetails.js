import React from 'react';
import { connect } from 'react-redux';
import DateTimePicker from 'react-datetime-picker/dist/DateTimePicker';
import { AlertMessage,ButtonWithLoader } from '../commons';
import { updateUser } from '../../actions/auth';
import axios from 'axios';

class SelfDetailsClass extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            userdata: {
                phone: props.userdata.phone || '',
                eventType: props.userdata.eventType || '',
                otherEventType: props.userdata.otherEventType || '',
                datetime: props.userdata.datetime || new Date(),
                hallName: props.userdata.hallName || '',
                ...props.userdata, 
            },
            password: '',
            repassword: '',
            alert: {
                type: '',
                message: '',
                isShown: false
            },
            varifiedPhoneButton: {
                isProccess: false,
                error: false,
                success: false,
                varifiedCode: '9999999999',
                enteredCode: ''
            },
            errorField: '',
            updateSelfDetailsProccess: false
        }

        this.subsribetion = null;
    }

    componentWillUnmount(){
        if(this.subsribetion != null) clearTimeout(this.subsribetion)
    }

    updateSelfDetails(event){
        event.preventDefault();
        this.setState({updateSelfDetailsProccess: true})
        const { password,repassword } = this.state;
        const regPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if(password !== '' && !regPass.test(password)) {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'סיסמא חייבת להכיל מינימום 8 תוים, אות אחת ומספר אחד'
                },
                errorField:'password',
                updateSelfDetailsProccess: false
            })
            return;
        }
        if(password !== '' && password !== repassword){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'הסיסמאות אינן תואמות'
                },
                errorField:'repassword',
                updateSelfDetailsProccess: false
            })
            return;
        }
        if(new Date(this.state.userdata.datetime).getTime() < new Date().getTime()){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'יש לקבוע תאריך עתידי'
                },
                errorField: 'datetime',
                updateSelfDetailsProccess: false
            })
            return;
        }
        // eslint-disable-next-line eqeqeq
        if(this.props.userdata.phone != this.state.userdata.phone && !this.state.varifiedPhoneButton.success){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'מספר הטלפון הוחלף אך לא אומת'
                },
                errorField: 'datetime',
                updateSelfDetailsProccess: false
            })
            return;
        }
        this.props.updateUser(this.state.userdata)
        .then(() => {
                this.setState({
                    alert: {
                        isShown: true,
                        type: 'alert-primary',
                        message: 'פרטייך עודכנו בהצלחה'
                    },errorField:'',
                    updateSelfDetailsProccess: false
                })

        })
    }

    onChangeUserdata(value, field) {
        const { userdata } = this.state;
        userdata[field] = value;
        this.setState({userdata})
    }

    checkNum(phone) {
        if(phone.length !== 10) return true;
        if(phone.indexOf('052') === 0) return false;
        if(phone.indexOf('050') === 0) return false;
        if(phone.indexOf('054') === 0) return false;
        if(phone.indexOf('055') === 0) return false;
        if(phone.indexOf('058') === 0) return false;
        return true;
    }

    sendVarifiedPhoneCode(event){
        event.preventDefault();
        var {varifiedPhoneButton} = this.state; 
        if(varifiedPhoneButton.isProccess) return;
        if(this.checkNum(this.state.userdata.phone)){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'יש לזין מספר טלפון תקין בפורמט 05XXXXXXXX'
                },
                errorField: 'phone',
                updateSelfDetailsProccess: false
            })
            return;
        }
        varifiedPhoneButton.isProccess = true;
        varifiedPhoneButton.success = true;
        const varifiedCode = Math.floor(Math.random() * (999999 - 100000) ) + 100000;
        varifiedPhoneButton.varifiedCode = varifiedCode;
        this.setState({varifiedPhoneButton});
        axios.get(`https://us-central1-wedding-app-a228e.cloudfunctions.net/sendVarifiedPhoneNumber?varifiedCode=${varifiedCode}&phone=${this.state.userdata.phone}`)
        .catch(() => {
            varifiedPhoneButton.isProccess = false;
            varifiedPhoneButton.success = true;
            this.setState({varifiedPhoneButton});
        })


        this.subsribetion = setTimeout(() => {
            varifiedPhoneButton.isProccess = true;
            varifiedPhoneButton.success = true;
            varifiedPhoneButton.varifiedCode = '9999999999';
            this.setState({varifiedPhoneButton});
        }, 60000)
    }

    onChangeVarificationCodeInput(event) {
        const val = event.target.value;
        var {varifiedPhoneButton} = this.state; 
        // eslint-disable-next-line eqeqeq
        if(val == varifiedPhoneButton.varifiedCode) {

            clearTimeout(this.subsribetion);
            varifiedPhoneButton.isProccess = false;
            varifiedPhoneButton.success = true;
            varifiedPhoneButton.varifiedCode = '9999999999';

            this.props.updateUser({phone: this.state.userdata.phone,uid: this.state.userdata.uid})
            .then(() => {
                this.setState({
                    varifiedPhoneButton,
                    alert: {
                        isShown: true,
                        type: 'alert-primary',
                        message: 'מספרך אומת במערכת'
                    },errorField:'',
                    updateSelfDetailsProccess: false
                });
            })
        }else if (val.length > 6){
            varifiedPhoneButton.error = true;
            this.setState({varifiedPhoneButton});
        } else {
            varifiedPhoneButton.enteredCode = val;
            this.setState({varifiedPhoneButton})
        }
    }

    render(){
        const { displayName,email,phone,eventType,otherEventType,datetime,hallName } = this.state.userdata;
        const { password,repassword, alert,errorField,updateSelfDetailsProccess } = this.state;
        const {isProccess,error,success,enteredCode,varifiedCode} = this.state.varifiedPhoneButton;
        return(
            <div id="self_details" className="container">
            <form>
                <h1>עדכון פרטים אישיים</h1>
                <AlertMessage {...alert}/>
                <div className="form-group-lg row">
                    <label htmlFor="fullname" className="col-sm-2 col-form-label">שם מלא</label>
                    <div className="col-sm-10">
                        <input  id="fullname" 
                                className="form-control"
                                type="text" 
                                onChange={(event) => this.onChangeUserdata(event.target.value, 'displayName')}
                                value={displayName}/>
                    </div>

                    <label htmlFor="email" className="col-sm-2 col-form-label">אימייל</label>
                    <div className="col-sm-10">
                        <input  id="email" 
                                className="form-control-plaintext"
                                disabled
                                type="text" 
                                value={email}/>
                    </div>

                    <label htmlFor="phone" className="col-sm-2 col-form-label">פלאפון</label>
                    <div className="col-sm-10">
                        <input  id="phone" 
                                className="form-control"
                                type="text" 
                                onChange={(event) => this.onChangeUserdata(event.target.value, 'phone')}
                                value={phone}/>
                        {
                            this.props.userdata.phone !== this.state.userdata.phone &&
                            <div className="col-4" style={{margin:'auto'}}>
                                <ButtonWithLoader   showLoader={isProccess} 
                                                    className="btn btn-success"
                                                    onClick={this.sendVarifiedPhoneCode.bind(this)}>שלח קוד אימות</ButtonWithLoader>
                                
                                {
                                    error &&
                                        (varifiedCode === '9999999999' ? 
                                        <small style={{color: 'red', display: 'block'}}>פג תוקף הסיסמא, יש לשלוח קוד מחדש</small> :
                                        <small style={{color: 'red', display: 'block'}}>קוד אינו נכון - יש לנסות שוב</small>)
                                }
                                {
                                    success &&
                                    <input  type="text" 
                                            value={enteredCode}
                                            className="form-control varifiedCode" 
                                            placeholder="קוד אימות"
                                            onChange={this.onChangeVarificationCodeInput.bind(this)}/>
                                }
                            </div>
                        }
                    </div>

                </div>
                
                <small>במידה ואינך מעוניין לעדכן סיסמא יש להשאיר שדות אלו ריקים</small>
                <div className="form-group-lg row">
                    <label htmlFor="password" className="col-sm-2 col-form-label">סיסמא</label>
                    <div className="col-sm-10">
                        <input  id="password" 
                                className="form-control"
                                style={{border: (errorField === 'password' || errorField === 'repassword') && 'red 1px solid'}}
                                type="password" 
                                onChange={(event) => this.setState({password: event.target.value})}
                                value={ password }
                                placeholder="הזן סיסמא חדשה"/>
                    </div>
                    <label htmlFor="repassword" className="col-sm-2 col-form-label">סיסמא חוזרת</label>
                    <div className="col-sm-10">
                        <input  id="repassword" 
                                className="form-control"
                                style={{border: errorField === 'repassword' && 'red 1px solid'}}
                                onChange={(event) => this.setState({repassword: event.target.value})}
                                value={ repassword }
                                type="password" 
                                placeholder="הזן שוב סיסמא"/>
                    </div>

                </div>
            </form>

            <form>
                <h1>עדכון פרטי האירוע</h1>
                    <div className="form-group-lg row">
                        <label htmlFor="type" className="col-sm-2 col-form-label">סוג האירוע</label>
                        <div className="col-sm-10">
                            <select value={eventType}
                                    className="form-control"
                                    onChange={(event) => this.onChangeUserdata(event.target.value, 'eventType')}>
                                    <option value=""></option>
                                    <option value="חתונה">חתונה</option>
                                    <option value="בר/בת מצווה">בר/בת מצווה</option>
                                    <option value="ברית/ה">ברית/ה</option>
                                    <option value="אחר">אחר</option>
                            </select>
                            {eventType === 'אחר' && <input  className="form-control"
                                                            onChange={(event) => this.onChangeUserdata(event.target.value, 'otherEventType')}
                                                            type="text" 
                                                            value={otherEventType}/>}
                        </div>

                        <label htmlFor="date" className="col-sm-2 col-form-label">תאריך ושעת האירוע</label>
                        <div style={{overflow: 'hidden'}} className={`col-sm-10 ${errorField === 'datetime' && 'errorfield'}`}>
                            <DateTimePicker onChange={(newdatetime) => this.onChangeUserdata(new Date(newdatetime).toJSON(),'datetime')}
                                            value={new Date(datetime)}
                                            id="date"/>
                        </div>
                        <label htmlFor="hallName" className="col-sm-2 col-form-label">שם האולם / כתובת האולם</label>
                        <div className="col-sm-10">
                            <input  id="hallName" 
                                    className="form-control"
                                    onChange={(event) => this.onChangeUserdata(event.target.value, 'hallName')}
                                    value={ hallName }/>
                        </div>
                    </div>
                    <ButtonWithLoader   className="btn btn-primary" 
                                        onClick={this.updateSelfDetails.bind(this)}
                                        showLoader={updateSelfDetailsProccess}>עדכון פרטים</ButtonWithLoader>
                </form>


            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateUser: (updatedUser) => dispatch(updateUser(updatedUser))
})


const mapStateToProps = (state) => ({
    userdata: state.userdata
})

export const SelfDetails = connect(mapStateToProps,mapDispatchToProps)(SelfDetailsClass)