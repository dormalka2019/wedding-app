import React,{useState} from 'react';
import {connect} from 'react-redux';
import { siteConfigs } from '../../shared/siteConfigs'
import {generatePaymentUrl} from '../../helpers/masof';

const mapDispatchToProps = (dispatch) => ({

})

const mapStateToProps = (state) => ({
    userdata: state.userdata,
})



export const PackageStore = connect(mapStateToProps,mapDispatchToProps)(({userdata}) => {
    const [_package, setPackage] = useState(null)
    return(
        <div id="package_store" className="container">
            <h1>רכישת חבילות</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>שם החבילה</th>
                        <th>עלות</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        siteConfigs.packages.map(pack => {

                            return (
                                <tr key={pack.api}>
                                    <td><input type="radio" name="pack" value={pack.api} onChange={()=>setPackage(pack)}/></td>
                                    <td>{pack.name}</td>
                                    <td>{pack.price}{`${pack.currency_code === 'shekel'? '₪':''}`} </td>
                                    <td style={{fontSize: 12}}>{pack.price/pack.count} אג' להודעה</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            {
                _package &&
                <button className="btn paypal-btn" onClick={() => generatePaymentUrl(_package, userdata)}>קנה עכשיו <i className="fas fa-credit-card"></i></button>
            }
        </div>
    )
})

