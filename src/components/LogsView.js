import React,{useEffect,useState} from 'react'
import { withRouter, } from 'react-router';
import queryString from 'query-string';
import * as firebase from 'firebase';
import { convertToArr, formatDate } from '../helpers/fucntions';
import DateTimePicker from 'react-datetime-picker/dist/DateTimePicker';

var day = 60 * 60 * 24 * 1000;


export const LogsView = withRouter((props) => {
    const [logs, setLogs] = useState([]);
    const [isLoad, setIsLoad] = useState(true);
    const [search, setSearch] = useState('');
    const [startDate, setStartDate] = useState(new Date().getTime() - day);
    const [endDate, setEndDate] = useState(new Date().getTime() + day);
    
    const [isUrlParams, setIsUrlParams] = useState(true);
    
    useEffect(() => { 
        const {relatedTo} = queryString.parse(props.location.search);
        if(isUrlParams && relatedTo){
            setSearch(relatedTo);
            setIsUrlParams(false)
        }       
        if(isLoad){
            firebase.database().ref('logs')
            .orderByKey()
            .startAt(new Date(startDate).getTime().toString())
            .endAt(new Date(endDate).getTime().toString())
            .once('value', (snapshot) => {
                setIsLoad(false)
                setLogs(convertToArr(snapshot.val()))
            }, (err) => setIsLoad(false))
            

        }
        return(() => firebase.database().ref('logs').off())
    },[props.location.search, isUrlParams, isLoad, startDate, endDate])

    return(
        <div id="all_logs_view" className="container">
            <h1>תצוגת לוגים</h1>

            <input  className="form-control" 
                    type="text" 
                    placeholder="חיפוש"
                    value={search} 
                    onChange={(event)=>setSearch(event.target.value)}/>
            <div className="row">
                <label htmlFor="date" className="col-sm-2 col-form-label">החל מ:</label>
                <div style={{overflow: 'hidden'}} className="col-sm-3">
                    <DateTimePicker onChange={(newdatetime) => {
                                        setStartDate(new Date(newdatetime).toJSON(),'datetime');
                                        setIsLoad(true)
                                    }}
                                    value={new Date(startDate)}
                                    id="date"/>
                </div>

                <label htmlFor="date" className="col-sm-2 col-form-label">עד ל:</label>
                <div style={{overflow: 'hidden'}} className="col-sm-3">
                    <DateTimePicker onChange={(newdatetime) => {
                                        setEndDate(new Date(newdatetime).toJSON(),'datetime')
                                        setIsLoad(true)
                                    }}
                                    value={new Date(endDate)}
                                    id="date"/>
                </div>

            </div>
            {
                isLoad? <img className="_loader" src="\assets\images\loader.gif" alt="loader"/> :
                <table id="messages" className="table table-hover">
                    <thead>
                        <tr>
                            <th>תאריך יצירה</th>
                            <th>סוג</th>
                            <th>מקושר ל</th>
                            <th>הודעה</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            logs.filter(log => log.message.indexOf(search) >= 0).map(log => (
                                <tr key={log.id}>
                                            <td>{formatDate(log.id*1)}</td>
                                            <td>{log.type}</td>
                                            <td>{log.relatedTo}</td>
                                            <td style={{textAlign: 'left'}}>{log.message}</td>
                                        </tr>
                            ))
                        }
                    </tbody>
                </table>

            }
        </div>
    )
});

