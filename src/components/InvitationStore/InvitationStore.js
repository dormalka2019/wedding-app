import React, {useState,useEffect} from 'react';
import { generatePaymentUrl } from '../../helpers/masof';
import { templatesList } from '../../shared/templatesList';
import { connect } from 'react-redux';
import { initializedFirebaseApp } from '../../configs/firebase';

const mapStateToProps = (state) => ({
    userdata: state.userdata
})

export const InvitationStore = connect(mapStateToProps,null)((props) => {
    const [tempList, setTempList] = useState([]);
    const isInit = React.useRef(true)
    const [purchaseTemps,setPurchaseTemps] = React.useState([]);

    useEffect(() => {
        var temps = templatesList.filter((temp) => temp.freeMode === false)
                    .map(temp => { 
                        return {
                            ...temp, 
                            api:temp.templateName,
                            name: temp.templateLabel
                        };
                    });
        setTempList(temps);

        if(isInit.current && props.userdata !== undefined){
            initializedFirebaseApp.database().ref(`users/${props.userdata.uid}/templatePurchase`)
            .on('value',snapshot => {
                console.log(snapshot.exists())
                if(snapshot.exists()) setPurchaseTemps([...purchaseTemps,...snapshot.val()])
            })

            isInit.current = false;
        }

        return(() => {
            initializedFirebaseApp.database().ref(`users/${props.userdata.uid}/templatePurchase`).off();
        })
    },[setTempList,isInit,props.userdata,setPurchaseTemps,purchaseTemps])

    const openViewMode = (designName) => {
        console.log(designName)
        const a = document.createElement('a');
        a.href = `/assets/images/templates/${designName}/template.png`;
        a.target = '_blank';
        a.click();
        a.remove();
    }

    const generateBuyButton = (elem) => {
        if(purchaseTemps !== undefined && purchaseTemps.includes(elem.api)){
            return(
                <button className="btn">
                    נרכש
                </button>
            )
        } else {
            return(
                <button className="btn active" onClick={() => generatePaymentUrl(elem,props.userdata)}>
                    {elem.price} שקלים  <i className="far fa-credit-card"></i>
                </button>
            )
        }
    }

    return (
        <div id="invitations_store" className="container">
        <h1>חנות עיצובי הזמנות</h1>
        <div id="card_layout">
                {tempList.map((elem,index) => (
                    <div className="store_card" key={index}>
                        <img    src={`/assets/images/templates/${elem.api}/template.png`} 
                                onClick={() => openViewMode(elem.api)} 
                                style={{opacity: purchaseTemps.includes(elem.api)? 0.7: 1}}
                                alt={elem.api}/>
                        {generateBuyButton(elem)}
                    </div>
                ))}
            </div>
        </div>
    )
})