import React,{useEffect,useState} from 'react'
import * as firebase from 'firebase';
import { convertToArr, formatDate } from '../helpers/fucntions';


export const MessagesView = () => {
    const [messages, setMessages] = useState([]);
    const [isLoad, setIsLoad] = useState(true);
    const [isHandled, setIsHandled] = useState(false);

    useEffect(() => {
        firebase.database().ref('tickets')
        .once('value', (snapshot) => {
            setIsLoad(false)
            setMessages(convertToArr(snapshot.val()))
        }, (err) => setIsLoad(false))

        return(() => firebase.database().ref('tickets').off())
    })
    return(
        <div id="all_messages_view" className="container">
            <h1>טיפול בפניות</h1>
            <nav>
                <div className={`${!isHandled? 'active':''}`} onClick={()=>setIsHandled(false)}>פניות שלא טופלו</div>
                <div className={`${isHandled? 'active':''}`} onClick={()=> setIsHandled(true)}>פניות שטופלו</div>
            </nav>
            {
                isLoad? <img className="_loader" src="\assets\images\loader.gif" alt="loader"/> :
                <table id="messages" className="table table-hover">
                    <thead>
                        <tr>
                            <th>שם הפונה</th>
                            <th>טלפון</th>
                            <th>תאריך הפנייה</th>
                            <th>תוכן הפנייה</th>
                            <th>לקוח קיים?</th>
                            <th>פעולות נוספות</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            messages.map(msg => (
                                ((!isHandled && !msg.done) || (isHandled && msg.done)) &&
                                <tr key={msg.id}>
                                            <td>{msg.name}</td>
                                            <td>{msg.phone}</td>
                                            <td>{formatDate(new Date(msg.time))}</td>
                                            <td>{msg.message}</td>
                                            <td>{msg.email}</td>
                                            <td>
                                                {
                                                    msg.done?(
                                                        <i className="fas fa-reply"
                                                            onClick={() => firebase.database().ref('tickets/'+msg.id+"/done").set(false)}></i>
                                                    ):(
                                                        <i className="fas fa-check-circle"
                                                            onClick={() => firebase.database().ref('tickets/'+msg.id+"/done").set(true)}></i>
                                                    )
                                                }
                                            </td>
                                        </tr>
                            ))
                        }
                    </tbody>
                </table>

            }
        </div>
    )
}

