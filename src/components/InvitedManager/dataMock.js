export default [
    {
        fullName: 'רועי ווינשטיין',
        totalCount: 2,
        meet: 2,
        vegetarian: 0,
        vegan: 0,
        children: 0,
        phone: '052-4431050',
        approved: 0,
        side: 'צד א',
        present: 0
    },
    {
        fullName: 'תמר גולדשטיין',
        totalCount: 5,
        meet: 1,
        vegetarian: 1,
        vegan: 1,
        children: 2,
        phone: '055-1234563',
        approved: 0,
        side: 'צד ב',
        present: 0
    },
    {
        fullName: 'יוסי רבינובינוביץ',
        totalCount: 3,
        meet: 2,
        vegetarian: 0,
        vegan: 0,
        children: 1,
        phone: '053-1245621',
        approved: 0,
        side: 'צד א',
        present: 0
    }

]