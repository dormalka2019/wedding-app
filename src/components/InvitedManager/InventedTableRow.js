import React,{useState} from 'react';
import {isMobile} from '../../helpers/device-detect';
import DataCell from './DataCell';




export default (props) => {
    const [trash, setTrash] = useState(false)
    return (
        <tr onMouseOver={() => setTrash(true)}
            onMouseLeave={() => setTrash(false)}>
            <DataCell
                value={props.fullName}
                type={'text'}
                style={{textAlign: 'right'}}
                onChange={event => props.onEdit({fullName:event.target.value},props.index)}
            />
            {
                <DataCell
                    value={props.phone}
                    type={'text'}
                    className="landscape"
                    onChange={event => props.onEdit({phone:event.target.value},props.index)}
                />
            }
            <DataCell
                value={props.totalCount}
                type={'number'}
                onChange={event => {
                    const totalCount = event.target.value;
                    if(totalCount >= (Number.parseInt(props.children)+Number.parseInt(props.vegetarian)+Number.parseInt(props.vegan))){
                        props.onEdit({totalCount:event.target.value},props.index)
                    }
                }}
            />

            {!isMobile &&
                <DataCell
                    value={props.children}
                    type={'number'}
                    onChange={event => {
                        const children = event.target.value;
                        if(props.totalCount >= (Number.parseInt(children)+Number.parseInt(props.vegetarian)+Number.parseInt(props.vegan))){
                            if(children >= 0)
                                props.onEdit({children},props.index)
                        }
                    }}
                />
            }
            {!isMobile &&
                <DataCell
                    value={props.vegetarian}
                    type={'number'}
                    onChange={event => {
                        const vegetarian = event.target.value;
                        if(props.totalCount >= (Number.parseInt(props.children)+Number.parseInt(vegetarian)+Number.parseInt(props.vegan))){
                            if(vegetarian >= 0)
                                props.onEdit({vegetarian},props.index)
                        }
                    }}
                />
            }
            {!isMobile &&
                <DataCell
                    value={props.vegan}
                    type={'number'}
                    onChange={event => {
                        const vegan = event.target.value;
                        if(props.totalCount >= (Number.parseInt(props.children)+Number.parseInt(props.vegetarian)+Number.parseInt(vegan))){
                            if(vegan >= 0)
                                props.onEdit({vegan},props.index)
                        }
                    }}
                />
            }

            {!isMobile &&
                <DataCell
                    value={props.side}
                    type={'select'}
                    options={['צד א','צד ב']}
                    onChange={event => props.onEdit({side:event.target.value},props.index)}
                />
            }
            <td style={{cursor: 'pointer'}} onClick={() => {
                let upadtedApproved = props.approved;
                if(props.approved > 1) {
                    upadtedApproved = -1;
                } else {
                    upadtedApproved += 1;
                }
                props.onEdit({approved:upadtedApproved},props.index)}
            }>
                {
                    props.approved === -1 ?
                    <i className="fas fa-user-times"></i>:
                    props.approved === 0 ?
                    <i className="fas fa-question"></i>:
                    props.approved === 1?
                    <i className="far fa-thumbs-up"></i>:
                    '-'
                }
            </td>
            {!isMobile &&
                <DataCell
                    value={props.present}
                    type={'number'}
                    onChange={event => {
                        const present = event.target.value;
                        if(present >= 0)
                            props.onEdit({present},props.index)
                    }}
                />
            }
            {
                !isMobile &&
                <DataCell
                    value={props.comments}
                    type={'text'}
                    onChange={event => props.onEdit({comments:event.target.value},props.index)}
                />
            }
            <td style={{width: '30px'}}>{trash && <i    className="fas fa-trash"
                                                        onClick={(event) => {
                                                                event.preventDefault();
                                                                props.onRemove(props.index)
                                                        }}></i>}</td>
        </tr>
    )
}