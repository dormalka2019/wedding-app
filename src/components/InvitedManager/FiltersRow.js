import React, {useState} from 'react';
import {isMobile} from '../../helpers/device-detect';

export default ({onChangeFilters}) => {
    const [fullName,setFullname] = useState('');
    const [countFilter,setCountFilter] = useState('');

    return(
        <div className="filter_row">
            <i className="fas fa-filter"></i>
            <input  type="text"
                    value={fullName}
                    placeholder="שם המוזמן"
                    className="form-control"
                    onChange={(event) => {
                        const newFullname = event.target.value;
                        onChangeFilters({fullName: newFullname});
                        setFullname(newFullname)
                    }}/>
            <select value={countFilter}
                    className="form-control"
                    onChange={(event) => {
                        const newCountFilter = event.target.value;
                        onChangeFilters({countApiFilter: newCountFilter});
                        setCountFilter(newCountFilter)
                    }}>
                        <option disabled>סינון לפי</option>
                        <option value="">הכל</option>
                        {!isMobile && <option value="children">ילדים</option>}
                        {!isMobile && <option value="vegan">טבעוניים</option>}
                        {!isMobile && <option value="vegetarian">צמחוניים</option>}
                        {!isMobile && <option value="present">מתנה</option>}
                        <option value="approved1">אישרו הגעה</option>
                        <option value="approved0">אולי מגיעים</option>
                        <option value="approved-1">לא מגיעים</option>                        
            </select>
            <div style={{clear:'both'}}></div>
        </div>
    )
}