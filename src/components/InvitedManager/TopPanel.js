import React from 'react';

export default ({onInsert,onSave,onRemove,onExport,onImport}) => {
    return(
        <div className="top_panel">
            <button className="btn"
                onClick={() => onInsert()}
            >
                הוספת מוזמן
                <i className="fas fa-plus"></i>            
            </button>
            <button className="btn"
                onClick={() => onSave()}
            >
                שמירת רשימה
                <i className="fas fa-pen"></i>
            </button>
            <button className="btn"
                    onClick={() => onRemove()}>
                מחיקת רשימה
                <i className="fas fa-trash"></i>
            </button>
            <button className="btn"
                    onClick={() => document.getElementById("uploadCsv").click()}>
                ייבוא רשימה
                <i className="fas fa-file-import"></i>
            </button>
            <input  type="file" 
                    id="uploadCsv" 
                    style={{display: 'none'}}
                    onChange={(event) => onImport('uploadCsv',event.target.value)} />
            <button className="btn"
                    onClick={() => onExport()}>
                ייצוא רשימה
                <i className="fas fa-file-export"></i>
            </button>
            <div style={{clear:'both'}}></div>
        </div>
    )
}