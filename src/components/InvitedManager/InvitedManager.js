import React from 'react'
import InventedTable,{tableHeaders} from './InventedTable';
import TopPanel from './TopPanel';
import FiltersRow from './FiltersRow'
import { startSetList,startFetchInvetedList } from '../../actions/invitedsData';
import { startSetListForUser } from '../../actions/admin';
import { connect } from 'react-redux';
import { AlertMessage } from '../commons';
import { exportTableToExcel,importFileToTableForInvitedList } from '../../helpers/excel';
import Sticky from 'react-sticky-el';

const initialItem = {
    fullName: '',
    totalCount: 0,
    meet: 0,
    vegetarian: 0,
    vegan: 0,
    children: 0,
    phone: '0',
    approved: 2,
    side: '',
    present: 0,
    comments: ''
}
class InvitedManager_class extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: props.invitedlist || [],
            sortedColumn: '',
            asc: true,
            loading: props.loading,
            alert: {
                type: 'alert-primary',
                message: '',
                isShown: false
            },
            filters: {
                fullName: '',
                countApiFilter: ''
            }
        }
    }
    componentWillMount(){
        
        this.props.startFetchInvetedList(this.props.uid)
        .then(() => {
            this.soryBy('fullName');
        })
    }

    componentWillUpdate(nextProps, nextState){
        if(this.props.invitedlist !== nextProps.invitedlist)
            this.setState({data:nextProps.invitedlist})
        if(this.props.loading !== nextProps.loading)
            this.setState({loading: nextProps.loading})
    }
    soryBy = (key) => {
        var data;
        var asc = this.state.sortedColumn !== key? true : !this.state.asc;
        // eslint-disable-next-line default-case
        switch(key){
            case 'fullName':
            case 'phone':
            case 'side':
                data = asc? this.state.data.sort((r1,r2) => r1[key].localeCompare(r2[key])):
                this.state.data.sort((r1,r2) => r2[key].localeCompare(r1[key]))
                break;
            case 'totalCount':
            case 'children':
            case 'meet':
            case 'vegetarian':
            case 'approved':
            case 'vegan':
            case 'present':
                data = asc? this.state.data.sort((r1,r2) => r1[key] - r2[key]):
                this.state.data.sort((r1,r2) => r2[key] - r1[key])
                break;
        }
        this.setState({data,sortedColumn:key,asc})
    }
    onRemove(index){
        var {data} = this.state;
        data.splice(index,1)
        this.setState({data})
    }

    onInsert(){
        const { data } = this.state;
        data.push(JSON.parse(JSON.stringify(initialItem)));
        this.setState({data})
    }

    checkNum(phone) {
        if(phone.length !== 10) return true;
        if(phone.indexOf('052') === 0) return false;
        if(phone.indexOf('050') === 0) return false;
        if(phone.indexOf('054') === 0) return false;
        if(phone.indexOf('055') === 0) return false;
        if(phone.indexOf('058') === 0) return false;
        return true;
    }

    checknumbers() {
        const { data } = this.state;
        return data.filter(({phone}) => this.checkNum(phone));
    }

    onSave(){
        if(this.checknumbers().length > 0){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'לא ניתן לשמור רשימה כיוון שיש מספרים שאינם מתאימין לפורמט 05XXXXXXXX'
                },
                data: this.state.data.map((person => {
                    if(this.checkNum(person.phone)){
                        return Object.assign(person,{errorNum: true})
                    }
                    return person
                })),
                filters: Object.assign(this.state.filters,{countApiFilter: 'errorNum'})
            })
        } else {
            if(this.props.isAdmin){
                this.props.startSetListForUser(this.state.data,this.props.uid)
            }else {
                this.props.startSetList(this.state.data);
            }
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'רשימה התעדכנה'
                }
            })
        }

    }
    
    resetList(){
        this.setState({data:[]})
    }

    onEdit(newData,index){
        var {data} = this.state;
        Object.assign(data[index], newData);
        data[index].meet = Number.parseInt(data[index].totalCount) - Number.parseInt(data[index].children) - Number.parseInt(data[index].vegetarian) - Number.parseInt(data[index].vegan); 
        if(data[index].meet < 0) return;

        this.setState({data,alert:{isShown:false}});
    }

    onExportToCsv(){
        exportTableToExcel(this.state.data,tableHeaders)
        .then(() => {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'ייצוא רשימה בוצע בהצלחה'
                }
            })
        })
    }

    onImportCsv(inputId,filename){
        importFileToTableForInvitedList(inputId,filename)
        .then(data => {
            this.setState({
                data,
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'ייבוא רשימה בוצע בהצלחה'
                }
            })
        })
    }

    onChangeFilters(filters) {
        const updateFilters = Object.assign(this.state.filters, filters)
        this.setState({filters:updateFilters})
    }

    render(){
        const { loading,alert } = this.state;
        return(
            <div id="invited_manager" className="container">
                <Sticky id="sticky_element">
                    <TopPanel
                        onInsert={() => this.onInsert()}
                        onSave={() => this.onSave()}
                        onExport={() => this.onExportToCsv()}
                        onImport={(inputId,fileName) => this.onImportCsv(inputId,fileName)}
                        onRemove={() => this.resetList()}
                    />
                    <AlertMessage
                        isShown={alert.isShown}
                        type={alert.type}
                        message={alert.message}
                    />
                    <FiltersRow onChangeFilters={(filters) => this.onChangeFilters(filters)}/>
                </Sticky>
                <InventedTable {...this.state}
                                soryBy={(key) => this.soryBy(key)}
                                onRemove={(index) => this.onRemove(index)}
                                onEdit={(data,index) => this.onEdit(data,index)}/>
            {loading && <img className="_loader" src="\assets\images\loader.gif" alt="loader"/>}

            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startSetList: (list) => dispatch(startSetList(list)),
    startSetListForUser: (list,userId) => dispatch(startSetListForUser(list,userId)),
    startFetchInvetedList: (uid) => dispatch(startFetchInvetedList(uid))
})

const mapStateToProps = (state) => ({
    invitedlist: state.invitedsData,
    loading: state.userdata.loading,
    isAdmin: !!state.userdata.adminId,
    uid: state.userdata.uid
})
export const InvitedManager = connect(mapStateToProps,mapDispatchToProps)(InvitedManager_class);