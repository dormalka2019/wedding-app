import React from 'react';
import InventedTableRow from './InventedTableRow';
import {isMobile} from '../../helpers/device-detect'

export default ({data,sortedColumn,asc,soryBy,onRemove,onEdit,filters}) => (
    <table id="inviteds" className="table table-hover">
        <thead>
            <tr>
                {tableHeaders.map((header,index) => 
                    <th key={index} 
                        className={!!header.landscape? 'landscape': ''}
                        onClick={() => header.sortby && soryBy(header.api)}
                        style={{display: header.show? header.landscape? '':'table-cell':'none'}}>
                            {header.label}
                            {sortedColumn === header.api && asc? <i className="fas fa-sort-up"></i> : header.sortby ? <i className="fas fa-sort-down"></i> :<i></i>}
                    </th>
                    )
                }
                <th></th>
            </tr>
        </thead>
        <tbody>
            {
                data.filter((row) => {
                    if(row.fullName.includes(filters.fullName)){
                        if(filters.countApiFilter === 'approved1'){
                            return row['approved'] > 0;
                        } else if (filters.countApiFilter === 'approved0') {
                            return row['approved'] === 0;
                        } else if (filters.countApiFilter === 'approved-1'){
                            return row['approved'] < 0;
                        } else if (filters.countApiFilter !== '') {
                            return row[filters.countApiFilter] > 0;
                        } else if(filters.countApiFilter === 'errorNum'){
                            return !row['errorNum'];
                        } else {
                            return row;
                        }
                    }
                    return null;
                })
                .map((row,i)=> 
                <InventedTableRow   key={i} 
                                    {...row}
                                    index={i}
                                    onEdit={onEdit.bind(this)}
                                    onRemove={onRemove.bind(this)}/>)
            }
        
        </tbody>

    </table>


)

export const tableHeaders = [
    {
        label: 'שם',
        api: 'fullName',
        show: true,
        sortby: true
    },
    {
        label: 'טלפון',
        api: 'phone',
        show: true,
        landscape: true,
        sortby: false
    },
    {
        label: `מוזמנים`,
        api: 'totalCount',
        show: true,
        sortby: true
    },
    {
        label: 'ילדים',
        api: 'children',
        show: !isMobile,
        sortby: true
    },
    {
        label: 'צמחוניים',
        api: 'vegetarian',
        show: !isMobile,
        sortby: true

    },
    {
        label: 'טבעוניים',
        api: 'vegan',
        show: !isMobile,
        sortby: true

    },
    {
        label: 'צד',
        api: 'side',
        show: !isMobile,
        sortby: true

    },
    {
        label: 'אישרו',
        api: 'approved',
        show: true,
        sortby: false

    },    
    {
        label: 'מתנה',
        api: 'present',
        show: !isMobile,
        sortby: true
    },
    {
        label: 'הערות',
        api: 'comments',
        show: !isMobile,
        sortby: false
    }
]
