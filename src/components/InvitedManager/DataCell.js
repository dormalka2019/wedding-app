
import React,{useState} from 'react'
export default ({type, onChange,value,options,style,forceDisable, className}={style:{},forceDisable:false}) => {
    const [disableField, setDisableField] = useState(true)
    if(type !== 'select'){
        return(
            <td style={{textAlign: 'right'}} className={className}>
                <span onClick={()=> setDisableField(false)}>
                    <input  value={value}
                            type={type}
                            style={style}
                            className={`form-control${disableField? '-plaintext':''}`}
                            onBlur={()=> !disableField && setDisableField(true)}
                            onChange={event => onChange(event)}
                            readOnly={disableField||forceDisable}/>
                </span>
            </td>
        )
    }
    else {
        return (
            <td style={{textAlign: 'right'}}>
                <span onClick={()=> setDisableField(false)}>
                    <select value={value}
                            type={type}
                            className={`form-control${disableField? '-plaintext':''}`}
                            onBlur={()=> !disableField && setDisableField(true)}
                            onChange={event => onChange(event)}
                            readOnly={disableField}>
                                <option key="-1" value=""></option>
                                {options.map((item,i) => <option key={item} value={item} >{item}</option>)}
                    </select>
                </span>
            </td> 
        )
    }

}