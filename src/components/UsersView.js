import React from 'react'
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import { convertToArr, formatDate } from '../helpers/fucntions';
import { startLoginAs } from '../actions/admin';
import {withRouter } from 'react-router-dom';


const mapDispatchToProps = (dispatch) => ({

    startLoginAs: (userid) => dispatch(startLoginAs(userid)) 
})
export const UsersView = connect(undefined, mapDispatchToProps)(    
    withRouter(class UsersView extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                users: [],
                isLoad: true
            }
        }

        componentDidMount(){
            firebase.database().ref('users')
            .once('value', (snapshot) => this.setState({users: convertToArr(snapshot.val()),isLoad: false}))
        }

        componentWillUnmount(){
            firebase.database().ref('users').off();
        }
        render(){
            const { users,isLoad } = this.state;
            return(
                <div id="all_users_view" className="container">
                    <h1>ניהול משתמשים</h1>
                    {
                        isLoad? <img className="_loader" src="\assets\images\loader.gif" alt="loader"/> :
                        <table id="scheduals" className="table table-hover">
                            <thead>
                                <tr>
                                    <th>שם לקוח</th>
                                    <th>אימייל</th>
                                    <th>תאריך התחברות אחרון</th>
                                    <th>פעולות נוספות</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    users.map((user => (
                                        user.uid !== firebase.auth().currentUser.uid &&
                                        <tr key={user.uid}>
                                            <td>{user.displayName}</td>
                                            <td>{user.email}</td>
                                            <td>{formatDate(new Date(user.lastLogin))}</td>
                                            <td>
                                                <i  className="fas fa-sign-in-alt"
                                                    onClick={() => {
                                                        this.props.startLoginAs(user.uid);
                                                    }}></i>
                                                <i  className="fas fa-users-cog"
                                                    onClick={() => this.props.history.push(`/admin/user/${user.id}`)}></i>
                                            </td>
                                        </tr>
                                    )))
                                }
                            </tbody>
                        </table>
                    }
                </div>
            )
        }
    })
) 