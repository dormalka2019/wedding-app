import React from 'react';

export default () => (
    <div    id="inform_only_desktop"
            style={{
                height: '100vh',
                textAlign: 'center',
                margin: '20px',
                paddingTop: '10vh'
                }}>
        <h2>
            על-מנת לאפשר חווית ניהול גבוהה, דף זה מוגבל לתצוגה רק דרך מחשב בייתי
        </h2>
        <img style={{width: '200px', height: '200px', marginTop: '20px'}} src="/assets/images/desktop.svg" alt="desktop"/>
    </div>
)