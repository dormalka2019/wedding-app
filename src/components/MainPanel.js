import React,{useEffect, useState} from 'react';
import * as firebase from 'firebase';
import { convertToArr } from '../helpers/fucntions';
import { getMessageBalance } from '../actions/admin';

export const MainPanel = (props) => {
    
    const [isLoad,setIsLoad] = useState(true)
    const [users,setUsers] = useState([])
    const [balance, setBalance] = useState(null);

    useEffect(() => {
        if(isLoad){
            firebase.database().ref(`users`)
            .once('value', snapshot => {
                setUsers(convertToArr(snapshot.val()))
                setIsLoad(false)
                calculateAllPandingMessages();
            })
        }
        async function fetchBalance(){
            const smsBalance = await getMessageBalance();
            setBalance(smsBalance)
        }
        fetchBalance();
        return(() => {
            firebase.database().ref('users').off();
        })
    })


    const calculateTotalInvited = (user) => {
        let total = 0;
        try{
            user.invitedlist.forEach(person => total += parseInt(person.totalCount))
        } catch (err) {
            
        }
        return total;
    }

    const calculateTotalBudget = (user) => {
        let total = 0;
        try{
            for(let category in user.budgetlist){
                // eslint-disable-next-line no-loop-func
                user.budgetlist[category].forEach(row => total += parseInt(row.price))
            }
        } catch (err) {
            
        }
        return total;
    }

    const calculateTotalPandingMessage = (user) => {
        let total = 0;

        try{
            convertToArr(user.scheduleMsgs.list).forEach(msg => {
                if(msg.inProccess) total += parseInt(msg.mapedSendTo.length)
            })
        } catch(error) {

        }
        return total;
    }

    const calculateAllPandingMessages = () => {
        let total = 0;
        users.forEach((user) => {
            if(user.scheduleMsgs !== undefined){
                convertToArr(user.scheduleMsgs.list).forEach(msg => {
                    if(msg.inProccess) total += parseInt(msg.mapedSendTo.length)  
                })
            }
        })
        return total;
    }
    return(
        <div id="main_admin_panel" className="container page">
            <h1 className="title">לוח בקרה - מנהל מערכת</h1>
            <div className={`alert ${balance-calculateAllPandingMessages() > 0? 'alert-primary' : 'alert-danger'}`} role="alert" style={{display:'flex', justifyContent:'space-around'}}>
                <span>
                     מאזן הודעות גלובאל אס.אמ.אס {balance}
                </span>
                    קיימים {calculateAllPandingMessages()} הודעות בהמתנה
                <span>

                </span>
            </div>
            {
                isLoad? <img className="_loader" src="\assets\images\loader.gif" alt="loader"/> :
                <div>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>שם משתמש</th>
                                <th>אימייל</th>
                                <th>כמות מוזמנים</th>
                                <th>תקציב כולל</th>
                                <th>הודעות ממתינות לשליחה</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                users.map(user => (
                                    <tr key={user.uid}>
                                        <td>{user.displayName}</td>
                                        <td>{user.email}</td>
                                        <td>{calculateTotalInvited(user)}</td>
                                        <td>{calculateTotalBudget(user)}</td>
                                        <td>{calculateTotalPandingMessage(user)}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>


                </div>
            }
        </div>
    )
}