import React from 'react';
import { connect } from 'react-redux';


const mapStateToProp = (state) => ({
    alerts: state.alerts,
  })
  

export default connect(mapStateToProp)(({alerts}) => {
    const {showAlert,message} = alerts
    return(
        <div id="alert" className={`${showAlert? 'show':'hide'}`}>
            {message} הועתק בהצלחה
        </div>
    )
})
