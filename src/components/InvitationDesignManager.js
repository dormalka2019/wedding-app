import React, {useEffect, useState} from 'react';
import * as firebase from 'firebase';
import { convertToArr } from '../helpers/fucntions';
import {ButtonWithLoader} from './commons';
export const InvitationDesignManager = () => {
    const [idlist, setIdlist] = useState([])
    const [designName, setDesignName] = useState('');
    const [designKeys, setdesignKeys] = useState('');
    const [designPrice, setDesignPrice] = useState(0);
    const [showLoader,setShowLoader] = useState(false)

    useEffect(() => {
        firebase.database().ref('invitationsdesigns')
        .once('value', snapshot =>{
            setIdlist(convertToArr(snapshot.val()))
        })

        return(() => {
            firebase.database().ref('invitationsdesigns').off();
        })
    })
    const addToStore = () => {
        if(designName === '') return;
        setShowLoader(true)
        const updatedIdList = idlist;
        updatedIdList.push({designName,designKeys,designPrice,DesignPurchase: 0})
        firebase.database().ref('invitationsdesigns').push({designName,designKeys,designPrice,DesignPurchase: 0})
        .then(() => {
            setShowLoader(false)
            setDesignPrice(0)
            setdesignKeys('')
            setDesignName('')
        })
    }

    return(
        <div id="invitaion_design_manager" className="container">
            <h1>ניהול חנות הזמנות</h1>
            <div className="row">
                <div className="col">
                    <input className="form-control" type="text" placeholder="שם העיצוב" onChange={(event) => setDesignName(event.target.value)}/>
                </div>
                <div className="col">
                    <input className="form-control" type="text" placeholder="מילות מפתח" onChange={(event) => setdesignKeys(event.target.value)}/>
                </div>

                <div className="col">
                    <input className="form-control" type="number" placeholder="מחיר" onChange={(event) => setDesignPrice(event.target.value)}/>
                </div>
                <ButtonWithLoader showLoader={showLoader} className="btn btn-success" onClick={() => addToStore()}>הוספת עיצוב לחנות</ButtonWithLoader>
            </div>
            <table className="table table-hover">
            <thead>
                <tr>
                    <th>שם העיצוב</th>
                    <th>מחיר</th>
                    <th>מספר רכישות</th>
                </tr>
            </thead>
            <tbody>
                {
                    idlist.map((elem,index) => (
                        <tr key={index}>
                            <td>{elem.designName}</td>
                            <td>{elem.designPrice} ש"ח</td>
                            <td>{elem.DesignPurchase}</td>
                        </tr>
                    ))
                    
                }
            </tbody>
        </table>
    </div>
    )
}