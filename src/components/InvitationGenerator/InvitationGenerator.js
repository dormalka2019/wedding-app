import React from 'react';
import RightPannel from './layouts/RightPanel';
import Workspace from './layouts/Workspace';
import SuccessCreation from './layouts/SuccessCreation';
import { switchTemplate } from '../invitationTemplates';
import { isMobile } from '../../helpers/device-detect';
import { connect } from 'react-redux';
import { startCreateInvitation,updateInvitaion } from '../../actions/invitations';
import * as firebase from 'firebase';
import { matchPath } from 'react-router';

const templateConfigurationInit = {
    tempalteIndex: 2,
    nameid: '',
    prolog: '',
    body: '',
    hall: '',
    datetime: new Date(),
    groomParents: '',
    brideParents: '',
    watchCount: 0,
    epilog: '',
    validate: true
}
export class InvitationGeneratorObj extends React.Component{

    constructor(props){
        super(props);
        console.log(props.userdata);
        var templateConfiguration = {};
        Object.assign(templateConfiguration,templateConfigurationInit);
        Object.assign(templateConfiguration,props.templateConfigurationProps)
        this.state = {
            templateConfiguration,
            hideRightPannel: false,
            createdTemplateKey: null,
            editMode: false
        }
    }

    componentDidMount(){
        if(matchPath(window.location.pathname,"/invitations/edit/:id")){
            this.fetchData(this.props.match.params)
            this.setState({editMode: true})
        }
    }

    componentWillUnmount(){
        firebase.database().ref('invitations').off()
    }
    fetchData({id}){
        firebase.database().ref(`invitations/${id}`)
        .once('value', snapshot => {
            const templateConfiguration = snapshot.val();
            if (!templateConfiguration['datetime']) templateConfiguration['datetime'] = null;
            this.setState({templateConfiguration})
        })
    }

    onChangeConfigurations = (newConfiguration) => {
        const templateConfiguration = {}
        Object.assign(templateConfiguration,this.state.templateConfiguration)
        Object.assign(templateConfiguration,newConfiguration)
        this.setState({templateConfiguration});
    }

    onResetForm(event) {
        event.preventDefault()
        var templateConfiguration = templateConfigurationInit;
        templateConfiguration.tempalteIndex = this.state.templateConfiguration.tempalteIndex;
        this.setState({templateConfiguration});
    }

    onFinish() {
        const { templateConfiguration,editMode } = this.state;
        if(templateConfiguration.nameid == null || templateConfiguration.nameid === '') 
            templateConfiguration.nameid = Date.now();
        const currUser = firebase.auth().currentUser;
        if(currUser){
            if(editMode){
                this.props.updateInvitaion(templateConfiguration,this.props.match.params.id)
                .then(snapshot => {
                    this.setState({createdTemplateKey: this.props.match.params.id})
                })
            } else {
                templateConfiguration['creator'] = currUser.uid;
                this.props.startCreateInvitation(templateConfiguration)
                .then(key => {
                    this.setState({createdTemplateKey: key})
                })  
            }
        } else {
            const key = new Date().getTime().toString(36);
            firebase.database().ref(`invitations/${key}`).set(templateConfiguration)
            .then(snapshot => {
                this.setState({createdTemplateKey: key})
            })    
        }
    }


    generateWorkspace(){
        var worksapce_jsx = this.state.createdTemplateKey != null?
        (
            <SuccessCreation    templateKey={this.state.createdTemplateKey}/>
        ):
        (
            <Workspace
                onBackToEditForm={() => this.setState({hideRightPannel:false})}
                onFinish={() => this.onFinish()}
            >
            {switchTemplate(this.state.templateConfiguration)}     
            </Workspace>
        )

        return worksapce_jsx;
    }


    render(){
        return(
            <div id="invitation_generator" className={isMobile? 'mobile': ''}>
                <RightPannel
                    {...this.state.templateConfiguration}
                    hide={this.state.hideRightPannel}
                    onChangeConfigurations={(value) => this.onChangeConfigurations(value)}
                    onResetForm={this.onResetForm.bind(this)}
                    isLogin={this.props.isLogin}
                    onFinish={(event) => {
                        event.preventDefault()
                        isMobile? this.setState({hideRightPannel: true}) :
                        this.onFinish()
                    }}
                />
                {this.generateWorkspace()}
            </div>
        )
    }
} 


const mapDispatchToProps = (dispatch) => ({
    startCreateInvitation: (invitation) => dispatch(startCreateInvitation(invitation)),
    updateInvitaion: (update,id) => dispatch(updateInvitaion(update,id))
})

const mapStateToProps = (state) => ({
    isLogin: !!state.userdata.uid,
    invitations: state.invitations,
    templateConfigurationProps: {
        hall: state.userdata.hallName || '',
        datetime: state.userdata.datetime || new Date(),
    },
    userdata: state.userdata
})

export const InvitationGenerator = connect(mapStateToProps,mapDispatchToProps)(InvitationGeneratorObj);