import React from 'react';
import DateTimePicker from 'react-datetime-picker';
import { templatesList } from '../../../shared/templatesList';
import { isMobile } from '../../../helpers/device-detect';
import { Link } from 'react-router-dom';
import { CheckBox } from '../../commons/Checkbox';


export default (props) => {
    const {tempalteIndex,prolog,body,hall,datetime,groomParents, brideParents,epilog,nameid,validate} = props;
    const { templateType } = templatesList[tempalteIndex] !== undefined? templatesList[tempalteIndex] : 'none';
    const [templatesAfter, setTemplateAfter] = React.useState([]);
    React.useEffect(() => {
        var temps = templatesList.filter((temp => temp.freeMode === true));
        setTemplateAfter(temps);
    },[setTemplateAfter])
    return(
        <div className={`right_pannel ${isMobile? 'mobile':'not_mobile'} ${props.hide? 'hide': ''}`}>
            {props.isLogin && <Link to='/invitationsstore'><i className="fas fa-store-alt"></i></Link>}
            {props.isLogin && <Link to='/invitaionmanager' className="go_to">מעבר למנהל הזמנות</Link>}
            <div className="container">
                <h1>הגדרות תבנית</h1>
                <form>
                    <div className="form-group">
                        <label htmlFor="templateType">סוג האירוע</label>
                        <select id="templateType" 
                                className="form-control"
                                value={tempalteIndex}
                                onChange={(event) => props.onChangeConfigurations({tempalteIndex: event.target.value})}>
                            <option value="-1"></option>
                            {templatesAfter.map((item,key) => (
                                <option value={key} key={key}>{item.templateLabel}</option>
                            ))}                        
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="prolog">שם מזהה</label>
                        <input  type="text" 
                                id="nameid" 
                                value={nameid}
                                className="form-control"
                                onChange={(event) => props.onChangeConfigurations({nameid: event.target.value})}/>

                    {
                        templateType !== 'birthday_1'?
                        (
                            <React.Fragment>
                                <label htmlFor="prolog">משפט פתיחה</label>
                                <input  type="text" 
                                        id="prolog" 
                                        value={prolog}
                                        className="form-control"
                                        onChange={(event) => props.onChangeConfigurations({prolog: event.target.value})}/>
    
                            </React.Fragment>
                        ): null
                    }
                        <label htmlFor="body">גוף ההזמנה</label>
                        <textarea   rows="3" 
                                    value={body}
                                    className="form-control"
                                    onChange={(event) => props.onChangeConfigurations({body: event.target.value})}/>
                        <label htmlFor="hall">שם האולם/כתובת האירוע</label>
                        <input  type="text" 
                                id="hall" 
                                value={hall}
                                className="form-control"
                                onChange={(event) => props.onChangeConfigurations({hall: event.target.value})}/>

                        <label htmlFor="startdatetime">תאריך ושעה</label>
                        <DateTimePicker onChange={(datetime) => props.onChangeConfigurations({datetime: new Date(datetime).toJSON()})}
                                        value={new Date(datetime)}
                                        id="startdatetime"/>
                        
                        {
                            templateType === 'wedding_1'? 
                            (
                                <div>
                                    <label htmlFor="groomParents">הורי צד א</label>
                                    <input  type="text" 
                                        id="groomParents" 
                                        value={groomParents}
                                        className="form-control"
                                        onChange={(event) => props.onChangeConfigurations({groomParents: event.target.value})}/>

                                    <label htmlFor="brideParents">הורי צד ב</label>
                                    <input  type="text" 
                                        id="brideParents" 
                                        value={brideParents}
                                        className="form-control"
                                        onChange={(event) => props.onChangeConfigurations({brideParents: event.target.value})}/>

                                </div>
                            ) : null                        
                        }

                        <label htmlFor="epilog">משפט סיום</label>
                        <input  type="text" 
                                id="epilog" 
                                value={epilog}
                                className="form-control"
                                onChange={(event) => props.onChangeConfigurations({epilog: event.target.value})}/>
                        <CheckBox   id="validate" 
                                    value={validate} 
                                    style={{fontSize: 16,marginTop: '0.5rem'}}
                                    onChange={(value) => props.onChangeConfigurations({validate:value})}>בקשת אישור הגעה</CheckBox>

                    </div>

                    <div className="buttons">
                        <button className={`btn btn-${isMobile? 'primary btn-lg btn-block':'success'}`}
                                onClick={(event) => props.onFinish(event)}>
                                    { isMobile? 'צפייה': 'סיום' }
                        </button>

                        <button className={`btn btn-danger ${isMobile? 'btn-lg btn-block':''}`}
                                onClick={(event) => props.onResetForm(event)}>איפוס</button>
                    </div>
                </form>

            </div>
        </div>
    )
}

