import React from 'react';
import { isMobile } from '../../../helpers/device-detect';

const Workspace = (props) =>{
        return(
            <div id="workspace">
                <div className="innerWindow">
                    {props.children}
                </div>
                {
                    isMobile?
                    (
                        <div className="buttons">
                            <button className="btn btn-success btn-lg btn-block"
                                    onClick={() => props.onFinish()}>סיום</button>
                            <button className="btn btn-secondery btn-lg btn-block"
                                    onClick={() => props.onBackToEditForm()}>חזרה</button>
                        </div>
                    ):
                    (
                        <span></span>
                    )
                }
            </div>
        )
}

export default Workspace;