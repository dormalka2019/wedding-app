import React from 'react';
import { copyToClipboard } from '../../../helpers/fucntions';
import { connect } from 'react-redux';
import { startSetAlert } from '../../../actions/alerts';

const mapDispatchToProps = (dispatch) => ({
    startSetAlert: (message,timeout) => dispatch(startSetAlert(message,timeout))
})


export default connect(undefined,mapDispatchToProps)((props) => (
    <div id="workspace" className="success">
        <h1>הזמנה נוצרה בהצלחה</h1>
        <p>כעת תוכלו להעתיק את הקישור ולשלוח את ההזמנה לכל האנשים שתרצו להזמין</p>
        <div style={{margin: '10px'}}>
            <img style={{width: '200px'}} src="/assets/images/goal.png" alt="goal"/>
        </div>
        <input  value={`https://${window.location.hostname}/invitations/view/${props.templateKey}`} 
                id="url_to_invitaion"
                onChange={() => {}} />
        <button className="btn btn-success" 
                onClick={() => props.startSetAlert(copyToClipboard('url_to_invitaion'),1500)}>
            העתק ללוח
        </button>
    </div>
))