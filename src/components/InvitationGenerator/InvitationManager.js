import React from 'react';
import { connect } from 'react-redux';
import {  formatDate,copyToClipboard } from '../../helpers/fucntions';
import { withRouter } from 'react-router-dom';
import { deleteInvitation,updateInvitaion } from '../../actions/invitations';
import { startSetAlert } from '../../actions/alerts';
import { isMobile } from '../../helpers/device-detect';


const mapDispatchToProps = (dispatch) => ({
    deleteInvitation: (invitationId) => dispatch(deleteInvitation(invitationId)),
    startSetAlert: (message,timeout) => dispatch(startSetAlert(message,timeout)),
    updateInvitaion: (update,invitationId) => dispatch(updateInvitaion(update,invitationId))
})

const mapStateToProps = (state) => ({
    invitations: state.invitations,
    uid: state.userdata.uid
})

export const InvitationManager = connect(mapStateToProps,mapDispatchToProps)(
    withRouter((props) => {
    return(
        <div id="invitations" className="container">
            <h1>מנהל הזמנות</h1>
            {/* <button className="btn" onClick={() => props.history.push(`/invitationsstore/`)}>מעבר לחנות <i className="fas fa-store"></i></button> */}
            <table className="table table-hover">
                <thead>
                    <tr>
                        {!isMobile && <th>מזהה הזמנה</th>}
                        {!isMobile && <th>תאריך יצירת הזמנה</th>}
                        <th className="linkth">קישור לשיתוף</th>
                        {isMobile? <th><i className="far fa-eye"></i></th>:<th>מספר צפיות</th>}
                        <th className="addition">פעולות נוספות</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.invitations.map((invitation) => (
                            <tr key={invitation.id}>
                                {!isMobile && <td>{invitation.nameid}</td>}
                                {!isMobile && <td>{formatDate(new Date(invitation.createdAt))}</td>}
                                <td>
                                    <input      value={`https://${window.location.hostname}/invitations/view/${invitation.id}`} 
                                                id={invitation.id}
                                                
                                                onChange={() => {}} />
                                </td>
                                <td>{invitation.watchCount}</td>
                                <td>
                                    <i  className="fas fa-trash"
                                        onClick={() => props.deleteInvitation(invitation.id)}></i>
                                        {
                                            !isMobile &&                                        
                                            <i  className="far fa-copy"
                                                onClick={() => props.startSetAlert(copyToClipboard(invitation.id),1500)}></i>
                                        }
                                    <i  className="far fa-edit"
                                        onClick={() => props.history.push(`/invitations/edit/${invitation.id}`)}></i>
                                    <i  className={`far fa-check-square ${invitation.default && 'active'}`}
                                        onClick={() => {
                                            const currDefault = props.invitations.filter(inv => inv.default === true)[0]
                                            if(currDefault) props.updateInvitaion({default: false},currDefault.id)
                                            props.updateInvitaion({default: true},invitation.id)
                                        }}></i>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    )
}));