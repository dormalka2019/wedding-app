/* eslint-disable jsx-a11y/iframe-has-title */
import React from 'react';
import { siteConfigs } from '../shared/siteConfigs';
import { Link  } from 'react-router-dom';
import { SmileEmoji } from './commons';
export const Main = () => {
    return (
        <div id="main" >
                <section style={{paddingTop: '50px', textAlign: 'center'}}>
                    אנחנו מכירים את התסכול שבארגון חתונה ובעיקר בחלק של התיאום מוזמנים.<br/>
                    תיאום מוזמנים הוא תהליך חשוב בכל אירוע, חוסר תיאום יעיל עלול לצורך הרבה התעסקות ולהוצאות גבוהות<br/><br/>
                    <h1>{siteConfigs.siteName}</h1>
                    <br/>
                    <b>באנו לעשות לכם סדר!</b>
                    <br/>
                    כאן תוכלו למצוא מגוון כלים שבאמצעותם תוכלו לנהל לנהל את רשימת המוזמנים שלכם ביעילות, בין הכלים שתמצאו באתר:<br/>
                    יצירת הזמנה דיגיטלית, ניהול רשימת מוזמנים, אישורי הגעה, ניהול תקציב אירוע ועוד..<br/><br/>
                    בזכות '{siteConfigs.siteName}' תוכלו לחסוך זמן ואפילו לצאת עם בונוס קטן מהאירוע <SmileEmoji/>
                </section>

                <section className="second_section">
                    באמצעות מחולל ההזמנות הדיגיטליות-<br/> 
                    תוכלו ליצור הזמנה דיגיטלית ב<br/>
                    <b>60 שניות</b><br/><br/>
                    <Link to='/invitations/create'>נסו עכשיו!</Link><br/><br/>
                    גם ממחשב וגם מהסמארטפון<br/>
                    בקלות, במהירות, <br/>
                    <b>בחינם!</b>

                    <div className="video">
                        <div>צפו בסרטון הדרכה</div>
                        <iframe width="168" height="95" src="https://www.youtube.com/embed/ef4JZnU54VQ" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>                    
                    </div>
                </section>

                <section className="third_section">
                    רוצים ליידע את המוזמנים שלכם על האירוע המתקרב ולוודא איתם הגעה בצורה הכי יעילה שיש<br/>
                    תוכלו לרכוש חבילות SMS בעלות מינימלית, <br/>
                    לנהל בעצמיכם את תוכן ההודעות שישלחו, <br/>
                    לקבוע זמנים לשליחת ההודעות ולהגדיר קבוצות שליחה <br/><br/>
                    <Link to='/signup'>לחצו להרשמה מהירה!</Link><br/><br/>
                    <b>למה לנו לתת כל-כך הרבה כלים בחינם ושירות הודעות במחיר כה זול?</b><br/>
                    העידן הטכנולוגי יש ביכולתינו ליצור כלים שיסייעו לנו בשליטה וניהול<br/>
                    לפיתוח כלים אלו מוקדשים הרבה זמן וחשיבה<br/>
                    ועבורנו זאת זכות להעניק את הזמן שלנו לטובת הכלל בכדי לעזור ולשפר את חווית ניהול אירוע<br/>
                    וכמובן - לחסוך לכם <b>הרבה כסף!</b>
                </section>


            <section className="forth_section">
                <h1>לא רק חתונות!!</h1>
                :הכלים שפותחו למערכת נבחרו והותאמו לכל סוגי האירועים האפשריים בהם אתם זקוקים לניהול מוזמנים:<br/>
                ימי הולדת, בריתות, הרצאות ועוד...<br/>
                יש כלי שחסר לכם במערכת והייתם רוצים שיהיה?!? <b>דברו איתנו!</b>
            </section>

        </div>
    )
}