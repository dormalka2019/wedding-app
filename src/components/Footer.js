import React from 'react';
import { Link  } from 'react-router-dom';
import { siteConfigs } from '../shared/siteConfigs';
import { ContactForm } from './commons';
import { connect } from 'react-redux';

export const FooterFoo = ({pagesToShow,phone,name}) => (
    <footer>
        <div className="shadow"></div>
        <nav>
            <h4>שירותיי האתר</h4>
            <ul>
                {
                    siteConfigs[pagesToShow].map((item,index) => <Link key={index} to={item.url}>{item.label}</Link>)
                }
            </ul>
        </nav>
        <div className="contact">
            <h4>צור קשר</h4>
            <ContactForm phone={phone} name={name}/>
        </div>
    </footer>
)

const mapStateToProps = (state) => ({
    pagesToShow: !!state.userdata.isAdmin? 'adminPages': 'pages',
    phone: state.userdata.phone,
    name: state.userdata.displayName
})
export const Footer = connect(mapStateToProps)(FooterFoo)