import React, {useState} from 'react';

export default ({invitedsData}) => {
    const [search, setSearch] = useState('');

    const renderPersonResult = () => {
        if(search === '') return null;
        const persons = invitedsData.filter((person,index) => person.fullName.includes(search))
        if(persons.length > 0)
            return persons.map((person,index) => (
                    index < 3 && 
                    <div    className="result_person"
                            key={index}>
                        <span>{person.fullName}</span>        
                        <span>{person.totalCount} מוזמנים סה"כ</span>        
                        <span>{`${person.tableNum? 'שולחן ' + person.tableNum: 'ללא שולחן'}`}</span>        
                    </div>
                ))
        else return null;
    }

    return(
        <div className="seach_box">
            <input  type="search"
                    className="form-control"
                    value={search}
                    placeholder="חיפוש מוזמן"
                    onChange={(event) => setSearch(event.target.value)}/>
            { renderPersonResult() }
        </div>
    )
}