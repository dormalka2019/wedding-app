import React,{useEffect,useState} from 'react';
import { AlertMessage } from '../../commons';


export default ({onClose,tables,invitedsData, pickedTable,updateTableDetails,alert, updatePersonDetails}) => {
    const keyPressHendler = (event) => {
        if(event.key === 'Escape') onClose();
    }
    useEffect(() => {
        window.addEventListener('keydown', keyPressHendler,false);
        return(() => window.removeEventListener('keydown',keyPressHendler,false))
    })
    const currTable = tables[pickedTable];

    const calculateTotalCountOnTable = () => {
        let total = 0;
        invitedsData.forEach(person => total += person.tableNum === currTable.tableNum ? person.totalCount : 0)
        return `(${total} סה"כ)`
    }

    const [tableNum,setTableNum] = useState(tables[pickedTable].tableNum)
    return(
        <div className="table_editor">
            <div    className="clickable_background"
                    onClick={onClose.bind(this)}></div>

            <div className="editor_box">
                <div className="top">
                    <h1>עריכת שולחן {currTable.tableNum} {calculateTotalCountOnTable()}</h1>
                    <AlertMessage
                        isShown={alert.isShown}
                        type={alert.type}
                        message={alert.message}
                    />
                </div>
                <div className="body">
                    <div className="editor_box_table_view">
                        <div    style={{height: currTable.height !== 3? '250px':'125px', 
                                        width: currTable.width !== 3? '250px':'125px',
                                        borderRadius: currTable.type === 'circle'? '50%':'0px'}}>
                                <input  type="number"
                                        placeholder="מספר שולחן"
                                        value={tableNum? tableNum : ''}
                                        className="form-control"
                                        onChange={(event) => {
                                            setTableNum(event.target.value);
                                            updateTableDetails(pickedTable, {tableNum: event.target.value})}}/>
                        </div>

                    </div>
                    <div className="person_picker">
                        <div className="outPerson">
                            <h4>ללא שולחן</h4>
                            <ul>
                                {
                                    invitedsData.sort((p1,p2) => {
                                                    if(p1.fullName < p2.fullName) { return -1; }
                                                    if(p1.fullName > p2.fullName) { return 1; }
                                                    return 0;
                                    })
                                    .map((person,index) => {
                                        return (!person.tableNum) && 
                                        (<li  key={index}
                                            onDoubleClick={() => updatePersonDetails(index,{tableNum: currTable.tableNum}, false)}>{person.fullName}  ({person.totalCount})</li>)
                                    })
                                }
                            </ul>
                        </div>
                        <div className="inPerson">
                            <h4>משוייכים לשולחן הנוכחי</h4>
                            <ul>
                                {
                                    invitedsData.sort((p1,p2) => {
                                        if(p1.fullName < p2.fullName) { return -1; }
                                        if(p1.fullName > p2.fullName) { return 1; }
                                        return 0;
                                    })
                                    .map((person,index) => {
                                        return (currTable.tableNum && person.tableNum === currTable.tableNum) && 
                                        (<li  key={index}
                                            onDoubleClick={() => updatePersonDetails(index,{tableNum: null}, true)}>{person.fullName} ({person.totalCount})</li>)
                                    })
                
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}