import React from 'react';
import {withRouter } from 'react-router-dom';
import queryString from 'query-string';
import TableEditor from './TableEditor';

export default withRouter(class SittingArrangeViewMode extends React.Component{
    constructor(props){
        super(props)
        this.state={
            tableMap: this.props.tableMap || [],
            tableWidth: 0,
            tables: this.props.tables || [],
            pickedTable: null,
            invitedsData: this.props.invitedsData || [],
            hoverView: {
                list: [],
                posX: 0,
                posY: 0
            },
            alert: {
                type: 'alert-primary',
                message: '',
                isShown: false
            },
        }
        this.mytable = React.createRef();
    }

    componentDidMount(){
        this.setState({tableWidth: this.mytable.current.offsetWidth})        
        window.addEventListener('resize',this.updateDimensions,false);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.updateDimensions,false);
    }
    
    updateDimensions = () =>{
        this.setState({tableWidth: this.mytable.current.offsetWidth})
    }
    
    componentWillUpdate(nextProps,nextState){
        if(nextProps.tables !== this.state.tables) this.setState({tables: nextProps.tables})
        const values = queryString.parse(this.props.location.search);
        if((!this.state.pickedTable || nextState.pickedTable !== this.state.pickedTable) && 
            nextProps.tables.length >=  Number.parseInt(values.tableIndex) + Number.parseInt(6)){
            this.setState({pickedTable: (Number.parseInt(values.tableIndex) + Number.parseInt(6))})
        }
    }

    componentWillReceiveProps(nextProps, nextContext){
        if(nextProps.tableMap !== this.state.tableMap) this.setState({tableMap: nextProps.tableMap})
        if(nextProps.invitedsData !== this.state.invitedsData) this.setState({invitedsData: nextProps.invitedsData})
    }
    
    onEditTable(index){
        const { pathname } = this.props.location;
        this.props.history.push(`${pathname}?tableIndex=${index-6}`)
    }

    onMouseOverTable(index){
        var { tables,invitedsData,hoverView } = this.state;
        const filterdData = invitedsData.filter(person => person.tableNum === tables[index].tableNum);
        hoverView.list = filterdData;
        this.setState({hoverView})
    }

    calculatTableSittingCount(index){
        var { tables,invitedsData } = this.state;
        let total = 0;
        const filterdData = invitedsData.filter(person => person.tableNum === tables[index].tableNum);
        filterdData.forEach(person => total+= person.totalCount);
        return `(${total})`
    }
    
    onMouseMoveTable(event){
        var {hoverView} = this.state;
        hoverView.posX = event.clientY+15;
        hoverView.posY = event.clientX+15;
        this.setState({hoverView})
    }

    onMouseLeaveTable() {
        var {hoverView} = this.state;
        hoverView.list = [];
        this.setState({hoverView})
    }

    onCloseTableEditor(){
        const { pathname } = this.props.location;
        this.props.history.push(`${pathname}`);
        this.props.onSaveData();
    }


    updateTableDetails(pickedTable,update){
        var { tables,invitedsData } = this.state;
        const oldTableNum = JSON.stringify(tables[pickedTable].tableNum);
        tables[pickedTable] = {
            ...tables[pickedTable],
            ...update
        }
        if(tables.filter(table => table.tableNum === tables[pickedTable].tableNum).length > 1) {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'לא ניתן לסמן יותר משולחן אחד באותו מספר'
                },
            })
            tables[pickedTable] = {
                ...tables[pickedTable],
                tableNum: oldTableNum? JSON.parse(oldTableNum) : undefined
            }
        } else {
            if(oldTableNum){
                invitedsData = invitedsData.map(person => {
                    if(person.tableNum === JSON.parse(oldTableNum)){
                        person.tableNum = tables[pickedTable].tableNum;
                    }
                    return person;
                })                    
            }
            this.setState({
                alert: {
                    isShown: false,
                    type: 'alert-danger',
                    message: ''
                },invitedsData
            })
        }
        this.setState({tables})
    }

    updatePersonDetails(index, update, force) {
        var { invitedsData } = this.state;
        if(!force && !update.tableNum){
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-danger',
                    message: 'תחילה יש לזין מספר שולחן'
                },
            })
        }
        else{
            
            invitedsData[index] = {
                ...invitedsData[index],
                ...update
            }    
            this.setState({
                alert: {
                    isShown: false,
                    type: 'alert-danger',
                    message: ''
                },
                invitedsData
            })
        }
    }


    render(){
        const { tableMap,tableWidth,tables,pickedTable,alert,invitedsData,hoverView } = this.state;
        return(
            <div>
                {
                    hoverView.list.length > 0 && 
                    <ul id="hoverList" style={{top: `${hoverView.posX}px`, left: `${hoverView.posY}px`}}>
                        {
                            hoverView.list.map((person,index)=> <li key={index}>{person.fullName}</li>)
                        }
                    </ul>
                }


                {pickedTable && tables.length !== 0  && <TableEditor    tables={tables}
                                                                        alert={alert}
                                                                        pickedTable={pickedTable}
                                                                        updateTableDetails={this.updateTableDetails.bind(this)}
                                                                        invitedsData={invitedsData}
                                                                        updatePersonDetails={this.updatePersonDetails.bind(this)}
                                                                        onClose={this.onCloseTableEditor.bind(this)}/>}
                <table  className="table"
                        ref={this.mytable}>
                    <tbody>
                    {
                        tableMap.map((row,rowindex) => {
                            return (
                                <tr key={rowindex}>
                                    {
                                        row.map((col,colindex) =>  {
                                            return (
                                                col.isShown && 
                                                <td key={colindex} 
                                                    rowSpan={col.rowSpan}
                                                    colSpan={col.colSpan}
                                                    style={{height: tableWidth/tableMap.length, width: tableWidth/tableMap.length}}>
                                                        {
                                                            tables.map((table,index) => {
                                                                if (colindex === table.posX && rowindex === table.posY) {
                                                                    const { type, text,tableNum }= table;
                                                                    return (
                                                                        <div    className="table_view_mode_cell"
                                                                                key={index}
                                                                                onClick={() => this.onEditTable(index)}
                                                                                onMouseEnter={() => this.onMouseOverTable(index)}
                                                                                onMouseMove={this.onMouseMoveTable.bind(this)}
                                                                                onMouseLeave={this.onMouseLeaveTable.bind(this)}
                                                                                style={{ borderRadius: type === 'circle'? '50%': '0px'}}>
                                                                            {tableNum || text}<br/>
                                                                            {this.calculatTableSittingCount(index)}
                                                                        </div>
                                                                    )                                                                
                                                                }
                                                                return null;                                                            
                                                            })
                                                        }
                                                </td>
                                            ) 
                                        })
                                    }
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>                
            </div>
        )
    }
})