import React from 'react';
import { connect } from 'react-redux';
import { startSetData } from '../../actions/sittingData';
import { startSetList } from '../../actions/invitedsData';
import SittingArrangeEditMode from './SittingArrangeEditMode/SittingArrangeEditMode';
import SittingArrangeViewMode from './SittingArrangeViewMode/SittingArrangeViewMode';
import SearchBox from './SittingArrangeViewMode/SearchBox';
import Sticky from 'react-sticky-el';


class SittingArrangeObj extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            userdata: {...props.userdata},
            tableMap: this.props.tableMap || [],
            tables: this.props.tables || [],
            invitedsData: this.props.invitedsData || [],
            editMode: false
        }
    }


    componentWillReceiveProps(nextProps, nextContext){

        if(nextProps.tableMap !== this.state.tableMap) this.setState({tableMap: nextProps.tableMap})
        if(nextProps.tables !== this.state.tables) this.setState({tables: nextProps.tables})
        if(nextProps.invitedsData !== this.state.invitedsData) this.setState({invitedsData: nextProps.invitedsData})
    }

    componentWillMount(){
        this.initEditor();
    }

    initEditor(){
        const { tables,tableMap } = this.state;
        tableMap === [] && this.setTableMapSize(10,10);
        if(tables === []){
            tables.push(this.createTable(3,3,'cube','3x3'))
            tables.push(this.createTable(3,6,'cube','3x6'))
            tables.push(this.createTable(3,9,'cube','3x9'))
            tables.push(this.createTable(6,3,'cube','6x3'))
            tables.push(this.createTable(9,3,'cube','9x3'))
            tables.push(this.createTable(4,4,'circle','קטן'))
            tables.push(this.createTable(6,6,'circle','גדול'))
            this.setState(tables)    
        }
    }


    setTableMapSize(rows, cols){
        let tableMap = [];
        if(rows < 10 || rows > 35) return;
        if(cols < 10 || cols > 35) return;

        const { tables } = this.state;
        var tableToDelete = [];

        for(let row = 0; row < rows; row ++){
            let tableRow = [];
            for(let col = 0; col < cols; col ++){
                let tableCol = { row, col, isShown: true, colSpan: 1, rowSpan: 1 }
                tableRow.push(tableCol)
            }
            tableMap.push(tableRow);
        }
        this.setState({tableMap})
        
        tables.forEach((tab,index) => {
            const {posX,posY,height,width} = tab;
            if(posX !== -1 && posY !== -1) {
                try{
                    tableMap[posY][posX].rowSpan = Math.floor(height/3);
                    tableMap[posY][posX].colSpan = Math.floor(width/3);
                    for(var i = posX; i < posX + Math.floor(width/3); i++){
                        for(var j = posY; j < posY + Math.floor(height/3); j++){
                            tableMap[j][i].isShown = false;
                        }
                    }
                    tableMap[posY][posX].isShown = true;        
                }
                catch (e) {
                    tableToDelete.push(index)
                }
            }
        })

        tableToDelete.forEach((k) => tables.splice(k,1));
        this.setState({tableMap,tables})
    }
    
    createTable(height,width, type, text){
        const table = { height,width,type,text}
        table.posX = -1;
        table.posY = -1;
        return table;
    }

    toggleMode(event){
        event.preventDefault();
        const { editMode,tableMap,tables } = this.state;
        if(editMode) this.props.startSetData(tableMap,tables);
        this.setState({editMode: !editMode});
    }

    saveData(){
        const { tableMap,tables,invitedsData } = this.state;
        this.props.startSetData(tableMap,tables);
        this.props.startSetList(invitedsData);
    }

    render(){
        const { editMode,invitedsData } = this.state;
        return (
            <div id="sitting_arrange">
                <button className="btn btn-primary"
                        style={{marginBottom: editMode? '0px':'5px'}}
                        onClick={this.toggleMode.bind(this)}>{editMode? 'סיום עריכה' : 'עריכת מפה'}</button>
                { !editMode && 
                    <Sticky id="sticky_element">
                        <SearchBox invitedsData={invitedsData}/>
                    </Sticky>
                }
                { editMode && <SittingArrangeEditMode   {...this.state} 
                                                        invitedsData={invitedsData}
                                                        setTableMapSize={this.setTableMapSize.bind(this)}/> } 
                { !editMode && <SittingArrangeViewMode {...this.state}
                                                        onSaveData={this.saveData.bind(this)}
                                                        invitedsData={invitedsData}/>}           
                <div style={{clear:'both'}}></div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    startSetData: (tableMap, tables) => dispatch(startSetData({tableMap, tables})),
    startSetList: (list) => dispatch(startSetList(list))
})

const mapStateToProp = (state) => ({
    tableMap: state.sittingData.tableMap,
    tables: state.sittingData.tables,
    invitedsData: state.invitedsData
})

export const SittingArrange = connect(mapStateToProp,mapDispatchToProps)(SittingArrangeObj);

