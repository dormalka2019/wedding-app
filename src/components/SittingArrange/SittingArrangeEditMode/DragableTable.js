import React from 'react'
import { ItemTypes } from './Constants'
import { useDrag } from 'react-dnd'

function DragableTable({type,text,onMouseDown,onDoubleClick}) {
  const [{isDragging}, drag] = useDrag({
    item: { type: ItemTypes.TABLE },
		collect: monitor => ({
			isDragging: !!monitor.isDragging(),
		}),
  })

  return (
    <div
      ref={drag}
      className="dragable_table"
      onMouseDown={() => onMouseDown()}
      onDoubleClickCapture={onDoubleClick && onDoubleClick.bind(this)}
      style={{
        opacity: isDragging ? 0.5 : 1,
        borderRadius: type === 'circle'? '50%': '0px'
      }}
    >
      {text}
    </div>
  )
}

export default DragableTable;