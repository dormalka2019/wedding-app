import React from 'react'
import { ItemTypes } from './Constants'
import { useDrop } from 'react-dnd'


function DropableTableCell({ x, y, children,observer,emptyCell }) {
    var tablePosition = [0,0];
    const emitChange = () => {
        observer(tablePosition)
    }

      
    const moveTable = (toX,toY) => {
        tablePosition = [toX, toY]
        emitChange();
    }

    const canMoveDrop = () => {
        return true;
    }
    const [{ isOver,canDrop }, drop] = useDrop({
      accept: ItemTypes.TABLE,
      drop: () => moveTable(x, y),
      canDrop: () => canMoveDrop(),
      collect: monitor => ({
        isOver: !!monitor.isOver(),
        canDrop: emptyCell(x,y),
      }),
    })
  
    return (
      <div
        ref={drop}
        style={{
          position: 'relative',
          width: '100%',
          height: '100%',
        }}
      >
        {children}
        {isOver && !canDrop && <Overlay color="red" />}
        {isOver && canDrop && <Overlay color="green" />}
      </div>
    )
  }
  
const Overlay = ({color}) => (
    <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            height: '100%',
            width: '100%',
            zIndex: 1,
            opacity: 0.5,
            backgroundColor: color,
          }}
    />
)

export default DropableTableCell