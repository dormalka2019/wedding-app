import React from 'react';
import DragableTable from './DragableTable';

export default ({tables,onPickTable}) => {
    return(
        tables.map((table,index) => 
        (table.posX === -1 && table.posY === -1) && 
        <div    key={index} 
                style={{height: table.height !== 3? '50px':'25px', width: table.width !== 3? '50px':'25px'}}
                className="table_view_elem">
                <DragableTable  key={index}
                                index
                                {...table}
                                onMouseDown={() => onPickTable(index)}/>
        </div>)
    )
}