import React from 'react';
import DragableTable from './DragableTable';
import { DndProvider } from 'react-dnd'
import Backend from 'react-dnd-html5-backend'
import DropableTableCell from './DropableTableCell';
import PickTableRow from './PickTableRow';
import Sticky from 'react-sticky-el';

export default class SittingArrangeEditMode extends React.Component {
    constructor(props){
        super(props)
        this.state={
            tableMap: this.props.tableMap || [],
            tableWidth: 0,
            tables: this.props.tables || [],
            invitedsData: this.props.invitedsData || [],
            pickedTable: -1,
            pickedNewTable: false,
            canDrop: true
        }
        this.mytable = React.createRef();
    }

    componentWillUpdate(nextProps, nextState){
        if(nextProps.tableMap !==  this.state.tableMap){
            this.setState({tableMap: nextProps.tableMap})
        }
    }


    componentDidMount(){
        this.setState({tableWidth: this.mytable.current.offsetWidth})    
        window.addEventListener('resize',this.updateDimensions,false);
    }
    
    componentWillUnmount(){
        window.removeEventListener('resize',this.updateDimensions,false);
    }
    
    updateDimensions = () => {
        this.setState({tableWidth: this.mytable.current.offsetWidth})
    }

    updateTablePosition([posX,posY]){
        var { tables,pickedTable,pickedNewTable,tableMap } = this.state;
        const tablesCpy = JSON.parse(JSON.stringify(tables));
        const tableMapCpy = JSON.parse(JSON.stringify(tableMap));
        try{
            if(posX === -1 && posY === -1){
                this.removeAllPersonFromTable(tables[pickedTable].tableNum);
                this.resetTableMapCell(tables[pickedTable]);
                tables.splice(pickedTable,1);
            }
            else if(!this.checkEmptyCell(posX,posY)) return;
            else if(pickedNewTable){
                var newTable = JSON.parse(JSON.stringify(tables[pickedTable]));
                newTable.posX = posX;
                newTable.posY = posY;
                tables.push(newTable);
                this.updateTableMapCell(newTable);
            } else {
                this.resetTableMapCell(tables[pickedTable]);
                tables[pickedTable].posX = posX;
                tables[pickedTable].posY = posY;
                this.updateTableMapCell(tables[pickedTable]); 
            }
        }
        catch(e) {
            tables = tablesCpy;
            this.setState({tableMap: tableMapCpy})
            alert('לא ניתן לשבץ שולחן במשבצת זאת!')
        }
        this.setState({tables})
    }


    removeAllPersonFromTable(tableNum){
        var { invitedsData } = this.state;
        if(tableNum){
            invitedsData = invitedsData.map(person => {
                if(person.tableNum === tableNum){
                    person.tableNum = null;
                }
                return person;
            })
            this.setState({invitedsData});    
        }
    }

    resetTableMapCell(oldTable){
        const {tableMap} = this.state;
        const {posX,posY,height,width} = oldTable;
        tableMap[posY][posX].rowSpan = 1;
        tableMap[posY][posX].colSpan = 1;
        for(var i = posX; i < posX + Math.floor(width/3); i++){
            for(var j = posY; j < posY + Math.floor(height/3); j++){
                tableMap[j][i].isShown = true;
            }
        }
        this.setState({tableMap});

    }

    updateTableMapCell(newTable){
        const {tableMap} = this.state;
        const {posX,posY,height,width} = newTable;
        tableMap[posY][posX].rowSpan = Math.floor(height/3);
        tableMap[posY][posX].colSpan = Math.floor(width/3);

        for(var i = posX; i < posX + Math.floor(width/3); i++){
            for(var j = posY; j < posY + Math.floor(height/3); j++){
                tableMap[j][i].isShown = false;
            }
        }
        tableMap[posY][posX].isShown = true;
        this.setState({tableMap});
    }

    checkEmptyCell(y,x){
        try{
            const {tables,pickedTable} = this.state;
            const currTable = pickedTable !== -1? tables[pickedTable] : {height: 0, width: 0};
            const {height,width} = currTable;
    
            for(let i = 0; i< tables.length;i++){
                const { posX, posY } = tables[i];
                const height2 = tables[i].height;
                const width2 = tables[i].width;
                for(var w = 0; w < Math.floor(width2/3); w++){
                    if(((posX + w) >= y && (posX + w) < (y + Math.floor(width/3))) && (posY >= x && posY < (x + Math.floor(height/3)))) {
                        return false
                    }    
                }
                for(var h = 0; h < Math.floor(height2/3); h++){
                    if((posX >= y && posX < (y + Math.floor(width/3))) && ((posY + h) >= x && (posY + h) < (x + Math.floor(height/3)))) {
                        return false
                    }    
                }
    
            }    
        } catch (e) {}
        return true;
    }

    render(){
        const { tableMap,tableWidth,tables } = this.state;
        return(
            <DndProvider backend={Backend}>
                <Sticky id="sticky_element">
                    <div className="form-group-lg row" style={{marginTop: '10px'}}>
                        <label htmlFor="mapsize" className="col-sm-2 col-form-label" style={{textAlign: 'center'}}>גודל מפה</label>
                        <div className="col-sm-10">
                            <input  type="number" 
                                    id="mapsize"
                                    value={tableMap.length}
                                    className="form-control"
                                    step="5"
                                    onChange={(event) => this.props.setTableMapSize(event.target.value,event.target.value)}/>

                        </div>
                    </div>
                    <div className="table_views">  
                        <PickTableRow   {...this.state}
                                        onPickTable={(index) => this.setState({pickedTable:index,pickedNewTable: true})}/>
                    </div>
                </Sticky>
                <table  className="table"
                        ref={this.mytable}>
                    <tbody>
                    {
                        tableMap.map((row,rowindex) => {
                            return (
                                <tr key={rowindex}>
                                    {
                                        row.map((col,colindex) =>  {
                                            return (
                                                col.isShown && 
                                                <td key={colindex} 
                                                    rowSpan={col.rowSpan}
                                                    colSpan={col.colSpan}
                                                    style={{height: tableWidth/tableMap.length, width: tableWidth/tableMap.length}}>
                                                    <DropableTableCell  x={colindex} 
                                                                        y={rowindex}
                                                                        emptyCell={this.checkEmptyCell.bind(this)}
                                                                        observer={this.updateTablePosition.bind(this)}>
                                                        {
                                                            tables.map((table,index) => {
                                                                    if (colindex === table.posX && rowindex === table.posY) {
                                                                        return <DragableTable   {...table}
                                                                                                onMouseDown={() => this.setState({pickedTable:index,pickedNewTable:false})}
                                                                                                onDoubleClick={() => this.updateTablePosition([-1,-1])}
                                                                                                key={index}/>
                                                                    }
                                                                    return null;                                                            
                                                                }
                                                            )
                                                        }
                                                    </DropableTableCell>
                                                </td>
                                            ) 
                                        })
                                    }
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>                
            </DndProvider>

        )
    }
}