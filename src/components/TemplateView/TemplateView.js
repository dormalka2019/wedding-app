import React from 'react';
import * as firebase from 'firebase';
import { switchTemplate } from '../invitationTemplates'
import queryString from 'query-string';
import TemplateViewNotify from './TemplateViewNotify';

export class TemplateView  extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: null,
            fullMode: false
        }
        this.fetchData(props.match.params)
    }

    fetchData({id}){
        var invitationId = id;
        var {p} = queryString.parse(this.props.location.search);
        firebase.database().ref(`invitations/${invitationId}`)
        .once('value', snapshot => {
            const data = snapshot.val();
            if(p){
                data['phone'] = p;
                this.setState({data,fullMode: data['validate']})
            } else {
                this.setState({data, fullMode: false})
            }
            if(!data['watchCount']) data['watchCount'] = 0;
            data['watchCount'] += 1;
            firebase.database().ref(`invitations/${invitationId}/watchCount`)
            .set(data['watchCount'])
        })
    }

    render(){
        const { data } = this.state;
        return (
            <div id="template_view">
                {
                    data != null? 
                        switchTemplate(this.state.data) 
                    :
                    (
                        <div>Not Found</div>
                    )
                }
                <div className="box">
                    {
                        data != null? 
                        (
                            <a href={`https://waze.com/ul?q=${this.state.data.hall}`} target="_blank" rel="noopener noreferrer">ניווט למקום האירוע <i className="fab fa-waze"></i></a>
                        ): null

                    }
                    {
                        this.state.fullMode && <TemplateViewNotify  userId={this.state.data.creator}
                                                                    invitationId={this.props.match.params.id}
                                                                    phone={queryString.parse(this.props.location.search).p}/>
                    }
                </div>

            </div>
        )    
    }
}
