import React from 'react';


export default ({onChange,label,value}) => (
    <div className="notify_input">
        {label}
        <div>
            <i  className="fas fa-arrow-circle-up"
                onClick={() => onChange(Number.parseInt(value)+1)}></i>
                <span>
                    {value === 0? '-' : value}
                </span>
            <i className="fas fa-arrow-circle-down"
                onClick={() => onChange(Number.parseInt(value)-1)}></i>
        </div>
    </div>
)