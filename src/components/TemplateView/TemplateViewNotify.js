import React, { useState, useEffect} from 'react';
import * as firebase from 'firebase';
import TemplateViewNotifyInput from './TemplateViewNotifyInput';
import axios from 'axios';

const INITIAL_PERSON = {
    children: 0,
    approved: 0,
    vegetarian: 0,
    meet: 0,
    vegan: 0
}

export default ({userId, invitationId, phone}) => {
    const [invitedlist, setinvitedlist] = useState([]);
    const [pickedPerson, setPickedPerson] = useState(INITIAL_PERSON);
    const [welcomeMsg, setWelcomMsg] = useState(null);
    const [addition, setAddition] = useState(false);


    const notifyOwner = (status) => {
        firebase.database().ref(`tokens/${userId}/`)
        .once('value',snapshot => {
            const {msgToken, msgTokenMobile} = snapshot.val() 
            if(snapshot.val()){
                var headers = {
                    'Content-Type': 'application/json',
                    'Authorization': 'key=AAAALZif8Kw:APA91bENez71F9bgbJN3RvWcmR6pq3s6y14uDbA8qZQb3xFdmSRSimikPPrnhAriynS8F9Z9nthp_UwwPi8C8WtA5vsW9VBy6tMWl4Vh-JNvKEdp6XzHSCYqul1Yf-AekHacu25LXxAw'
                }        
                var message = {
                    data: {
                        notification: {
                            title: 'התקבל אישור הגעה נוסף'
                        }
                    }

                }
                if(status === 1){
                    message.data.notification.body = `איזה כייף! ${pickedPerson['fullName']} אישר את הגעתו`;
                } else if (status === 0){
                    message.data.notification.body = `${pickedPerson['fullName']} הודיע שאולי יגיע`;
                } else {
                    message.data.notification.body = `${pickedPerson['fullName']} הודיע שלא יגיע`;
                }
                if(msgToken) {
                    message.to = msgToken; 
                    axios.post('https://fcm.googleapis.com/fcm/send',message, {headers})
                    .finally(() => {
                        if(msgTokenMobile) {
                            message.to = msgTokenMobile; 
                            axios.post('https://fcm.googleapis.com/fcm/send',message, {headers})
                        }
                    })
                }
            } else {
                console.error('no token exist')
            }
        }, error => {
            console.error(error)
        })
    }

    const save = (approved) =>{
        const updatedList = invitedlist.map(person => {
            if(person.phone === phone){
                if(approved === 1 || approved === 0){
                    pickedPerson['meet'] = pickedPerson['totalCount'] - pickedPerson['vegan'] - pickedPerson['vegetarian'];
                } else {
                    pickedPerson['children'] = 0;
                    pickedPerson['meet'] = 0;
                    pickedPerson['vegan'] = 0; 
                    pickedPerson['vegetarian'] = 0;
                    pickedPerson['totalCount'] = 0;
                }
                pickedPerson['approved'] = approved;
                return {...person,...pickedPerson, confirm: approved, updateDate: new Date().toJSON()};
            }
            return person;
        })
        firebase.database().ref(`users/${userId}/invitedlist`)
        .set(updatedList)
        .then(() => {
            if(approved === 1){
                setWelcomMsg('תודה רבה על העדכון! מתרגשים לראותכם =)!')
            } else if (approved === 0){
                setWelcomMsg('תודה רבה על העדכון! מקווים לראותכם =)!')
            } else {
                setWelcomMsg('תודה רבה על העדכון!')
            }
            notifyOwner(approved);
        })
    }

    useEffect(() => {
        if(invitedlist.length === 0){
            firebase.database().ref(`users/${userId}/invitedlist`)
            .on('value', snapshot => {
                setinvitedlist(snapshot.val())
                const pickedPerson = {
                    ...snapshot.val().filter(person => person.phone === phone)[0],
                    ...INITIAL_PERSON
                };
                setPickedPerson(pickedPerson)
            })    
        }
    })
    return(
        <div id="notifying">
            <br/>
            {
                welcomeMsg? <span>{welcomeMsg}</span>:
                    <span>
                    היי {pickedPerson['fullName']}! <br/>
                    על-מנת שנוכל להערך בהתאם נשמח אם תוכל/י לעדכן את סוג המנות שתרצו ולאשר את הגעתכם                    
                    <br/><br/>
                    <TemplateViewNotifyInput
                        label="אורחים"
                        onChange={(value) => (value >= 0 && value >= (pickedPerson['children'] + pickedPerson['vegetarian'] + pickedPerson['vegan'])) && setPickedPerson({...pickedPerson, totalCount: value})}
                        value={pickedPerson['totalCount']}
                    />
                    {
                        !addition?
                        <button className="btn plus" onClick={() => setAddition(true)}><i className="fas fa-plus"></i> בקשות מיוחדות</button>: (
                            <span>
                            <TemplateViewNotifyInput
                            label="ילדים"
                            onChange={(value) => value >= 0 && value <= (pickedPerson['totalCount'] - pickedPerson['vegetarian'] - pickedPerson['vegan']) && setPickedPerson({...pickedPerson, children: value})}
                            value={pickedPerson['children']}
                            />
                            <TemplateViewNotifyInput
                            label="צמחוניים"
                            onChange={(value) => value >= 0 && value <= (pickedPerson['totalCount'] - pickedPerson['children'] - pickedPerson['vegan']) && setPickedPerson({...pickedPerson, vegetarian: value})}
                            value={pickedPerson['vegetarian']}
                            />
                            <TemplateViewNotifyInput
                            label="טבעוניים"
                            onChange={(value) => value >= 0 && value <= (pickedPerson['totalCount'] - pickedPerson['children'] - pickedPerson['vegetarian']) && setPickedPerson({...pickedPerson, vegan: value})}
                            value={pickedPerson['vegan']}
                            />
                            </span>
                        )
                    }
                    <br/>
                        <button className="btn"
                            onClick={() => save(1)}>יאללה אנחנו מגיעים! <i className="far fa-thumbs-up"></i></button>
                        <button className="btn" onClick={() => save(0)}>
                            אולי אנחנו נגיע <i className="fas fa-question"></i>
                        </button>
                        <button className="btn" style={{marginBottom: '10px'}} onClick={() => save(-1)}>
                            אנחנו לא נגיע, תודה! <i className="fas fa-user-times"></i>
                        </button>
                    </span>
                }

        </div>
    )
}