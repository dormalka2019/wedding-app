import React,{useState,useEffect,useRef} from 'react';
import DataCell from './DataCell';
import {isMobile} from '../../helpers/device-detect';
import useForceUpdate from 'use-force-update';


export default ({rows,categoryapi,label,dataApi,onEdit, onRemove, invitedlistCount}) => {
    const [hide,setHide] = useState(false);
    const [rowsState, setRowsState] = useState(rows);
    const [showCategory, setShowCategory] = useState(true)
    const forceUpdate = useForceUpdate();
    var flag = useRef(true);
    useEffect(() => {
        if(flag){
            setRowsState(rows)
            flag.current = false;
        }
    },[rows])

    const generateRow = () => {
        let total = 0;
        return  rowsState.map((row,rowindex) => {
            if(row['perPerson'] !== undefined && row['perPerson']){
                total += (Number.parseInt(row['price']) * invitedlistCount)
            } else {
                total += row['price'] !== undefined && Number.parseInt(row['price'])
            }
            return rowindex !== rowsState.length-1?
                (<tr key={rowindex}>
                    { rowindex === 0 && <td rowSpan={rowsState.length} 
                                            key="-1" 
                                            style={{display: hide && 'none'}}
                                            className="category_label"
                                            onClick={() => setHide(!hide)}>{label}</td> }
                    { dataApi.map(({api},callindex) => api !== 'categoty' && 
                        <DataCell   key={callindex}
                                    value={row[api]}
                                    hide={hide}
                                    forceDisable={api ==='expence' && categoryapi !== 'others'}
                                    onChange={(value) => {
                                                row[api] = value
                                                onEdit(categoryapi,rowindex,row)}}
                        />)
                    }
                    {categoryapi === 'others' && !hide && 
                    <td><i  className="fas fa-trash"
                            onClick={() => onRemove(row)}></i></td>}
                </tr>):
                (<tr key="-2"><td   className="sum_cell" 
                                    colSpan="5"
                                    style={{cursor: hide&&'pointer'}}
                                    onClick={()=>setHide(false)}>סה"כ {label} - {total} שקלים</td></tr>)
        }) 
    }


    const renderRowForMobile = (dataApi, row, rowindex) => {
        if(!showCategory) return null; 
        return categoryapi !=='others' && row['expence'] === ''? null: (
            <React.Fragment key={rowindex}>
                <tr key={rowindex}>
                {
                    dataApi.map(({api},callindex) => 
                    (api !== 'categoty' && api !== 'contact' && api !== 'phone') && 
                            <DataCell   key={callindex}
                                        value={row[api]}
                                        type="text"
                                        forceDisable={api ==='expence' && categoryapi !== 'others'}
                                        placeholder={api === 'expence'? 'שם הוצאה': ''}
                                        onChange={(value) => {
                                        row[api] = value
                                        onEdit(categoryapi,rowindex,row)}}
                    />
                )}
                <td key="-10"><i    className={`${row['open']? 'fas fa-chevron-up':'fas fa-id-card-alt'}`} 
                                    onClick={() => {
                                        row['open'] = !row['open']
                                        setRowsState(rows);
                                        forceUpdate();
                                    }}></i>
                </td>
            </tr>
            {
                row['open'] &&
                <tr>
                    <DataCell   key="1"
                                value={row['contact']}
                                type="text"
                                placeholder="איש קשר"
                                onChange={(value) => {
                                        row['contact'] = value
                                        onEdit(categoryapi,rowindex,row)}}/>
                    <DataCell   key="2"
                                value={row['phone']}
                                type="text"
                                placeholder="טלפון"
                                onChange={(value) => {
                                        row['phone'] = value
                                        onEdit(categoryapi,rowindex,row)}}/>                                        
                </tr>
            }

            </React.Fragment>
        )
    }

    const generateRowModile = () => {
        let total = 0;
        return  rowsState.map((row,rowindex) => {
                if(row['perPerson'] !== undefined && row['perPerson']){
                    total += (Number.parseInt(row['price']) * invitedlistCount)
                } else {
                    total += row['price'] !== undefined && Number.parseInt(row['price'])
                }
                dataApi.forEach(({api}) => {
                    if(!row[api]){
                        if(api === 'price') row[api] = 0;
                        else row[api] = ''
                    }
                }) 
                return rowindex === 0? (
                        <React.Fragment key={rowindex}>
                            <tr key="-3">
                                <td colSpan="5" onClick={() => {
                                    setShowCategory(!showCategory)
                                }}><b>{label} סה"כ הוצאות: {total}₪</b></td>
                            </tr>
                            {renderRowForMobile(dataApi,row,rowindex)}
                        </React.Fragment>
                ): renderRowForMobile(dataApi,row,rowindex)
            })
    }
    return !isMobile? generateRow() : generateRowModile();
}