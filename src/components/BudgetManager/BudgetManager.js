import React from 'react';
import { connect } from 'react-redux';
import ChartPicker from './ChartPicker';
import { ToggleBar } from '../commons'
import BudgetTable from './BudgetTable';
import TableConstanse from './TableConstanse'
import TopPanel from './TopPanel';
import { startSetList } from '../../actions/budgetData';
import { exportTableToExcel,importFileToTableForBudgetList } from '../../helpers/excel';
import { isMobile } from '../../helpers/device-detect';
class BudgetMenagerClass extends React.Component{
    constructor(props){
        super(props);
        var budgetTable = {};
        Object.assign(budgetTable,JSON.parse(JSON.stringify(TableConstanse)));
        Object.assign(budgetTable,props.budgetlist)
        this.state = {
            budgetTable,
            alert: {
                type: 'alert-primary',
                message: '',
                isShown: false
            },
        }
    }
    UNSAFE_componentWillUpdate(nextProps, nextState){
        if(this.props.budgetlist !== nextProps.budgetlist){
            var budgetTable = {};
            Object.assign(budgetTable,this.state.budgetTable);
            Object.assign(budgetTable,nextProps.budgetlist)
            this.setState({budgetTable})
        }
    }
    onEdit(category, row, update) {
        var {budgetTable} = this.state;
        Object.assign(budgetTable[category][row],update)
        this.setState({budgetTable})
    }

    onInsert() {
        const {budgetTable} = this.state;
        budgetTable['others'].unshift({ expence: '', contact: '', phone: '', price: 0})
        this.setState({budgetTable})
    }
    onRemove(){
        this.setState({budgetTable: JSON.parse(JSON.stringify(TableConstanse))})
    }

    onSave(){
        this.props.startSetList(this.state.budgetTable)
        this.setState({
            alert: {
            isShown: true,
            type: 'alert-primary',
            message: 'רשימה נשמרה בהצלחה'
        }})
    }
    onRemoveRow(index){
        const { budgetTable } = this.state;
        budgetTable['others'].splice(index+1,1)
        this.setState({budgetTable})
    }
    onExportToCsv(){
        let datalist = [];
        const { budgetTable } = this.state;
        
        budgetTable['general'].forEach(elem => datalist.push(Object.assign(elem,{category: 'הוצאות כלליות'})))
        budgetTable['self'].forEach(elem => datalist.push(Object.assign(elem,{category: 'חתן וכלה'})))
        budgetTable['more'].forEach(elem => datalist.push(Object.assign(elem,{category: 'תוספות'})))
        budgetTable['package'].forEach(elem => datalist.push(Object.assign(elem,{category: 'הוצאות ידועות'})))
        budgetTable['hall'].forEach(elem => datalist.push(Object.assign(elem,{category: 'אולם'})))
        budgetTable['others'].forEach(elem => datalist.push(Object.assign(elem,{category: 'אחר'})))

        exportTableToExcel(datalist,headers)
        .then(() => {
            this.setState({
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'ייצוא רשימה בוצע בהצלחה'
                }
            })
        })
    }

    onImportFromCsv(inputId,filename){
        importFileToTableForBudgetList(inputId,filename)
        .then(data => {
            var budgetTable = {
                general: data.filter(row => row['category'] === 'הוצאות כלליות'),
                self: data.filter(row => row['category'] === 'חתן וכלה'),
                more: data.filter(row => row['category'] === 'תוספות'),
                package: data.filter(row => row['category'] === 'הוצאות ידועות'),
                hall: data.filter(row => row['category'] === 'אולם'),
                others: data.filter(row => row['category'] === 'אחר'),
            }
            this.setState({
                budgetTable,
                alert: {
                    isShown: true,
                    type: 'alert-primary',
                    message: 'ייבוא רשימה בוצע בהצלחה'
                }
            })
        })
    }

    sumBudget(){
        let total = 0;
        const { budgetTable } = this.state;
        for(let category in budgetTable){
            // eslint-disable-next-line no-loop-func
            budgetTable[category].forEach(row => {
                if(row['perPerson'] !== undefined && row['perPerson'] === true){
                    total += (Number.parseInt(row['price']) * this.props.invitedlist.length)
                }
                else {
                    total += Number.parseInt(row['price'])
                }
            });
        }
        return total;
    }

    sumFields(field){
        const {invitedlist} = this.props;
        let total = 0;
        invitedlist.forEach(row => total += Number.parseInt(row[field]))
        return total;
    }

    totalApproved(approved){
        const {invitedlist} = this.props;
        let total = 0;
        invitedlist.forEach(row => {
            // eslint-disable-next-line eqeqeq
            if(row['approved'] == approved) total += Number.parseInt(row['totalCount'])
        })
        return total;
    }

    render(){
        const {invitedlist} = this.props;
        const { alert } = this.state;
        return(
            <div id="budget_manager" className="container">
                {
                    !isMobile &&
                    <ToggleBar label="גרפים" id='mainToggle'>
                        { ["chartOne","chartTwo","chartThree"].map((id,index) => <ChartPicker   list={invitedlist}
                                                                chartId={id}
                                                                key={index}
                                                                chartList={optionsChart}/>)}
                        <div style={{clear:'both'}}></div>
                    </ToggleBar>

                }
                
                <ToggleBar label="סיקור כללי" id="budgetToggle">
                    <table id="budget_sumeraize">
                        <thead>
                            <tr>
                                <th>תקציב כולל</th>
                                <th>כל המתנות</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="sum_budget">{this.sumBudget()}&#x20AA;</td>
                                <td className="sum_presents">{this.sumFields('present')}&#x20AA;</td>
                            </tr>
                            <tr>
                                <th>מאזן</th>
                                <th>תקציב למוזמן</th>
                            </tr>
                            <tr>
                                <td style={{color: this.sumFields('present') >= this.sumBudget()? 'green' : 'red'}}>{this.sumFields('present') - this.sumBudget()}&#x20AA;</td>
                                <td className="sum_budget">{(this.sumBudget() / this.sumFields('totalCount')).toFixed(2)}&#x20AA;</td>
                            </tr>
                        </tbody>
                    </table>

                    <table id="counting_sumerize">
                        <tbody>
                            <tr>
                                <td><i className="fas fa-child"></i>{`${this.sumFields('children')} ${!isMobile ? 'ילדים':''}`}</td>
                                <td><i className="fas fa-carrot"></i>{`${this.sumFields('vegetarian')} ${!isMobile ? 'צמחוניים':''}`}</td>
                                <td><i className="fas fa-seedling"></i>{`${this.sumFields('vegan')} ${!isMobile ? 'טבעוניים':''}`}</td>
                            </tr>
                            <tr>
                                <td><i className="fas fa-male"></i>{`${this.sumFields('totalCount')} ${!isMobile ? 'מוזמנים':''}`}</td>
                                <td><i className="far fa-thumbs-up"></i>{`${this.totalApproved(1)} ${!isMobile ? 'אישרו הגעה':''}`}</td>
                                <td><i className="fas fa-question"></i>{`${this.totalApproved(0)} ${!isMobile ? 'אולי מגיעים':''}`}</td>
                            </tr>
                        </tbody>
                    </table>

                </ToggleBar>

                <TopPanel   onInsert={this.onInsert.bind(this)}
                            onRemove={this.onRemove.bind(this)}
                            onSave={this.onSave.bind(this)}
                            onExport={this.onExportToCsv.bind(this)}
                            onImport={(inputId,fileName) => this.onImportFromCsv(inputId,fileName)}/>
                {   
                    alert.isShown && <div className={`alert ${alert.type}`} role="alert">
                        {alert.message}
                    </div>
                }
                <BudgetTable    budgetTable={this.state.budgetTable}
                                onRemove={this.onRemoveRow.bind(this)}
                                invitedlistCount={this.sumFields('totalCount')}
                                onEdit={(api, row, update) => this.onEdit(api, row, update)}/>
            </div>
        )
    }
}

const headers = [
    {label: 'הוצאה', api:'expence'},
    {label: 'איש קשר', api:'contact'},
    {label: 'טלפון', api:'phone'},
    {label: 'מחיר', api:'price'},
    {label: 'קטגוריה', api:'category'},
];

const optionsChart = [
    {
        label: 'חלוקת מוזמנים עוגה',
        value: 'pieInvited'
    },
    {
        label: 'חלוקת מוזמנים עמודות',
        value: 'columnInvited'
    },        
    {
        label: 'מוזמנים לפי צד',
        value: 'allSideInveted'
    },
]

const mapDispatchToProps = (dispatch) => ({
    startSetList: (list) => dispatch(startSetList(list))
})

const mapStateToProps = (state) => ({
    invitedlist: state.invitedsData,
    budgetlist: state.budgetData
})

export const BudgetMenager = connect(mapStateToProps,mapDispatchToProps)(BudgetMenagerClass);