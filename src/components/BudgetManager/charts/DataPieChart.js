import React from 'react';
import { PieChart,Pie, Cell } from 'recharts';

import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
        

export const DataPieChart = ({data}) => {
    return (
        <PieChart width={400} height={400}>
            <Pie
                data={data}
                cx={200}
                cy={200}
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={80}
                fill="#8884d8"
                dataKey="value"
            >
            {
                data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
            }
            </Pie>
        </PieChart>
    )
}


const translateLabel = (label) => {
    if(label === 'approved') return 'אישרו';
    if(label === 'children') return 'ילדים';
    if(label === 'meet') return 'אוכלי בשר';
    if(label === 'vegan') return 'טבעוניים';
    if(label === 'vegetarian') return 'צמחוניים'
}


const RADIAN = Math.PI / 180;
const COLORS = scaleOrdinal(schemeCategory10).range();

const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, index, name,value  }) => {
    var radius = innerRadius + (outerRadius - innerRadius);
    if((midAngle >= 30 && midAngle <= 120) || (midAngle >= 210 && midAngle <= 330)){
        radius *= 1.2;
    } else radius *= 2;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);
    return (
        <text   fontSize="14" 
                x={x} 
                y={y} 
                fill={COLORS[index]} 
                textAnchor={x > cx ? 'start' : 'end'} 
                dominantBaseline="central">
                {`${translateLabel(name)} - ${value}`}
        </text>
    );
  };
  

