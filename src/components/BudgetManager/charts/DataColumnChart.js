import React from 'react';
import { Cell,BarChart, Bar, XAxis, YAxis, CartesianGrid } from 'recharts';

import PropTypes from 'prop-types';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
        

export const DataColumnChart = ({data}) => {
    data = data.map(elem => {return {name: translateLabel(elem.name), female: elem.value}})
    return (
        <BarChart
            width={500}
            height={300}
            data={data}
            margin={{
            top: 20, right: 30, left: 20, bottom: 5,
        }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Bar dataKey="female" fill="#8884d8" shape={<TriangleBar />} label={{ position: 'top' }}>
            {
            data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={COLORS[index % 20]} />
            ))
            }
            </Bar>
        </BarChart>
    )    
}



const translateLabel = (label) => {
    if(label === 'approved') return 'אישרו';
    if(label === 'children') return 'ילדים';
    if(label === 'meet') return 'אוכלי בשר';
    if(label === 'vegan') return 'טבעוניים';
    if(label === 'vegetarian') return 'צמחוניים'
}

const COLORS = scaleOrdinal(schemeCategory10).range();


const TriangleBar = (props) => {
    const { fill, x, y, width, height} = props;
    return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
};

TriangleBar.propTypes = {
    fill: PropTypes.string,
    x: PropTypes.number,
    y: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
};


  
const getPath = (x, y, width, height) => `M${x},${y + height}
                    C${x + width / 3},${y + height} ${x + width / 2},${y + height / 3} ${x + width / 2}, ${y}
                    C${x + width / 2},${y + height / 3} ${x + 2 * width / 3},${y + height} ${x + width}, ${y + height}
                    Z`;

