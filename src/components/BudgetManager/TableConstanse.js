
export default {
    general: [
        { expence: 'רבנות', contact: '', phone: '', price: 0},
        { expence: 'רב', contact: '', phone: '', price: 0},
        { expence: 'אקום', contact: '', phone: '', price: 0},
    ],
    self: [
        { expence: 'טבעות נישואין', contact: '', phone: '', price: 0},
        { expence: 'שמלת כלה', contact: '', phone: '', price: 0},
        { expence: 'איפור כלה', contact: '', phone: '', price: 0},
        { expence: 'נעלי כלה', contact: '', phone: '', price: 0},
        { expence: 'חליפת חתן', contact: '', phone: '', price: 0},
        { expence: 'נעלי חתן', contact: '', phone: '', price: 0},
        { expence: 'זר כלה', contact: '', phone: '', price: 0},
        { expence: 'קישוט לרכב', contact: '', phone: '', price: 0},
        { expence: 'ליל כלולות', contact: '', phone: '', price: 0},
    ],
    more: [
        { expence: 'סידורי שולחן', contact: '', phone: '', price: 0},
        { expence: 'הזמנות', contact: '', phone: '', price: 0},
        { expence: 'חולצות שטות', contact: '', phone: '', price: 0},
        { expence: 'אביזרים לרחבה', contact: '', phone: '', price: 0},
    ],
    package: [
        { expence: 'DJ', contact: '', phone: '', price: 0},
        { expence: 'צלם מגנטים', contact: '', phone: '', price: 0},
        { expence: 'פירוטכניקה', contact: '', phone: '', price: 0},
        { expence: 'תאורה', contact: '', phone: '', price: 0},
    ],
    hall: [
        { expence: 'מחיר מנה (עבור מוזמן)', contact: '', phone: '', price: 0, perPerson: true},
        { expence: 'שירותי מזיגה (עבור מוזמן)', contact: '', phone: '', price: 0, perPerson: true},
        { expence: 'בר אלכוהול', contact: '', phone: '', price: 0},
        { expence: 'טיפים למלצר', contact: '', phone: '', price: 0},
    ],
    others:[]
}
