import React, {useState} from 'react';
import { DataPieChart,DataColumnChart,DataColumnTwoChart } from './charts';

export default (props) => {
    const [chartName, setChartName] = useState(localStorage.getItem(props.chartId));
    const [active, setActive] = useState(false)
    return(
        <div className="chart_picker">
            {
                active && (
                    <select value={chartName}
                    className="form-control"
                    onChange={(event) => {
                        const newChartName = event.target.value;
                        localStorage.setItem(props.chartId,newChartName)
                        setChartName(newChartName)
                    }}>
                        <option value="">ללא</option>
                        {props.chartList && props.chartList.map((item,index) => <option key={index} value={item.value}>{item.label}</option>)}
            </select>
                        )
            }
            <div    className={`chart_view ${active? 'active' : ''} ${chartName}`}
                    onClick={()=>setActive(!active)}>
                {switchChart(chartName,props.list)}
            </div>
        </div>
    )
}


export const switchChart = (chartName,list) => {
    if(chartName === 'pieInvited')
        return <DataPieChart data={sumeraizeData(list,['vegan','children','meet','vegetarian'])}/>
    if(chartName === 'columnInvited')
        return <DataColumnChart data={sumeraizeData(list,['vegan','children','meet','vegetarian'])}/>
    if(chartName === 'allSideInveted') 
        return <DataColumnTwoChart data={allSideInvetedSumeraize(list)}/>
    return (<div className="empty_chart"><h1>+</h1></div>)
}



const sumeraizeData = (data, keys) => {
    return keys.map(key => {
        const dt = {name: key, value: 0};
        data.forEach(elem => {
            dt.value += Number.parseInt(elem[key])
        })
        return dt;
    }).filter(obj => obj.value !== 0)       
}

const allSideInvetedSumeraize = (list) => {
    const total = {side1: 0, side2:0, none: 0 };
    const approved = {side1: 0, side2:0, none: 0 };

    list.forEach(elem => {
        if(elem.side === 'צד א') {
            total.side1 += Number.parseInt(elem.totalCount);
            approved.side1 += Number.parseInt(elem.approved);
        }
        else if(elem.side === 'צד ב') {
            total.side2 += Number.parseInt(elem.totalCount);
            approved.side2 += Number.parseInt(elem.approved);
        }
        else {
            total.none += Number.parseInt(elem.totalCount);
            approved.none += Number.parseInt(elem.approved);
        }
    })
    return [
        {name: 'צד א', כללי: total.side1, אישרו: approved.side1},
        {name: 'צד ב', כללי: total.side2, אישרו: approved.side2},
        {name: 'ללא שיוך', כללי: total.none, אישרו: approved.none},
    ]
}



