
import React,{useState} from 'react'
export default ({type,hide, onChange,value,style,forceDisable,placeholder}) => {
    const [disableField, setDisableField] = useState(true)
    return(
        <td style={{textAlign: 'right', display: hide && 'none'}}>
            <span onClick={()=> setDisableField(false)}>
                <input  value={value}
                        type={type}
                        placeholder={placeholder}
                        style={style}
                        className={`form-control${disableField? '-plaintext':''}`}
                        onBlur={()=> !disableField && setDisableField(true)}
                        onChange={event => onChange(event.target.value)}
                        readOnly={disableField||forceDisable}/>
            </span>
        </td>
    )
}