import React from 'react';
import BudgetTableCategory from './BudgetTableCategory';
import {isMobile} from '../../helpers/device-detect';

const dataApi= [
    {label: 'קטגוריה', api: 'categoty'},
    {label: 'הוצאה', api: 'expence'},
    {label: 'איש קשר', api: 'contact'},
    {label: 'טלפון', api: 'phone'},
    {label: 'עלות', api: 'price'},
]


export default ({budgetTable, onEdit, onRemove,invitedlistCount}) => {

    return (
        <div id="budget_table" className="table-responsive">
            <table className="table">
                <thead>
                    <tr>
                        {dataApi.map((elem,index) => (!isMobile || (isMobile && index !== 0 && index !== 2 && index !== 3))? <td key={index}>{elem.label}</td>: null)}
                    </tr>
                </thead>
                <tbody>
                    <BudgetTableCategory
                        rows={[...budgetTable['hall'],{}]}
                        categoryapi={'hall'}
                        label={'אולם'}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                        invitedlistCount={invitedlistCount}
                    />
                    <BudgetTableCategory
                        rows={[...budgetTable['general'],{}]}
                        categoryapi={'general'}
                        label={'הוצאות קבועות'}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                    />
                    <BudgetTableCategory
                        rows={[...budgetTable['self'],{}]}
                        categoryapi={'self'}
                        label={'חתן וכלה'}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                    />
                    <BudgetTableCategory
                        rows={[...budgetTable['more'],{}]}
                        categoryapi={'more'}
                        label={'תוספות'}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                    />
                    <BudgetTableCategory
                        rows={[...budgetTable['package'],{}]}
                        categoryapi={'package'}
                        label={'הוצאות ידועות'}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                    />
                    <BudgetTableCategory
                        rows={[...budgetTable['others'],{}]}
                        categoryapi={'others'}
                        label={'אחר'}
                        onRemove={onRemove.bind(this)}
                        onEdit={onEdit.bind(this)}
                        dataApi={dataApi}
                    />
                </tbody>
            </table>
        </div>
    )
}