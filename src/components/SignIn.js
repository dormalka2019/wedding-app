import React,{useState} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { startSignIn,startSignInWithGoogle,startSignUp,updateUser } from '../actions/auth';
import { matchPath } from 'react-router';
import { ButtonWithLoader } from './commons';

const mapDispatchToProps = (dispatch) => ({
    startSignIn: (email,pass) => dispatch(startSignIn(email,pass)),
    startSignInWithGoogle: () => dispatch(startSignInWithGoogle()),
    startSignUp: (email,pass) => dispatch(startSignUp(email,pass)),
    updateUser: (user) => dispatch(updateUser(user))
  });


export const SignIn = connect(undefined,mapDispatchToProps)((props) => {
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [repassword,setRepassword] = useState('')
    const [lock, setLock] = useState(false)
    const [error, setError] = useState(null);
    const isSignUp = matchPath(window.location.pathname,"/signup")

    const onSubmit = (event) => {
        event.preventDefault();
        setLock(true)
        const regPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        const regEmail = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
        if(!regEmail.test(email)){
            setError('אימייל לא תקין')
            setLock(false)
            return;
        } else if(!regPass.test(password)) {
            setError('סיסמא חייבת להכיל מינימום 8 תוים, אות אחת ומספר אחד')
            setLock(false)
            return;
        }
        if(isSignUp) onSignUp()
        else onSignIn();
    }

    const onSignIn = (event) => {
        props.startSignIn(email,password)
        .then((res) => props.history.push('/invitedmanager'))
        .catch((error) => {
            switch(error.code){
                case 'auth/invalid-email':
                    setError('כתובת אימייל לא קיימת');
                    break;
                default:
                    setError(error.code);
                    break;
            }
        })
        .finally(() => setLock(false))
    }

    const onSignUp = () => {
        if(password !== repassword) {
            setError('סיסמאות לא תואמות')
            setLock(false)
            return;
        }
        
        props.startSignUp(email,password)
        .then(({user}) => {
            
            const updateUser = {
                displayName: user.displayName,
                email: user.email,
                emailVerified: user.emailVerified,
                uid: user.uid,
                photoURL: user.photoURL,
            }

            props.updateUser(updateUser)
        })
        .catch((error) => {
            switch(error.code){
                case 'auth/invalid-email':
                    setError('כתובת אימייל לא קיימת');
                    break;
                default:
                    setError(error.code);
                    break;
            }
        })
        .finally(() => setLock(false))

    }

    const onSignUpWithGoogle = (event) => {
        event.preventDefault();
        setLock(true);
        props.startSignInWithGoogle()
        .then(({user}) => {
            const updateUser = {
                displayName: user.displayName,
                email: user.email,
                emailVerified: user.emailVerified,
                uid: user.uid,
                photoURL: user.photoURL,
            }

            props.updateUser(updateUser)
            
        })
        .catch((error) => {
            setError(error.code)
        })
        .finally(() => setLock(false))
    }

    return(
        <form id="sign_in">
            <h1>{isSignUp? 'הרשמה': 'התחברות'}</h1>
            {error &&   <div className="alert alert-danger">
                            <strong>שגיאה!</strong> {error}
                        </div>}
            <input  type="text"
                    className="form-control"
                    value={email}
                    placeholder="אימייל"
                    disabled={lock}
                    onChange={(event) => setEmail(event.target.value)}/>
            <input  type="password"
                    placeholder="סיסמא"
                    className="form-control"
                    disabled={lock}
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}/>

            {isSignUp &&    <input  type="password"
                                    className="form-control"
                                    disabled={lock}
                                    placeholder="סיסמא חוזרת"
                                    value={repassword}
                                    onChange={(event) => setRepassword(event.target.value)}/>
            }
            {isSignUp? <Link to="/signin">התחברות למערכת</Link>:<Link to="/signup">הרשמה למערכת</Link>}
            <ButtonWithLoader   onClick={(event) => onSubmit(event)}
                                showLoader={lock}>
            {isSignUp? 'הרשמה': 'התחברות'}
            </ButtonWithLoader>
            <button className="btn btn-social btn-google"
                    onClick={(event) => onSignUpWithGoogle(event)}>
                <i className="fab fa-google"></i> {`${isSignUp? 'הרשמה':'התחברות'} באמצעות גוגל`}            
            </button>
            {isSignUp && (<div>בהרשמה להמערכת אני מאשר כי קראתי ואישרתי את <Link to="/takanon">תקנון האתר</Link></div>)}
        </form>
    )
});