export const templatesList = [
    {
        templateName: "babyBorn_male",
        templateLabel: 'רך נולד',
        templateType: "babyborn_1",
        freeMode: true
    },
    {
        templateName: "babyBorn_female",
        templateLabel: 'תינוקת באה',
        templateType: "babyborn_1",
        freeMode: true
    },
    {
        templateName: "wedding_classic",
        templateLabel: 'חתונה קלאסית',
        templateType: 'wedding_1',
        freeMode: true
    },
    {
        templateName: "wedding_sea",
        templateLabel: 'חתונה ים',
        templateType: 'wedding_1',
        price: 10,
        ownerMail: 'dormalk@gmail.com',
        ownerName: 'Dor Malka',
        freeMode: false
    },
    {
        templateName: "wedding_elegant",
        templateLabel: 'חתונה אלגנטית',
        templateType: 'wedding_1',
        freeMode: true
    },
    {
        templateName: "classic_event",
        templateLabel: 'אירוע קלאסי',
        templateType: 'event_1',
        freeMode: true
    },
    {
        templateName: "birthday_1",
        templateLabel: 'יומולדת שמח',
        templateType: 'birthday_1',
        freeMode: true
    }

    
]