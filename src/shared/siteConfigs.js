export const siteConfigs = {
    siteName: 'MyEvent',
    whatsappNum: '972524431050',
    pages: [
        {
            label: 'עדכון פרטים',
            url: '/selfdetails',
            isPrivate: true
        },
        {
            label: 'סידורי הושבה',
            url: '/sittingarrange',
            isPrivate: false
        },
        {
            label: 'ניהול הודעות',
            url: '/messagemanager',
            isPrivate: false
        },        
        {
            label: 'הזמנה דיגיטלית חינם',
            url: '/invitations/create',
            isPrivate: false
        },
        {
            label: 'ניהול מוזמנים',
            url: '/invitedmanager',
            isPrivate: false
        },
        {
            label: 'ניהול תקציב',
            url: '/budgetmanager',
            isPrivate: false
        }

    ],
    adminPages: [
        {
            label: 'לוגים',
            url: '/logs',
            isPrivate: false
        },
        {
            label: 'טיפול בפניות',
            url: '/alltickets',
            isPrivate: false
        },
        {
            label: 'ניהול משתמשים',
            url: '/allusers',
            isPrivate: false
        },
        {
            label: 'לוח בקרה - מנהל מערכת',
            url: '/mainpanel',
            isPrivate: false
        }
        // {
        //     label: 'ניהול חנות עיצובים',
        //     url: '/invitaionsdesign',
        //     isPrivate: false
        // }
    ],
    packages: [
        {
            name: '50 הודעות',
            price: 0.3 * 50,
            api: 'p50',
            count: 50,
            currency_code: 'shekel',
            paypal_code: 'RBEKFTFLTZWFG'
        },
        {
            name: '100 הודעות',
            price: 0.25 * 100,
            count: 100,
            api: 'p100',
            currency_code: 'shekel',
            paypal_code: 'UFVNYRXMMRLSE'
        },
        {
            name: '200 הודעות',
            price: 0.23 * 200,
            api: 'p200',
            count: 200,
            currency_code: 'shekel',
            paypal_code: '7XEUZTUQYETSN'
        },
        {
            name: '400 הודעות',
            price: 0.18 * 400,
            api: 'p400',
            count: 400,
            currency_code: 'shekel',
            paypal_code: 'WHZCG6TPV8EQ8'
        },
        {
            name: '800 הודעות',
            price: 0.13 * 800,
            api: 'p800',
            count: 800,
            currency_code: 'shekel',
            paypal_code: '9PTQ24Q4KH6T4'
        },
        // {
        //     name: 'חבילת הודעות בדיקה',
        //     price: 0.1,
        //     api: 'test',
        //     currency_code: 'shekel',
        //     paypal_code: 'NH3FYJHALSPWU'
        // },
        // {
        //     name: 'הכל כלול',
        //     price: '1.25₪ למוזמן',
        //     api: 'allinone',
        //     // paypal_code: 'NH3FYJHALSPWU'
        // },

    ]
}