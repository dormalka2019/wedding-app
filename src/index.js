import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";


import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './styles/styles.scss';
import './styles/templates.scss';

import App from './App';
// import * as serviceWorker from './serviceWorker';
import configureStore from './store/configureStore';
import { siteConfigs } from './shared/siteConfigs'
import { login, logout } from './actions/auth'; 
import { matchPath } from 'react-router';
import { registerServiceWorker } from './register-sw';
import { initializedFirebaseApp } from './configs/firebase';


const store = configureStore();
const jsx = (
    <Provider store={store}>
        <App/>
        {
            !matchPath(window.location.pathname,"/invitations/view/:id") &&
            // eslint-disable-next-line react/jsx-no-target-blank
            <a id="whatsapplink" href={`https://wa.me/${siteConfigs.whatsappNum}`} target="_blank">
                <img src="/assets/images/whatsapp.svg" alt="whatsapp"/>
            </a>
        }
    </Provider>
)
store.dispatch({type:'IN_LOAD'});


const unsubscribe = store.subscribe(()=> {
    if(!store.getState().userdata.loading){
        ReactDOM.render(jsx, document.getElementById('root'));
        unsubscribe();
    }
})


ReactDOM.render(<div id="loader"><h1>{siteConfigs.siteName}</h1></div>, document.getElementById('root'));


initializedFirebaseApp.auth().onAuthStateChanged(user => {
    if(user) store.dispatch(login(user.uid))
    else store.dispatch(logout())
})
registerServiceWorker();



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

