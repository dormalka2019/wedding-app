importScripts('https://www.gstatic.com/firebasejs/7.8.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.2/firebase-messaging.js');

firebase.initializeApp({
  apiKey: "AIzaSyAO-2rlQYXc_xT-2eeMz8iuxPzCTYJHii8",
  authDomain: "wedding-app-a228e.firebaseapp.com",
  databaseURL: "https://wedding-app-a228e.firebaseio.com",
  projectId: "wedding-app-a228e",
  storageBucket: "wedding-app-a228e.appspot.com",
  messagingSenderId: "195834146988",
  appId: "1:195834146988:web:b370923c80af3e9cd1ef09",
  measurementId: "G-SDN8GV3VPK"
});
if(firebase.messaging.isSupported()){
  const messaging = firebase.messaging();
  messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const {body,title} = JSON.parse(payload.data.notification);
    console.log(body,title)
    const options = {
      body  
    };
    return self.registration.showNotification(title,options);
  });
}